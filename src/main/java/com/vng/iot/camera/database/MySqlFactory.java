package com.vng.iot.camera.database;

import com.vng.iot.camera.common.CloudConfig;
import snaq.db.ConnectionPool;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by baohv on 13/07/2017.
 */
public class MySqlFactory {
    private static MySqlFactory ourInstance = new MySqlFactory();

    public static MySqlFactory getInstance() {
        return ourInstance;
    }

    private MySqlFactory() {
    }

    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final ConnectionPool pool;
    private static final String conn_str;

    private static final String MYSQL_HOST = CloudConfig.getInstance().getMySqlConfig().getHost();
    private static final String MYSQL_PORT = CloudConfig.getInstance().getMySqlConfig().getPort();
    private static final String MYSQL_NAME = CloudConfig.getInstance().getMySqlConfig().getDbName();
    private static final String MYSQL_USER = CloudConfig.getInstance().getMySqlConfig().getUser();
    private static final String MYSQL_PASS = CloudConfig.getInstance().getMySqlConfig().getPass();

    static {
        try {
            Class<?> c = Class.forName(JDBC_DRIVER);
            Driver driver = (Driver) c.newInstance();
            DriverManager.registerDriver(driver);
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
        }
        //conn_str = "jdbc:mysql://" + MYSQL_HOST + ":" + MYSQL_PORT + "/" + MYSQL_NAME
        //        + "?autoReconnect=true&failOverReadOnly=false&maxReconnects=10&useUnicode=true&noAccessToProcedureBodies=true&useSSL=false";
        conn_str = "jdbc:mysql://" + MYSQL_HOST + ":" + MYSQL_PORT + "/" + MYSQL_NAME
                  + "?autoReconnect=true&failOverReadOnly=false&maxReconnects=10&useUnicode=true&characterEncoding=UTF-8&noAccessToProcedureBodies=true&useSSL=false";
        pool = new ConnectionPool("local", 1, 10, 30, 180, conn_str, MYSQL_USER, MYSQL_PASS);
    }

    public Connection getConnection() {
        if (pool != null) {
            try {
                return pool.getConnection(2000);
            } catch (SQLException ex) {
                Logger.getLogger(MySqlFactory.class.getName()).log(Level.SEVERE, "getConnection : " + ex.getMessage());
            }
        } else {
            return null;
        }
        return null;

    }

    public void safeClose(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(MySqlFactory.class.getName()).log(Level.SEVERE,
                        "safeClose.Connection:" + e.getMessage(), e);
            }
        }
    }

    public void safeClose(ResultSet res) {
        if (res != null) {
            try {
                res.close();
            } catch (SQLException e) {
                Logger.getLogger(MySqlFactory.class.getName()).log(Level.SEVERE,
                        "safeClose.ResultSet:" + e.getMessage(), e);
            }
        }
    }

    public void safeClose(Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                Logger.getLogger(MySqlFactory.class.getName()).log(Level.SEVERE,
                        "safeClose.Statement:" + e.getMessage(), e);
            }
        }
    }
}
