/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.controller;

import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.data.Segment;
import com.vng.iot.camera.data.Site;
import com.vng.iot.camera.model.SiteModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author phuoc
 */
public class SiteController extends BaseController {

    private static final Logger LOG = Logger.getLogger(PublisherController.class.getName());

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = request.getPathInfo();
        PrintWriter out = response.getWriter();

        if (null != cmd) {
            switch (cmd) {
                case "/add": {
                    String dt = IOUtils.toString(request.getInputStream());
                    processAddSite(dt, out);
                    break;
                }
                case "/udt": {
                    String dt = IOUtils.toString(request.getInputStream());
                    processUpdateSite(dt, out);
                    break;
                }case "/del":{
                    String dt = IOUtils.toString(request.getInputStream());
                    processDelSite(dt, out);
                    break;
                }
                default:
                    sendFailed(out, -1, "Unknown command");
            }
        } else {
            sendFailed(out, -1, "Unknown command");
        }
    }
    private void processDelSite(String dt,PrintWriter out){
        Site site = Common.GSON.fromJson(dt, Site.class);
        if (null == site) {
            LOG.warning("Invalid Segment info");
            sendFailed(out, -1);
            return;
        }else{
            if(SiteModel.getInstance().delSite(site) !=0){
                LOG.warning("Deleted failed");
                sendFailed(out, -3);
                return;
            }
            
            sendSuccess(out);
        }
    }
    private void processAddSite(String dt, PrintWriter out) {
        Site site = Common.GSON.fromJson(dt, Site.class);
        StringBuilder camInsertId = new StringBuilder();
        if (null == site) {
            LOG.warning("Invalid Segment info");
            sendFailed(out, -1);
            return;
        } else {
            if (SiteModel.getInstance().addSite(site, camInsertId) != 0) {
                LOG.warning("Add camera failed");
                sendFailed(out, -4);
                return;
            }
            sendSuccess(out);
        }
    }

    private void processUpdateSite(String dt, PrintWriter out) {
        Site site = Common.GSON.fromJson(dt, Site.class);
        if (null == site) {
            LOG.warning("Invalid Segment info");
            sendFailed(out, -1);
            return;
        } else if (SiteModel.getInstance().updateSite(site) != 0) {
            LOG.warning("Update site failed");
            sendFailed(out, -4);
            return;
        } else {
            sendSuccess(out);
        }
    }

}
