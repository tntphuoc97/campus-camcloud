package com.vng.iot.camera.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.common.Define;
import com.vng.iot.camera.data.Camera;
import com.vng.iot.camera.data.Hub;
import com.vng.iot.camera.data.Stream;
import com.vng.iot.camera.model.HubControllingDataModel;
import com.vng.iot.camera.model.HubModel;
import com.vng.iot.camera.model.StreamModel;
import com.vng.iot.camera.model.UserModel;
import com.vng.iot.camera.mqtt.MqttPublisherHelper;
import com.vng.iot.camera.mqtt.StreamModelMqtt;
import com.vng.iot.mqtt.controller.DefineBrokerModel;
import com.vng.iot.mqtt.controller.MqttCameraModel;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

public class PublisherController extends AuthenController {

    private static final Logger LOG = Logger.getLogger(PublisherController.class.getName());
    private Gson gson = new GsonBuilder().serializeNulls().create();

    @Override
    protected void processAuthorizedRequest(HttpServletRequest request, HttpServletResponse response, String userId) throws ServletException, IOException {
        String cmd = request.getPathInfo();
        PrintWriter out = response.getWriter();
        String token = getTokenUserCurrent(request, response);
        if (null != cmd) {
            switch (cmd) {
                case "/get_config": {
                    String dt = IOUtils.toString(request.getInputStream());
                    processGetConfig(dt, out);
                    break;
                }
                case "/add": {
                    String dt = IOUtils.toString(request.getInputStream());
                    processAddHub(dt, out, userId, token);
                }
                default:
                    sendFailed(out, -1, "Unknown command");
            }
        } else {
            sendFailed(out, -1, "Unknown command");
        }
    }

    private String getTokenUserCurrent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String token = "";
        HttpSession session = request.getSession(false);
        StringBuilder userIdBuilder = new StringBuilder();
        if (checkSession(session, userIdBuilder)) {
            LOG.info("Authorize");
            String userId = session.getAttribute(Define.KEY_SESSION_USER_ID).toString();
            token += UserModel.getInstance().getTokenByUserId(userId);
        }
        return token;
    }

    private void processAddHub(String dt, PrintWriter out, String userId, String token) {

        UUID uuid = UUID.randomUUID();

        Hub hub = Common.GSON.fromJson(dt, Hub.class);

        if (null == hub) {
            LOG.warning("Invalid camera info");
            sendFailed(out, -4);
            return;
        }
       
        hub.setUuid(uuid.toString().replace("-", ""));
        // Add camera to database
        StringBuilder hubInsertId = new StringBuilder();
        
        if (HubModel.getInstance().addHub(userId, hub, hubInsertId) == 0) {
               
            if (processAddHubMqtt(hub) != 0) {
                LOG.warning("Create Hub to Mqtt failed");
                sendFailed(out, -6);
                return;
            }
           
            StringBuilder insertId = new StringBuilder();
            if(HubControllingDataModel.getInstance().addHubControllingData(hub, insertId) != 0){
                LOG.warning("Cant add hub controlling data model to cloud");
                 sendFailed(out, -7);
            }
             sendSuccess(out);
        } else {
            LOG.warning("Insert camera failed");
            sendFailed(out, -2);
        }
    }

    private int processAddHubMqtt(Hub hub) {
        int ret = 0;
//        List<Camera> cams = new ArrayList<>();
//        // convert list cams to streams
//        
//        
//        List<StreamModelMqtt> streams = new ArrayList<StreamModelMqtt>();
//        MqttCameraModel hubMqtt = DefineBrokerModel.defineHubMqttController(cams, hub.getUuid(), 0,10);
//       
//        String dt = gson.toJson(hubMqtt);
//        String topic = Define.getTopicSetConfigHub(hub.getUuid());
//        MqttPublisherHelper mqtt = new MqttPublisherHelper();
//        if(mqtt.pushDataToBroker(dt, topic, 1) != 0)
//        {
//            ret = 0;
//        }
//        
        return ret;
    }

    private void processGetConfig(String dt, PrintWriter out) {
        List<Stream> streams = new ArrayList<>();
        Hub hub = Common.GSON.fromJson(dt, Hub.class);
        if (HubModel.getInstance().getHubId(hub.getUuid(), hub) == 0) {
            if (StreamModel.getInstance().getStreams(hub.getId(), streams) == 0) {
                JsonElement jdt = Common.GSON.toJsonTree(streams);
                sendSuccess(out, jdt);

                HubModel.getInstance().updateLastUpdateHub(hub.getId());
            } else {
                LOG.warning("Get stream by hub error");
            }
        } else {
            LOG.warning("Get hub id error");
        }
    }
}
