package com.vng.iot.camera.controller;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vng.iot.camera.common.CloudConfig;
import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.data.RecordConfig;
import com.vng.iot.camera.data.RecordStream;
import com.vng.iot.camera.data.Recorder;
import com.vng.iot.camera.data.Segment;
import com.vng.iot.camera.model.RecordModel;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class RecordController extends BaseController {

    private static final Logger LOG = Logger.getLogger(RecordController.class.getName());

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = request.getPathInfo();
        PrintWriter out = response.getWriter();

        if (null != cmd) {
            switch (cmd) {
                case "/get_config": {
                    String dt = IOUtils.toString(request.getInputStream());
                    processGetConfig(dt, out);
                    break;
                }
                case "/add_segment": {
                    String dt = IOUtils.toString(request.getInputStream());
                    processAddSegment(dt, out);
                    break;
                }

                case "/get_segment": {
                    String dt = IOUtils.toString(request.getInputStream());
                    processGetSegment(dt, out);
                    break;
                }
                case "/del_segment": {
                    String dt = IOUtils.toString(request.getInputStream());
                    processDelSegment(dt, out);
                    break;
                }

                default:
                    sendFailed(out, -1, "Unknown command");
            }
        } else {
            sendFailed(out, -1, "Unknown command");
        }
    }

    private void processDelSegment(String dt, PrintWriter out) {

        if (RecordModel.getInstance().delSegment(dt) != 0) {
            LOG.warning("Delete segment failed");
            sendFailed(out, -1);
            return;
        }
        sendSuccess(out);
    }

    private void processGetSegment(String dt, PrintWriter out) {
        JsonObject jparams = new JsonParser().parse(dt).getAsJsonObject();
        String camId;
        Long from, to;
        from = jparams.get("from").getAsLong();
        to = jparams.get("to").getAsLong();
        camId = jparams.get("cam").getAsString();

        List<Segment> segments = new ArrayList<>();
        if (RecordModel.getInstance().getSegments(camId, from, to, segments) == 0) {
            JsonElement jdt = Common.GSON.toJsonTree(segments);
            sendSuccess(out, jdt);
        }
    }

    private void processAddSegment(String dt, PrintWriter out) {
        //Ex: 32/20170920/32-1505925703-20170920-234143.mp4
        JsonObject jdt = new JsonParser().parse(dt).getAsJsonObject();
        String uuid, segmentName;
        uuid = jdt.get("uuid").getAsString();
        segmentName = jdt.get("segment").getAsString();

        LOG.info("New segment: " + segmentName + " from " + uuid);

        Recorder rec = new Recorder();
        if (RecordModel.getInstance().getRecorderId(uuid, rec) == 0) {
            String segPathParams[] = segmentName.split("/");

            if (segPathParams.length != 3) {
                LOG.warning("Invalid segment name format");
                sendFailed(out, -1);
            } else {
                String segParams[] = segPathParams[2].split("-");

                if (segParams.length != 4) {
                    LOG.warning("Invalid segment name format 2");
                    sendFailed(out, -2);
                } else {
                    String camId = segParams[0];
                    String timestamp = segParams[1];

                    Segment segment = new Segment();
                    segment.setCamId(Integer.parseInt(camId));
                    segment.setFilename(segmentName);
                    segment.setTimestamp(Double.parseDouble(timestamp));
                    //segment.setDuration(0);
                    segment.setRecorder(Integer.parseInt(rec.getId()));
                    StringBuilder insertId = new StringBuilder();
                    if (RecordModel.getInstance().addSegment(segment,insertId) == 0) {
                        LOG.info("Add segment success");

                        sendSuccess(out);
                    } else {
                        LOG.warning("Insert segment to db failed");
                        sendFailed(out, -3);
                    }
                }
            }
        }
    }

    private void processGetConfig(String dt, PrintWriter out) {
        Recorder rec = Common.GSON.fromJson(dt, Recorder.class);
        if (RecordModel.getInstance().getRecorderId(rec.getUuid(), rec) == 0) {
            List<RecordStream> recordStreams = new ArrayList<>();
            List<RecordConfig> recordConfigs = new ArrayList<>();
            if (RecordModel.getInstance().getRecordConfigs(rec.getId(), recordConfigs) == 0) {
                recordConfigs.forEach((config) -> {
                    RecordStream stream = new RecordStream();
                    stream.setName(config.getStreamName());
                    String command = CloudConfig.getInstance().getAppConfig().getRecordFormat();
                    command = command.replace("<live_server>", CloudConfig.getInstance().getAppConfig().getLiveDomain());
                    command = command.replace("<stream_name>", config.getStreamName());
                    command = command.replace("<segment_length>", String.valueOf(config.getSegmentLength()));
                    stream.setCommand(command);
                    recordStreams.add(stream);
                });

                JsonElement jdt = Common.GSON.toJsonTree(recordStreams);
                sendSuccess(out, jdt);

            } else {
                LOG.warning("Get stream record by recorder error");
            }
        } else {
            LOG.warning("Get recorder id error");
        }
    }
}
