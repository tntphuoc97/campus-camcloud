/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.controller;

import com.google.gson.JsonElement;
import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.data.Hub;
import com.vng.iot.camera.model.HubModel;
import com.vng.iot.camera.data.Segment;
import com.vng.iot.camera.data.Stream;
import com.vng.iot.camera.model.StreamModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author phuoc
 */
public class SegmentController extends BaseController{
    private static final Logger LOG = Logger.getLogger(PublisherController.class.getName());

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = request.getPathInfo();
        PrintWriter out = response.getWriter();

        if (null != cmd) {
            switch (cmd) {
                case "/del_segment": {
                    String dt = IOUtils.toString(request.getInputStream());
                    processDeleteSegement(dt,out);
                    break;
                }
                default:
                    sendFailed(out, -1, "Unknown command");
            }
        } else {
            sendFailed(out, -1, "Unknown command");
        }
    }

    private void processDeleteSegement(String dt, PrintWriter out) {
        Segment segment = Common.GSON.fromJson(dt, Segment.class);
        
        if (null == segment) {
            LOG.warning("Invalid Segment info");
            sendFailed(out, -1);
            return;
        }
    }
}
