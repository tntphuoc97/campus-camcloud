/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.controller;

import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.data.Group;
import com.vng.iot.camera.data.Site;
import com.vng.iot.camera.model.GroupModel;
import com.vng.iot.camera.model.SiteModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author phuoc
 */
public class GroupController extends BaseController{
    private static final Logger LOG = Logger.getLogger(PublisherController.class.getName());
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = request.getPathInfo();
        PrintWriter out = response.getWriter();

        if (null != cmd) {
            switch (cmd) {
                case "/add": {
                    String dt = IOUtils.toString(request.getInputStream());
                    processAddGroup(dt, out);
                    break;
                }
                case "/udt": {
                    String dt = IOUtils.toString(request.getInputStream());
                    processUpdateGroup(dt, out);
                    break;
                }case "/del":{
                    String dt = IOUtils.toString(request.getInputStream());
                    processDelGroup(dt, out);
                    break;
                }
                case "/getGroups":{
                   
                    processGetGroups(out);
                    break;
                }
                default:
                    sendFailed(out, -1, "Unknown command");
            }
        } else {
            sendFailed(out, -1, "Unknown command");
        }
    }
    private void processGetGroups(PrintWriter out){
        List<Group> groups = new ArrayList<>();
        if(GroupModel.getInstance().getGroups(groups) == 0){
             sendSuccess(out, Common.GSON.toJsonTree(groups));
        }else{
            LOG.info("Get groups error");
            sendFailed(out, -1);
        }
        
    }
    private void processAddGroup(String dt, PrintWriter out) {
        Group group = Common.GSON.fromJson(dt, Group.class);
        StringBuilder groupInsertId = new StringBuilder();
        if (null == group) {
            LOG.warning("Invalid Segment info");
            sendFailed(out, -1);
            return;
        } else {
            if (GroupModel.getInstance().addGroup(group, groupInsertId) != 0) {
                LOG.warning("Add group failed");
                sendFailed(out, -4);
                return;
            }
            sendSuccess(out);
        }
    }

    private void processUpdateGroup(String dt, PrintWriter out) {
        Group group = Common.GSON.fromJson(dt, Group.class);
        if (null == group) {
            LOG.warning("Invalid Segment info");
            sendFailed(out, -1);
            return;
        } else if (GroupModel.getInstance().updateGroup(group) != 0) {
            LOG.warning("Update site failed");
            sendFailed(out, -4);
            return;
        } else {
            sendSuccess(out);
        }
    }

    private void processDelGroup(String dt, PrintWriter out) {
        Group group = Common.GSON.fromJson(dt, Group.class);
        if (null == group) {
            LOG.warning("Invalid Segment info");
            sendFailed(out, -1);
            return;
        }else{
            if(GroupModel.getInstance().delGroup(group) !=0){
                LOG.warning("Deleted failed");
                sendFailed(out, -3);
                return;
            }
            
            sendSuccess(out);
        }
    }
    
    
    
}
