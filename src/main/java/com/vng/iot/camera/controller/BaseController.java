package com.vng.iot.camera.controller;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 * Created by baohv on 05/07/2017.
 */
public class BaseController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, OPTIONS");
        response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-Auth-Token, X-Csrf-Token, WWW-Authenticate, Authorization");
        response.addHeader("Access-Control-Allow-Credentials", "false");
        response.addHeader("Access-Control-Max-Age", "3600");

    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Logger.getLogger(BaseController.class.getName()).log(Level.INFO, "processRequest");
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            out.print("Default page");
        }
    }

    protected void sendSuccess(PrintWriter out, String msg) {
        sendResponse(out, 0, msg, null);
    }

    protected void sendSuccess(PrintWriter out, String msg, JsonElement data) {
        sendResponse(out, 0, msg, data);
    }

    protected void sendSuccess(PrintWriter out, JsonElement jdt) {
        sendResponse(out, 0, "", jdt);
    }

    protected void sendSuccess(PrintWriter out) {
        sendResponse(out, 0, "", null);
    }

    protected void sendFailed(PrintWriter out, int err, String msg) {
        sendResponse(out, err, msg, null);
    }

    protected void sendFailed(PrintWriter out, int err) {
        sendFailed(out, err, null);
    }

    private void sendResponse(PrintWriter out, int err, String msg, JsonElement jdt) {
        JsonObject jres = new JsonObject();
        jres.addProperty("err", err);
        if (jdt != null) {
            jres.add("dt", jdt);
        }

        if (msg != null) {
            jres.addProperty("msg", msg);
        }

        out.print(jres.toString());
    }

}
