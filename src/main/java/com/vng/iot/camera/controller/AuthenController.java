package com.vng.iot.camera.controller;

import com.vng.iot.camera.common.Define;
import com.vng.iot.camera.data.User;
import com.vng.iot.camera.model.UserModel;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Created by baohv on 13/07/2017.
 */
public class AuthenController extends BaseController {

    private static final Logger LOG = Logger.getLogger(BaseController.class.getName());

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOG.info("Request authorize");
        HttpSession session = request.getSession(false);
        StringBuilder userIdBuilder = new StringBuilder();
        if (checkSession(session, userIdBuilder)) {
            LOG.info("Authorize");
            String userId = session.getAttribute(Define.KEY_SESSION_USER_ID).toString();
            processAuthorizedRequest(request, response, userId);
        } else {
            LOG.warning("Unauthorize request");
            sendFailed(response.getWriter(), -999, "");
        }
       
    }

    /**
     *
     * @param session
     * @return true if logged in session
     */
    protected boolean checkSession(HttpSession session, StringBuilder userIdBuilder) {
        // Check session and response if unauthorize request
        if (session != null) {
            String userId = (String) session.getAttribute(Define.KEY_SESSION_USER_ID);
            if (userId != null) {
                userIdBuilder.append(userId);
                return true;
            }
        }

        return false;
    }

    protected void checkDeviceRequest(HttpServletRequest request, StringBuilder userIdBuilder) {

    }

    protected void processAuthorizedRequest(HttpServletRequest request, HttpServletResponse response, String userId) throws ServletException, IOException {

    }

    protected void processDeviceRequest(HttpServletRequest request, HttpServletResponse response, String userId) throws ServletException, IOException {

    }
}
