package com.vng.iot.camera.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.vng.iot.camera.common.Define;
import com.vng.iot.camera.data.User;
import com.vng.iot.camera.model.UserModel;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.mutable.MutableBoolean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class UserController extends AuthenController {

    private static final Logger LOG = Logger.getLogger(UserController.class.getName());
    private Gson gson = new GsonBuilder().serializeNulls().create();
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Create a session object if it is already not  created.
        LOG.info("Handler user request");

        HttpSession session = request.getSession(true);
        if (session.isNew()) {
            LOG.info("Session is new");
        } else {
            LOG.info("Session is old");
        }

        String cmd = request.getPathInfo();
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        LOG.info(request.getParameter("username"));
        if (null != cmd) {
            switch (cmd) {
                case "/login": {
                    String dt = IOUtils.toString(request.getInputStream());
                    LOG.info("Login data: " + dt);
                    processLogin(dt, session, out);
                    break;
                }
                case "/logout":
                    LOG.info("processLogout");
                    processLogout(session, out);
                    break;
                case "/register":
                    LOG.info("processRegister");
                    String dt = IOUtils.toString(request.getInputStream());
                    processRegister(dt, out);
                    break;
                case "/authorize":
                    LOG.info("processAuthorize");
                    processAuthorize(out, session);
                    break;
                default:
                    sendFailed(out, -1, "Unknown command");
            }
        } else {
            sendFailed(out, -1, "Unknown command");
        }
    }

    private void processRegister(String dt, PrintWriter out) {
        //TODO
        LOG.warning(dt);
        User user = gson.fromJson(dt, User.class);
        StringBuilder InsertId = new StringBuilder();
        
        if(null == user){
            LOG.warning("Add user failed - UserNull");
            sendFailed(out, -1);
        }else {
            if (UserModel.getInstance().addUser(InsertId, user) != 0) {
                LOG.warning("Add userfailed - Query Failed");
                sendFailed(out, -4);
                return;
            }
            sendSuccess(out);
        }
        
        sendSuccess(out);
    }

    private void processLogout(HttpSession session, PrintWriter out) {
        session.invalidate();
        sendSuccess(out);
    }

    // Key1: A^9c\f
    // Tok1: user+pass
    // Key2: nwZ(7k
    private void processLogin(String dt, HttpSession session, PrintWriter out) {
        JsonObject jdt = new JsonParser().parse(dt).getAsJsonObject();
        
        String userName, userPasswd;
        userName = jdt.get("username").getAsString();
        userPasswd = jdt.get("password").getAsString();
        
        // Check session and response if unauthorize request
        User user = new User();
        if (UserModel.getInstance().getUser(userName, userPasswd, user) == 0) {
            // Save userId to session
            session.setAttribute(Define.KEY_SESSION_USER_ID, user.getId());
            
            
            sendSuccess(out, gson.toJson(user));
            LOG.info("Login successfull");
        } else {
            //session.setAttribute(Define.KEY_SESSION_USER_ID,Define.USER_NOT_EXIST);
            sendFailed(out, -999, "Login Failure");
            LOG.info("Login failure");
        }
    }

    private void processAuthorize(PrintWriter out, HttpSession session) {
//        if ( checkSession(session).equals(Define.AUTHORIZE.VALID) )
//            sendSuccess(out,"Authorize valid");
//        else sendFailed(out,-999,"Authorize failure");
    }
}
