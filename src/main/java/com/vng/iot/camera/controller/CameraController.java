package com.vng.iot.camera.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.vng.iot.camera.common.CloudConfig;
import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.common.Define;
import com.vng.iot.camera.data.Camera;
import com.vng.iot.camera.data.CameraMap;
import com.vng.iot.camera.data.Hub;
import com.vng.iot.camera.data.RecordConfig;
import com.vng.iot.camera.data.Recorder;
import com.vng.iot.camera.data.Site;
import com.vng.iot.camera.data.Stream;
import com.vng.iot.camera.model.CameraModel;
import com.vng.iot.camera.model.HubModel;
import com.vng.iot.camera.model.RecordConfigModel;
import com.vng.iot.camera.model.RecordModel;
import com.vng.iot.camera.model.SiteModel;
import com.vng.iot.camera.model.StreamModel;
import com.vng.iot.camera.model.UserModel;
import com.vng.iot.camera.mqtt.MqttConnectBroker;
import com.vng.iot.camera.mqtt.MqttPublisherHelper;
import com.vng.iot.camera.mqtt.StreamModelMqtt;
import com.vng.iot.mqtt.controller.DefineBrokerModel;
import com.vng.iot.mqtt.controller.MqttCameraModel;
import com.vng.iot.mqtt.controller.MqttRecordStreamModel;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.eclipse.paho.client.mqttv3.MqttException;

public class CameraController extends AuthenController {

    private static final Logger LOG = Logger.getLogger(CameraController.class.getName());
    private Gson gson = new GsonBuilder().serializeNulls().create();
    private static int oldHub = 0; 
    @Override
    protected void processAuthorizedRequest(HttpServletRequest request, HttpServletResponse response, String userId) throws ServletException, IOException {
        LOG.info("Request camera api");
        String token = getTokenUserCurrent(request, response);
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        String cmd = request.getPathInfo();
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        switch (cmd) {
            case "/mapget": {
                LOG.info("processLogin");
                processMapGet(out, userId);
                break;
            }
            case "/gets":
                processGetCamera(out, userId);
                break;
            case "/get_hubs":
                processGetHubs(out, userId);
                break;

            case "/add": {
                LOG.info("Add Camera");
                String dt = IOUtils.toString(request.getInputStream());

            
                processAddCamera(out, dt, userId, token);
            
                break;
            }
            case "/udt": {
                String dt = IOUtils.toString(request.getInputStream());
                MqttConnectBroker mqtt;

                processUpdateCamera(out, dt, userId, token);
                break;
            }
            case "/del": {
                String dt = IOUtils.toString(request.getInputStream());
                processDeleteCamera(out, dt, userId);
                break;
            }
            case "/getRecorders": {
                //String dt = IOUtils.toString(request.getInputStream());
                getListRecoder(out, userId);
                break;
            }
            case "/getRecordConfig": {
                String camId = IOUtils.toString(request.getInputStream());
                getListRecordConfig(out, camId);
                break;
            }
            case "/get_days_storage_by_camId": {
                String camId = IOUtils.toString(request.getInputStream());
                processGetDaysStorage(out, camId);
                break;

            }
            case "/get_source_storage_by_camId": {
                String camId = IOUtils.toString(request.getInputStream());
                processGetSourceStorage(out, camId);
                break;
            }
            case "/getRecordStreams": {
                processGetRecordStream(out);
                break;
            }
            case "/getSites": {
                processGetSites(out);
                break;
            }
            default:
                sendFailed(out, -998, "Unknown command");
        }
    }

    private void processGetRecordStream(PrintWriter out) {
        List<RecordConfig> recordStreams = new ArrayList<RecordConfig>();
        if (RecordModel.getInstance().getRecordConfigs(recordStreams) == 0) {
            sendSuccess(out, Common.GSON.toJsonTree(recordStreams));
        } else {
            LOG.info("Get cameras error");
            sendFailed(out, -1);
        }
    }

    private String getTokenUserCurrent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String token = "";
        HttpSession session = request.getSession(false);
        StringBuilder userIdBuilder = new StringBuilder();
        if (checkSession(session, userIdBuilder)) {
            LOG.info("Authorize");
            String userId = session.getAttribute(Define.KEY_SESSION_USER_ID).toString();
            token += UserModel.getInstance().getTokenByUserId(userId);
        }
        return token;
    }

    private int getRecordStreamFromResponse(String url) {

        int index;
        index = url.indexOf("}");

        return index + 1;
    }

    private void getListRecordConfig(PrintWriter out, String camId) {
        RecordConfig re = new RecordConfig();
        if (RecordConfigModel.getInstance().getRecordConfigByCamId(camId, re) == 0) {
            sendSuccess(out, Common.GSON.toJsonTree(re));
        } else {
            LOG.info("Get record config by camId error");
            sendFailed(out, -1);
        }

    }

    private void getListRecoder(PrintWriter out, String userId) {
        List<Recorder> recorders = new ArrayList<Recorder>();
        if (RecordModel.getInstance().getListRecorders(userId, recorders) == 0) {
            sendSuccess(out, Common.GSON.toJsonTree(recorders));
        } else {
            LOG.info("Get cameras error");
            sendFailed(out, -1);
        }
    }

    private void processGetSites(PrintWriter out) {
        List<Site> sites = new ArrayList<Site>();
        if (SiteModel.getInstance().getSites(sites) == 0) {
            sendSuccess(out, Common.GSON.toJsonTree(sites));
        } else {
            LOG.info("Get sites error");
            sendFailed(out, -1);
        }
    }

    private void processDeleteCamera(PrintWriter out, String dt, String userId) {
        Camera camera = Common.GSON.fromJson(dt, Camera.class);
        int outHubId = CameraModel.getInstance().getOlderHubId(Integer.parseInt(camera.getId()));
        if (null == camera) {
            LOG.warning("Invalid camera info");
            sendFailed(out, -1);
            return;
        }
        RecordConfig recordstream = new RecordConfig();
        if (RecordConfigModel.getInstance().getRecordConfigByCamId(camera.getId(), recordstream) != 0) {
            LOG.warning("Get record stream failed");
            sendFailed(out, -10);
            return;
        }
        if (StreamModel.getInstance().deleteStream(camera.getMainStreamId()) != 0) {
            LOG.warning("Delete main stream failed");
            sendFailed(out, -2);
            return;
        }

        if (StreamModel.getInstance().deleteStream(camera.getSubStreamId()) != 0) {
            LOG.warning("Delete sub stream failed");
            sendFailed(out, -3);
            return;
        }

        if (CameraModel.getInstance().deleteCamera(camera.getId()) != 0) {
            LOG.warning("Delete camera failed");
            sendFailed(out, -4);
            return;
        }
        if (RecordConfigModel.getInstance().deleteRecordStream(camera.getId()) != 0) {
            LOG.warning("Delete camera failed");
            sendFailed(out, -5);
            return;
        }
//        if (processPushCameraMqtt(camera, -1) != 0) {
//            LOG.warning("Push Camera to Mqtt Failed");
//            sendFailed(out, -6);
//            return;
//        }
        if (processPushCameraMqttHardCode() != 0) {
            LOG.warning("Push Camera to Mqtt Failed");
            sendFailed(out, -6);
            return;
        }
        
        if (processPushRecordStreamMqtt(camera, recordstream,outHubId) != 0) {
            LOG.warning("Push Record Stream to Mqtt Failed");
            sendFailed(out, -7);
            return;
        }
        sendSuccess(out);
    }

    private void processGetHubs(PrintWriter out, String userId) {
        List<Hub> hubs = new ArrayList<>();
        if (HubModel.getInstance().getHubs(userId, hubs) == 0) {
            for (final Hub hub : hubs) {
                LocalDateTime lastActive = LocalDateTime.parse(hub.getLastActive(), Common.Utils.DTF);
                if (lastActive.plusMinutes(3).isAfter(LocalDateTime.now())) {
                    hub.setOnline(true);
                } else {
                    hub.setOnline(false);
                }
            }
            String json = new Gson().toJson(hubs);
            JsonElement jdt = Common.GSON.toJsonTree(hubs);
            sendSuccess(out, jdt);
        } else {
            sendFailed(out, -1);
        }
    }

    private void processGetCamera(PrintWriter out, String userId) {
        List<Camera> cameras = new ArrayList<Camera>();
        if (CameraModel.getInstance().getCameras(userId, cameras) == 0) {
            sendSuccess(out, Common.GSON.toJsonTree(cameras));
        } else {
            LOG.info("Get cameras error");
            sendFailed(out, -1);
        }
    }

    private void processUpdateCamera(PrintWriter out, String dt, String userId, String token)  {
        int index = getRecordStreamFromResponse(dt);
        String recordstreamData = dt.substring(index);
        String cameraDT = dt.substring(0, index);
       
        Camera camera = Common.GSON.fromJson(cameraDT, Camera.class);

        RecordConfig recordstream = Common.GSON.fromJson(recordstreamData, RecordConfig.class);
        //MqttClientIoT mqtt = new  MqttClientIoT();
        int oldHubId = CameraModel.getInstance().getOlderHubId(Integer.parseInt(camera.getId()));
        
        //mqtt.pushDataToBroker(camera);
        if (null == camera) {
            LOG.warning("Invalid camera info");
            sendFailed(out, -1);
            return;
        }

        if (CameraModel.getInstance().updateCamera(camera.getId(), camera) != 0) {
            LOG.warning("Update camera failed");
            sendFailed(out, -2);
            return;
        }

        if (StreamModel.getInstance().updateStream(camera.getMainStreamId(), generateStream(camera.getId(), camera.getMainStream())) != 0) {
            LOG.warning("Update main stream failed");
            sendFailed(out, -3);
            return;
        }

        if (StreamModel.getInstance().updateStream(camera.getSubStreamId(), generateStream(camera.getId() + "_sub", camera.getSubStream())) != 0) {
            LOG.warning("Update sub stream failed");
            sendFailed(out, -4);
            return;
        }
        if (RecordConfigModel.getInstance().updateRecordConfig(camera.getId(), recordstream) != 0) {
            LOG.warning("Update sub stream failed");
            sendFailed(out, -5);
            return;
        }
//        if (processPushCameraMqtt(camera, outHubId) != 0) {
//            LOG.warning("Push Camera to Mqtt Failed");
//            sendFailed(out, -6);
//            return;
//        }
        if (processPushCameraMqttHardCode() != 0) {
            LOG.warning("Push Camera to Mqtt Failed");
            sendFailed(out, -6);
            return;
        }
        if (processPushRecordStreamMqtt(camera, recordstream,oldHubId) != 0) {
            LOG.warning("Push Record Stream to Mqtt Failed");
            sendFailed(out, -7);
            return;
        }
        sendSuccess(out);
    }

    Stream generateStream(String streamName, String mainStreamUrl) {
        Stream mainStream = new Stream();
        mainStream.setSrc(mainStreamUrl);
        mainStream.setSrcOption("-fflags nobuffer -rtsp_flags prefer_tcp -i");
        mainStream.setTranscode("-c copy -an");
        String rtmpLive = String.format("-f flv rtmp://%s/live/%s", CloudConfig.getInstance().getLiveDomain(), streamName);
        mainStream.setOut(rtmpLive);
        String hlsLive = String.format("https://%s:8998/live", CloudConfig.getInstance().getLiveDomain());
        mainStream.setLiveHls(hlsLive);
        mainStream.setName(streamName);
        return mainStream;
    }

    private void processAddCamera(PrintWriter out, String dt, String userId, String token)  {
        //process add camera
        int index = getRecordStreamFromResponse(dt);
        String recordstreamData = dt.substring(index);
        String cameraDT = dt.substring(0, index);

        Camera camera = Common.GSON.fromJson(cameraDT, Camera.class);

        //int outHubId = CameraModel.getInstance().getOuterHubId(Integer.parseInt(camera.getId()));
        RecordConfig recordstream = Common.GSON.fromJson(recordstreamData, RecordConfig.class);
        if (null == camera) {
            LOG.warning("Invalid camera info");
            sendFailed(out, -4);
            return;
        }

        // Add camera to database
        StringBuilder camInsertId = new StringBuilder();

        if (CameraModel.getInstance().addCamera(userId, camera, camInsertId) == 0) {
            StringBuilder mainStreamInsertId = new StringBuilder();
            if (StreamModel.getInstance().addStream(camInsertId.toString(), generateStream(camInsertId.toString(), camera.getMainStream()), mainStreamInsertId) != 0) {
                LOG.warning("Insert main stream failed");
                sendFailed(out, -2);
                return;
            }

            StringBuilder subStreamInsertId = new StringBuilder();
            if (StreamModel.getInstance().addStream(camInsertId.toString(), generateStream(camInsertId.toString() + "_sub", camera.getSubStream()), subStreamInsertId) != 0) {
                LOG.warning("Insert main stream failed");
                sendFailed(out, -3);
                return;
            }

            if (CameraModel.getInstance().updateCameraStream(camInsertId.toString(), mainStreamInsertId.toString(), subStreamInsertId.toString()) != 0) {
                LOG.warning("Insert main stream failed");
                sendFailed(out, -4);
                return;
            }
            StringBuilder recordStreamInsertId = new StringBuilder();

            if (RecordConfigModel.getInstance().addRecordConfig(camInsertId, recordstream, recordStreamInsertId) != 0) {
                LOG.warning("Insert recordstream failed");
                sendFailed(out, -5);
                return;
            }

//            if (processPushCameraMqtt(camera) != 0) {
//                LOG.warning("Push Camera to Mqtt Failed");
//                sendFailed(out, -6);
//                return;
//            }
            if (processPushCameraMqttHardCode() != 0) {
                LOG.warning("Push Camera to Mqtt Failed");
                sendFailed(out, -6);
                return;
            }
            if (processPushRecordStreamNew(camera, recordstream) != 0) {
                LOG.warning("Push Record Stream to Mqtt Failed");
                sendFailed(out, -7);
                return;
            }
            sendSuccess(out);

        } else {
            LOG.warning("Insert camera failed");
            sendFailed(out, -2);
        }
    }

    private int processPushCameraMqtt(Camera cam)  {
        int ret = -1;
        StringBuilder hubUuid = new StringBuilder();
        if (CameraModel.getInstance().getHubUuidById(cam, hubUuid) != 0) {
            LOG.warning("Get hub UUid failed ");
        } else {
            List<Camera> cameras = new ArrayList<>();
            if (CameraModel.getInstance().getCamerasByHubId(cam.getHubId(), cameras) == 0) {
                MqttCameraModel mqttController = DefineBrokerModel.defineHubMqttController(cameras, hubUuid.toString(), 0, 4);

                String dt = gson.toJson(mqttController, MqttCameraModel.class);
                //String topic = Define.getTopicSetConfigHub(hubUuid.toString());
                String topic = Define.getTopicSetConfigHub(Define.UuidStream);
//                MqttPublisherHelper mqtt = new MqttPublisherHelper();
                if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) != 0) {
                    LOG.warning("Publish to broker failed");
                }

                ret = 0;
            }
        }

        return ret;
    }

    private int processPushRecordStreamNew(Camera camera, RecordConfig recordstream)  {
        int ret = -1;
        StringBuilder hubUuid = new StringBuilder();
        if (CameraModel.getInstance().getHubUuidById(camera, hubUuid) != 0) {
            LOG.warning("Get Uuid Failed ");
        } else {
            List<RecordConfig> recordstreams = new ArrayList<>();

            if (RecordConfigModel.getInstance().getRecordConfigByUuid(recordstreams, hubUuid.toString()) == 0) {
                MqttRecordStreamModel mqttController = DefineBrokerModel.defineRecordMqttController(recordstreams, hubUuid.toString(), 0, 15);

                String dt = gson.toJson(mqttController, MqttRecordStreamModel.class).replaceAll("", "");
                String topic = Define.getTopicRecordStream(hubUuid.toString());
//                MqttPublisherHelper mqtt = new MqttPublisherHelper();
                if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) == 0) {
                    LOG.warning("Publish record stream ");
                    ret = 0;
                }

            } else {
                LOG.warning("Failed to set config record stream to broker when add");
            }
        }

        return ret;
    }

    private int processPushCameraMqttHardCode()  {
        int ret = -1;
        List<Camera> cameras = new ArrayList<>();
        
        if (CameraModel.getInstance().getAllCameras(cameras) != 0) {
            LOG.warning("Get all camera failed");
        }
        MqttCameraModel mqttController = DefineBrokerModel.defineHubMqttController(cameras, Define.UuidStream, 0, 4);

        String dt = gson.toJson(mqttController, MqttCameraModel.class);
        //String topic = Define.getTopicSetConfigHub(hubUuid.toString());

        //hard code uuid push stream 
        String topic = Define.getTopicSetConfigHub(Define.UuidStream);
//        MqttPublisherHelper mqtt = new MqttPublisherHelper();
        if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) != 0) {
            LOG.warning("Publish to broker failed");
        }

        ret = 0;

        return ret;
    }

    private int processPushCameraMqtt(Camera cam, int outHub)  {
        int ret = -1;
        
        int outHubId = outHub;
        StringBuilder hubUuid = new StringBuilder();

        //hard code push all stream to MQTT to Broker
        if (outHubId != 0) {
            if (outHubId == cam.getHubId() || outHub == -1) {

                if (CameraModel.getInstance().getHubUuidById(cam, hubUuid) != 0) {
                    LOG.warning("get Uuid for Camera Mqtt failed");

                } else {
                    //get list cameras by uuid

                    List<Camera> cams = new ArrayList<>();

                    if (CameraModel.getInstance().getCamerasByHubId(cam.getHubId(), cams) == 0) {

                        MqttCameraModel mqttController = DefineBrokerModel.defineHubMqttController(cams, hubUuid.toString(), 0, 4);

                        String dt = gson.toJson(mqttController, MqttCameraModel.class);
                        //String topic = Define.getTopicSetConfigHub(hubUuid.toString());

                        //hard code 
                        String topic = Define.getTopicSetConfigHub(Define.UuidStream);
//                        MqttPublisherHelper mqtt = new MqttPublisherHelper();
                        if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) != 0) {
                            LOG.warning("Publish to broker failed");
                        }

                        ret = 0;
                    } else {
                        LOG.warning("Failed to set config camera to broker");
                    }

                    // hardcode push all stream to MQTT
                    MqttCameraModel mqttController = DefineBrokerModel.defineHubMqttController(cams, hubUuid.toString(), 0, 4);

                    String dt = gson.toJson(mqttController, MqttCameraModel.class);
                    //String topic = Define.getTopicSetConfigHub(hubUuid.toString());

                    //hard code uuid push stream 
                    String topic = Define.getTopicSetConfigHub(Define.UuidStream);
//                    MqttPublisherHelper mqtt = new MqttPublisherHelper();
                    if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) != 0) {
                        LOG.warning("Publish to broker failed");
                    }

                    ret = 0;

                }

            } else {
                if (CameraModel.getInstance().getHubUuidById(cam, hubUuid) != 0) {
                    LOG.warning("get Uuid for Camera Mqtt failed");

                }
                List<Camera> camsAdd = new ArrayList<>();
                List<Camera> camsDel = new ArrayList<>();
                if (CameraModel.getInstance().getCamerasByHubId(cam.getHubId(), camsAdd) == 0) {
                    List<StreamModelMqtt> streams = new ArrayList<StreamModelMqtt>();

                    MqttCameraModel mqttControllerAdd = DefineBrokerModel.defineHubMqttController(camsAdd, hubUuid.toString(), 0, 4);
                    String dtAdd = gson.toJson(mqttControllerAdd, MqttCameraModel.class);
                    //String topicAdd = Define.getTopicSetConfigHub(hubUuid.toString());

                    //hard code
                    String topicAdd = Define.getTopicSetConfigHub(Define.UuidStream);
//                    MqttPublisherHelper mqtt = new MqttPublisherHelper();
                    if (MqttPublisherHelper.getInstance().pushDataToBroker(dtAdd, topicAdd, 1) != 0) {
                        LOG.warning("Publish to broker failed");
                    }

                    Camera camera = new Camera();
                    cam.setHubId(outHubId);
                    StringBuilder uuidAddCameras = new StringBuilder();
                    if (CameraModel.getInstance().getHubUuidById(camera, uuidAddCameras) == 0) {
                        if (CameraModel.getInstance().getCamerasByHubId(outHubId, camsDel) == 1) {

                            StringBuilder uuidOut = new StringBuilder();
                            if (HubModel.getInstance().getUuidByHubId(uuidOut, outHubId) != 0) {
                                LOG.warning("Can not load Uuid By HubId");
                            }

                            List<StreamModelMqtt> streams2 = new ArrayList<StreamModelMqtt>();
                            camsDel.forEach(elements -> {
                                StreamModelMqtt stream = new StreamModelMqtt();
                                stream.setId(elements.getId());
                                stream.setStreamInput("rtsp://sarimi:123456@172.24.126.38:554/1/1");
                                stream.setStreamOutout("rtmp://sarimi.camera.campus.zing.vn:8999/live/" + elements.getId());

                                streams2.add(stream);

                                StreamModelMqtt sub_stream = new StreamModelMqtt();
                                sub_stream.setId(elements.getId() + "_sub");
                                sub_stream.setStreamInput("rtsp://sarimi:123456@172.24.126.38:554/1/2");
                                sub_stream.setStreamOutout("rtmp://sarimi.camera.campus.zing.vn:8999/live/" + elements.getId() + "_sub");

                                streams2.add(sub_stream);
                            });

                            MqttCameraModel mqttControllerDel = DefineBrokerModel.defineHubMqttController(camsDel, hubUuid.toString(), 0, 5);
                            String dtDel = gson.toJson(mqttControllerDel, MqttCameraModel.class);
                            //String topicDel = Define.getTopicSetConfigHub(uuidOut.toString());
                            //hardcode

                            String topicDel = Define.getTopicSetConfigHub(Define.UuidStream);
                            if (MqttPublisherHelper.getInstance().pushDataToBroker(dtDel, topicDel, 1) == 0) {
                                LOG.warning("Publish to broker failed");
                                ret = 0;
                            }
                        }
                    }

                } else {
                    LOG.warning("Failed to Publish Data Update s");
                }
            }

        }

        return ret;
    }

    private int processPushRecordStreamMqtt(Camera cam, RecordConfig recordStream ,int outHubId) {
        int ret = -1;

        StringBuilder hubUuid = new StringBuilder();
        if (CameraModel.getInstance().getHubUuidById(cam, hubUuid) != 0) {
            LOG.warning("get Uuid for Camera Mqtt failed");

        } else {
            //get list cameras by uuid
//            int outHubId = CameraModel.getInstance().getOlderHubId(Integer.parseInt(cam.getId()));

            if (outHubId == cam.getHubId()) {
                List<RecordConfig> recordstreams = new ArrayList<>();

                if (RecordConfigModel.getInstance().getRecordConfigByUuid(recordstreams, hubUuid.toString()) == 0) {
                    MqttRecordStreamModel mqttController = DefineBrokerModel.defineRecordMqttController(recordstreams, hubUuid.toString(), 0, 15);

                    String dt = gson.toJson(mqttController, MqttRecordStreamModel.class).replaceAll("\\\\", "");
                    String topic = Define.getTopicRecordStream(hubUuid.toString());
//                    MqttPublisherHelper mqtt = new MqttPublisherHelper();
                    if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) == 0) {
                        LOG.warning("Publish record stream ");
                        ret = 0;
                    }

                } else {
                    LOG.warning("Failed to set config record stream to broker");
                }
            } else {
                List<RecordConfig> recordstreamsAdd = new ArrayList<>();
                List<RecordConfig> recordstreamsDel = new ArrayList<>();
                StringBuilder outUuid = new StringBuilder();

                if (HubModel.getInstance().getUuidByHubId(outUuid, outHubId) != 0) {
                    LOG.info("Error from get Uuid");
                }
                if (RecordConfigModel.getInstance().getRecordConfigByUuid(recordstreamsAdd, hubUuid.toString()) == 0) {
                    MqttRecordStreamModel mqttController = DefineBrokerModel.defineRecordMqttController(recordstreamsAdd, hubUuid.toString(), 0, 15);

                    String dt = gson.toJson(mqttController, MqttRecordStreamModel.class);
                    String topic = Define.getTopicRecordStream(hubUuid.toString());
//                    MqttPublisherHelper mqtt = new MqttPublisherHelper();
                    if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) != 0) {
                        LOG.warning("Publish record stream failed");
                    }
                    if (RecordConfigModel.getInstance().getRecordConfigByUuid(recordstreamsDel, outUuid.toString()) == 0) {
                        MqttRecordStreamModel mqttControllerDel = DefineBrokerModel.defineRecordMqttController(recordstreamsDel, outUuid.toString(), 0, 16);

                        String dtDel = gson.toJson(mqttControllerDel, MqttRecordStreamModel.class);
                        String topicDel = Define.getTopicRecordStream(outUuid.toString());

                        if (MqttPublisherHelper.getInstance().pushDataToBroker(dtDel, topicDel, 1) == 0) {
                            LOG.warning("Publish record stream");
                            ret = 0;
                        }

                    }
                } else {
                    LOG.warning("Failed to set config record stream to broker");
                }
            }

        }
        
        return ret;
    }

    private void processMapGet(PrintWriter out, String userId) {
        List<CameraMap> cameras = new ArrayList<CameraMap>();
        if (CameraModel.getInstance().getCamerasMap(userId, cameras) == 0) {
            sendSuccess(out, Common.GSON.toJsonTree(cameras));
        } else {
            LOG.info("Get cameras error");
            sendFailed(out, -1);
        }
    }

    private void processGetDaysStorage(PrintWriter out, String camId) {
        RecordConfig recordConfig = new RecordConfig();
        if (RecordConfigModel.getInstance().getDaysStorage(recordConfig, camId) == 0) {
            sendSuccess(out, Common.GSON.toJsonTree(recordConfig));
        } else {
            LOG.info("Get cameras error");
            sendFailed(out, -1);
        }
    }

    private void processGetSourceStorage(PrintWriter out, String camId) {
        Camera camera = new Camera();
        if (CameraModel.getInstance().getSourcesStorage(camera, camId) == 0) {
            sendSuccess(out, Common.GSON.toJsonTree(camera));
        } else {
            LOG.info("Get cameras error");
            sendFailed(out, -1);
        }
    }
}
