/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.mqtt;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vng.iot.camera.common.Define;
import com.vng.iot.camera.data.Camera;
import com.vng.iot.camera.model.CameraModel;
import com.vng.iot.camera.data.RecordConfig;
import com.vng.iot.camera.model.RecordConfigModel;
import com.vng.iot.camera.model.RecordModel;
import com.vng.iot.camera.data.Segment;
import com.vng.iot.mqtt.controller.DataHubListener;
import com.vng.iot.mqtt.controller.DefineBrokerModel;
import com.vng.iot.mqtt.controller.MqttCameraModel;
import com.vng.iot.mqtt.controller.MqttRecordStreamModel;
import com.vng.iot.mqtt.controller.SegmentMessageMqtt;
import com.vng.iot.mqtt.controller.SegmentModelMqtt;
import com.vng.iot.server.CameraCloudBroker;
import org.eclipse.paho.client.mqttv3.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author phuoc
 */
public class MqttSubscriberHelper {

    private static final Logger LOG = Logger.getLogger(MqttSubscriberHelper.class.getName());
    private static Gson gson = new GsonBuilder().serializeNulls().create();
    MqttClient client = MqttConnectBroker.getClient();
    private final String topicAddSegment = Define.getTopicAddSegment();
    private final String topicDeleteSegment = Define.getTopicDeleteSegment();
    private final String topicConfigStream = Define.getTopicConfigCamera();
    private final String topicConfigRecord = Define.getTopicConfigRecord();
    private static ConcurrentHashMap<String, Integer> AddedSegment = new ConcurrentHashMap<>();

    public void subscribeTopic(String topic) throws MqttException {

        client.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable thrwbl) {
                MqttClient client= MqttConnectBroker.getClient();
                try {
                    client.connect();
                } catch (MqttException ex) {
                    Logger.getLogger(MqttSubscriberHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    LOG.warning("Connection Lost Broker" + thrwbl.getMessage());
                   CameraCloudBroker.runBroker();
                   
//                    LOG.info("Reconnect to server success");
                } catch (Exception ex) {
                    LOG.info("Failed to restart connect to broker MQTT:" + ex.getMessage());
                }
                
                
            }

            @Override
            public void messageArrived(String to, MqttMessage mm) throws Exception {
                try {

                    if (Define.getTopicAddSegment().equals(to)) {
                        try {
                            if (processAddSegmentSubscriber(mm) == 0) {
                                LOG.info("Added segment success");
                            } else {
                                LOG.info("Added segment failed ");
                            }
                        } catch (Exception e) {
                            LOG.info("Exception add segment subscriber" + e.getMessage());
                        }

                    } else if (Define.getTopicDeleteSegment().equals(to)) {
                        try {
                            if (processDelSegmentSubscriber(to, mm) == 0) {
                                LOG.warning("Deleted segment success ");
                            }
                        } catch (Exception e) {
                            LOG.info("Exception delz segment subscriber" + e.getMessage());
                        }

                    } else if (topicConfigStream.equals(to)) {
                        try {
                            if (processStreamSubscriber(to, mm) == 0) {
                                LOG.info("Subscribe stream success");
                            }
                        } catch (Exception e) {
                            LOG.info("Exception streamz subscriber" + e.getMessage());

                        }

                    } else if (topicConfigRecord.equals(to)) {
                        try {
                            if (processRecordSubscriber(to, mm) == 0) {
                                LOG.info("Subscribe record success");
                            }
                        } catch (Exception e) {
                            LOG.info("Exception record subscriber" + e.getMessage());

                        }

                    } else {
                        System.out.println("Topic is not exist");
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken imdt) {
                System.out.println(".deliveryComplete()");
            }
        });
        String[] topics = new String[]{topicAddSegment, topicDeleteSegment, topicConfigStream, topicConfigRecord};
        client.subscribe(topics);

    }

    private int processAddSegmentSubscriber(MqttMessage mm) {
        int ret = -1;
        SegmentModelMqtt model = gson.fromJson(mm.toString(), SegmentModelMqtt.class);
        SegmentMessageMqtt segment = model.getDt();
        String fileName = segment.getSegment();
        LOG.info("File name: "+mm.toString());
        if (AddedSegment.get(fileName) != null) {

            ret=-1;
        }
        if (processAddSegment(model, fileName) == 0) {

            AddedSegment.put(fileName, 1);

            ret = 0;
        } else {
            //

        }
        return ret;
    }

    private int processDelSegmentSubscriber(String to, MqttMessage mm) {
        int ret = -1;
        SegmentModelMqtt model = gson.fromJson(mm.toString(), SegmentModelMqtt.class);
        SegmentMessageMqtt segment = model.getDt();
        LOG.info(to + mm + model.getTimestamp());
        if (RecordModel.getInstance().delSegment(segment.getSegment()) == 0) {
            ret = 0;

        }
        return ret;
    }

    private int processRecordSubscriber(String to, MqttMessage mm) {
        int ret = 0;
        DataHubListener data = gson.fromJson(mm.toString(), DataHubListener.class);
        String uudi = data.getDt().getUuid();

        List<RecordConfig> recordconfigs = new ArrayList<>();
        if (RecordConfigModel.getInstance().getRecordConfigByUuid(recordconfigs, uudi) != 0) {
            LOG.warning("Load list Cameras for Hub Listener failed");
            ret = -1;
        } else {
            List<RecordModelMqtt> records = new ArrayList<>();

            //convert cameras to streams
            MqttRecordStreamModel mqttController = DefineBrokerModel.defineBasicRecordMqttController(recordconfigs, uudi, 0, 3);

            String dt = gson.toJson(mqttController, MqttRecordStreamModel.class);
            String topic = Define.getTopicRecordStream(uudi);
            LOG.info("Subcriber: " + topic + dt);
//                            MqttPublisherHelper mqtt = new MqttPublisherHelper();
            if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) != 0) {
                LOG.warning("Publish to broker failed");
                ret = -1;
            }

        }
        return ret;
    }

    private int processStreamSubscriber(String to, MqttMessage mm) {
        int ret = 0;
        DataHubListener data = gson.fromJson(mm.toString(), DataHubListener.class);
        String uudi = data.getDt().getUuid();

        List<Camera> cameras = new ArrayList<>();
        if (CameraModel.getInstance().getAllCameras(cameras) != 0) {
            LOG.warning("Load list Cameras for Hub Listener failed");
            ret = -1;
        } else {
            List<StreamModelMqtt> streams = new ArrayList<>();

            //convert cameras to streams
            MqttCameraModel mqttController = DefineBrokerModel.defineBasicHubMqttController(cameras, Define.UuidStream, 0, 3);

            String dt = gson.toJson(mqttController, MqttCameraModel.class);
            String topic = Define.getTopicGetConfigCamera(Define.UuidStream);
            LOG.info("Topic stream mqtt: " + to + dt);
//                            MqttPublisherHelper mqtt = new MqttPublisherHelper();
            if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) != 0) {
                LOG.warning("Publish to broker failed");
                ret = -1;
            }

        }
        return ret;

    }
    private void reconnectBroker(){
        
    }
    private int processAddSegment(SegmentModelMqtt model, String fileName) {
        int ret = 0;
        try {
            Segment segmentCheck = new Segment();
            if (RecordModel.getInstance().getSegmentByFileName(segmentCheck, fileName) != 0) {
                return -1;
            }
            String filename = model.getDt().getSegment();
            LOG.info(model.getDt().getSegment());
            Segment segment = new Segment();
            String[] paras = filename.split("/");
            String recorderId = paras[3].substring(paras[3].length() - 1);
            String camId = paras[4];

            segment.setCamId(Integer.parseInt(camId));
            segment.setFilename(filename);
            segment.setRecorder(Integer.parseInt(recorderId));
            segment.setTimestamp(model.getTimestamp());
            //add duration
            //segment.setDuration(model.getDuration());
            StringBuilder insertId = new StringBuilder();
            if (RecordModel.getInstance().addSegment(segment, insertId) != 0) {
                LOG.warning("Can't add segment");
                ret = -1;
            }
        } catch (Exception e) {
            LOG.warning("Function Failed " + e.getMessage());
        }

        return ret;
    }

    public static void main(String[] args) {
        System.out.println("record01".substring("record01".length() - 1));
    }

    public void subscribeTopicAddSegment(String topicAddSegment) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void subscribeTopicDelSegment(String topicDeleteSegment) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
