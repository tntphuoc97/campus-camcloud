/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.mqtt;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.common.Define;
import com.vng.iot.camera.data.Camera;
import com.vng.iot.camera.data.Segment;

import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

/**
 *
 * @author phuoc
 */
public class MqttPublisherHelper {

    private static MqttPublisherHelper instance;
    private static final Logger logger = Logger.getLogger(MqttPublisherHelper.class.getName());
    private MqttClient mqttClient;
    private static Gson gson = new GsonBuilder().serializeNulls().create();
    private MqttPublisherHelper() {
        this.mqttClient = MqttConnectBroker.getClient();

    }

    public static MqttPublisherHelper getInstance() {
        if (instance == null) {
            instance = new MqttPublisherHelper();
        }
        return instance;
    }

    public int pushDataToBroker(String dt, String topic, int qos){
        int ret = -1;
        try {
//             this.client = MqttConnectBroker.getClient();

            MqttMessage message = new MqttMessage(dt.getBytes());
            
            message.setQos(qos);
            
            this.getMqttClient().publish(topic, message);
            ret = 0;
            logger.info("Not error to pub data");
        } catch (MqttException me) {
            logger.info("Error push data to Broker :" + me.getMessage());
        }

        return ret;
    }

//    public int pushSegmentToBroker(Segment segment, int qos, String macId) {
//
//        String topic = String.format("vng/camera-cloud/camera/%s/segments", macId);
//        // get segment and hubuuid
//        String segData = gson.toJson(segment,Segment.class);
//       
//        int ret = -1;
//        try {
//            MqttClient client = MqttConnectBroker.getClient();
//
//            MqttMessage message = new MqttMessage(segData.getBytes());
//            message.setQos(qos);
//            client.publish(topic, message);
//            ret = 0;
//        } catch (MqttException me) {
//            logger.info("Error push segment to Broker :" + me.getMessage());
//        }
//
//        return ret;
//    }
//
//    public int pushCameraToBroker(Camera camera, int qos) {
//        int ret = -1;
//        
//        String dt = gson.toJson(camera, Camera.class);
//        
//        String topic = Define.getTopicSetConfigCamera();
//
//        try {
//            MqttClient client = MqttConnectBroker.getClient();
//
//            MqttMessage message = new MqttMessage(dt.getBytes());
//            message.setQos(qos);
//            client.publish(topic, message);
//            ret = 0;
//        } catch (MqttException me) {
//            logger.info("Error push camera to Broker :" + me.getMessage());
//        }
//        return ret;
//    }
//   

    public MqttClient getMqttClient() {
        return mqttClient;
    }

    public void setMqttClient(MqttClient mqttClient) {
        this.mqttClient = mqttClient;
    }
}
