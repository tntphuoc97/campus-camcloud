/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.mqtt;

/**
 *
 * @author phuoc
 */
public class RecordModelMqtt {
    private String id;
    private String recordInput;
    private String segment;
    private String recordPath;
    private int dayStorage;
    private boolean enable;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecordInput() {
        return recordInput;
    }

    public void setRecordInput(String recordInput) {
        this.recordInput = recordInput;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getRecordPath() {
        return recordPath;
    }

    public void setRecordPath(String recordPath) {
        this.recordPath = recordPath;
    }

    public int getDayStorage() {
        return dayStorage;
    }

    public void setDayStorage(int dayStorage) {
        this.dayStorage = dayStorage;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
    
    
}
