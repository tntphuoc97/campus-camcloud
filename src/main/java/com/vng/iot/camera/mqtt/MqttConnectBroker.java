/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.mqtt;

import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 *
 * @author phuoc
 */
public class MqttConnectBroker {

    private String dataAddSegment = "{ \"dt\": { \"segment\": \"/mnt/recorder01/121/20190609/121-1560067207-20190609-150007.mp4\" }, \"msg_id\": 1, \"timestamp\": 1563366563 }";
    private String dataAddCam = "{ \"dt\": { \"segment\": \"/mnt/recorder01/121/20190609/121-1560067207-20190609-150007.mp4\" }, \"msg_id\": 1, \"timestamp\": 1563366563 }";
    private static final Logger logger = Logger.getLogger(MqttConnectBroker.class.getName());
    private static String broker = "tcp://localhost:1883";
    private static String clientId = "2e14bd6a-51d7-476b-9272-f062fffb3f2c";
    private static MemoryPersistence persistence = new MemoryPersistence();
    private static MqttClient _client = null;

    public static MqttClient getClient() {
        if (_client == null || !_client.isConnected()) {
            try {
                _client = new MqttClient(broker, clientId, persistence);
                MqttConnectOptions connOpts = new MqttConnectOptions();
                connOpts.setCleanSession(true);
//                connOpts.setKeepAliveInterval(100);
                connOpts.setAutomaticReconnect(true);
                logger.info("Connecting to broker: " + broker);
                _client.connect(connOpts);
            } catch (MqttException me) {
                logger.warning("Exception to connect Broker: " + me.getMessage());
            }

        }
        return _client;
    }

    public  static void reconnectBroker() {
        try {
            _client = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            logger.info("Connecting to broker: " + broker);
            _client.connect(connOpts);
        } catch (MqttException me) {
            logger.warning("Exception to connect Broker: " + me.getMessage());
        }
    }

}
