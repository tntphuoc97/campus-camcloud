/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.mqtt;

/**
 *
 * @author phuoc
 */
public class StreamModelMqtt {
    private String id;
    private String streamInput;
    private String streamOutput;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStreamInput() {
        return streamInput;
    }

    public void setStreamInput(String streamInput) {
        this.streamInput = streamInput;
    }

    public String getStreamOutout() {
        return streamOutput;
    }

    public void setStreamOutout(String streamOutout) {
        this.streamOutput = streamOutout;
    }
    
}

