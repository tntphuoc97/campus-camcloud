package com.vng.iot.camera.data;

public class CameraMap {
    private String id;
    private String alias;
    private int hubId;
    private int mainEncoder;
    private String address;
    private int subEncoder;
    private String sourceStorage;
    private String streamMain;
    private String streamSub;
    private int siteId;
    private String macAdress;

    public String getMacAdress() {
        return macAdress;
    }

    public void setMacAdress(String macAdress) {
        this.macAdress = macAdress;
    }
    
    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }
    
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getHubId() {
        return hubId;
    }

    public void setHubId(int hubId) {
        this.hubId = hubId;
    }

    public int getMainEncoder() {
        return mainEncoder;
    }

    public void setMainEncoder(int mainEncoder) {
        this.mainEncoder = mainEncoder;
    }

    public int getSubEncoder() {
        return subEncoder;
    }

    public void setSubEncoder(int subEncoder) {
        this.subEncoder = subEncoder;
    }

    

    public String getSourceStorage() {
        return sourceStorage;
    }

    public void setSourceStorage(String sourceStorage) {
        this.sourceStorage = sourceStorage;
    }
    
  

  

    public String getStreamMain() {
        return streamMain;
    }

    public void setStreamMain(String streamMain) {
        this.streamMain = streamMain;
    }

    public String getStreamSub() {
        return streamSub;
    }

    public void setStreamSub(String streamSub) {
        this.streamSub = streamSub;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
