package com.vng.iot.camera.data;

public class RecordConfig {
    private int id;
    private int camId; 
    private String streamName;
    private int segmentLength;
    private int recorderId;
    private int dayStorage;

    private int typeRecordInput;
    public int getDayStorage() {
        return dayStorage;
    }

    public void setDayStorage(int dayStorage) {
        this.dayStorage = dayStorage;
    }
    private int enable;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCamId() {
        return camId;
    }

    public void setCamId(int camId) {
        this.camId = camId;
    }

    public int getRecorderId() {
        return recorderId;
    }

    public void setRecorderId(int recorderId) {
        this.recorderId = recorderId;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }
    public String getStreamName() {
        return streamName;
    }

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }

    public int getSegmentLength() {
        return segmentLength;
    }

    public void setSegmentLength(int segmentLength) {
        this.segmentLength = segmentLength;
    }

    public int getTypeRecordInput() {
        return typeRecordInput;
    }

    public void setTypeRecordInput(int typeRecordInput) {
        this.typeRecordInput = typeRecordInput;
    }
    
}
