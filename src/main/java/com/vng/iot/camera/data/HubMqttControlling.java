/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.data;

/**
 *
 * @author phuoc
 */
public class HubMqttControlling {
    private int id;
    private int hub_id;
    private int sequence_id_stream;
    private int sequence_id_record;
    private String uuid;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHub_id() {
        return hub_id;
    }

    public void setHub_id(int hub_id) {
        this.hub_id = hub_id;
    }

    public int getSequence_id_stream() {
        return sequence_id_stream;
    }

    public void setSequence_id_stream(int sequence_id_stream) {
        this.sequence_id_stream = sequence_id_stream;
    }

    public int getSequence_id_record() {
        return sequence_id_record;
    }

    public void setSequence_id_record(int sequence_id_record) {
        this.sequence_id_record = sequence_id_record;
    }

    
    
    
}
