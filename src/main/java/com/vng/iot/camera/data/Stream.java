package com.vng.iot.camera.data;

public class Stream {
    private String name;
    private String srcOption;
    private String src;
    private String out;
    private String transcode;
    private String liveHls;

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getTranscode() {
        return transcode;
    }

    public void setTranscode(String transcode) {
        this.transcode = transcode;
    }

    public String getOut() {
        return out;
    }

    public void setOut(String out) {
        this.out = out;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSrcOption() {
        return srcOption;
    }

    public void setSrcOption(String srcOption) {
        this.srcOption = srcOption;
    }

    public String getLiveHls() {
        return liveHls;
    }

    public void setLiveHls(String liveHls) {
        this.liveHls = liveHls;
    }
}
