package com.vng.iot.camera.data;

public class Camera {
    private String id;
    private String alias;
    private String address;
    private int hubId;
    private String mainStream;
    private String mainStreamId;
    private int mainEncoder;
    private String subStream;
    private String subStreamId;
    private int subEncoder;
    private String sourceStorage;
    private int siteId;
    private String macAddress;

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
    
    public int getMainEncoder() {
        return mainEncoder;
    }

    public void setMainEncoder(int mainEncoder) {
        this.mainEncoder = mainEncoder;
    }

    public int getSubEncoder() {
        return subEncoder;
    }

    public void setSubEncoder(int subEncoder) {
        this.subEncoder = subEncoder;
    }

    public String getSourceStorage() {
        return sourceStorage;
    }

    public void setSourceStorage(String sourceStorage) {
        this.sourceStorage = sourceStorage;
    }
    
   
    
    
    

   
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    

    public String getMainStream() {
        return mainStream;
    }

    public void setMainStream(String mainStream) {
        this.mainStream = mainStream;
    }

    public String getSubStream() {
        return subStream;
    }

    public void setSubStream(String subStream) {
        this.subStream = subStream;
    }

   
    public int getHubId() {
        return hubId;
    }

    public void setHubId(int hubId) {
        this.hubId = hubId;
    }

    public String getMainStreamId() {
        return mainStreamId;
    }

    public void setMainStreamId(String mainStreamId) {
        this.mainStreamId = mainStreamId;
    }

    public String getSubStreamId() {
        return subStreamId;
    }

    public void setSubStreamId(String subStreamId) {
        this.subStreamId = subStreamId;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }
    
}
