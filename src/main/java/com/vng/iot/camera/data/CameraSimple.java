package com.vng.iot.camera.data;

public class CameraSimple {
    private String alias;
    private String lng;
    private String lat;
    private String icon;
    private String iconCluster;
    private String streamMain;
    private String streamSub;
}
