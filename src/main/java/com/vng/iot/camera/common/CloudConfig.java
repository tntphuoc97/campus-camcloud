package com.vng.iot.camera.common;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class CloudConfig {

    private static CloudConfig ourInstance = new CloudConfig();

    public static CloudConfig getInstance() {
        return ourInstance;
    }

    private CloudConfig() {
    }

    public class AppConfig {

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }

        private String workingDir;
        private String liveDomain;
        private int serverPort;

        private String recordFormat;

        public String getWorkingDir() {
            return workingDir;
        }

        public void setWorkingDir(String workingDir) {
            this.workingDir = workingDir;
        }

        public int getServerPort() {
            return serverPort;
        }

        public void setServerPort(int serverPort) {
            this.serverPort = serverPort;
        }

        private MySqlConfig mysql;

        public MySqlConfig getMysql() {
            return mysql;
        }

        public void setMysql(MySqlConfig mysql) {
            this.mysql = mysql;
        }

        public String getLiveDomain() {
            return liveDomain;
        }

        public void setLiveDomain(String liveDomain) {
            this.liveDomain = liveDomain;
        }

        public String getRecordFormat() {
            return recordFormat;
        }

        public void setRecordFormat(String recordFormat) {
            this.recordFormat = recordFormat;
        }
    }

    public class MySqlConfig {

        private String host;
        private String port;
        private String dbName;
        private String user;
        private String pass;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getPort() {
            return port;
        }

        public void setPort(String port) {
            this.port = port;
        }

        public String getDbName() {
            return dbName;
        }

        public void setDbName(String dbname) {
            this.dbName = dbName;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }
    }

    private AppConfig appConfig = null;

    public AppConfig getAppConfig() {
        return appConfig;
    }

    public String getWorkingDir() {
        return appConfig.getWorkingDir();
    }

    public int getServerPort() {
        return appConfig.getServerPort();
    }

    public MySqlConfig getMySqlConfig() {
        return appConfig.getMysql();
    }

    public String getLiveDomain() {
        return appConfig.getLiveDomain();
    }

    public int setup(String path) {
        try {
            appConfig = Common.GSON.fromJson(new FileReader(path), AppConfig.class);
        } catch (FileNotFoundException e) {
            return -1;
        }

        return 0;
    }

    public static void main(String[] args) throws FileNotFoundException {
        String path = "/zserver/java-projects/camera-cloud/config/cloudconfig.json";
        System.out.println(Common.GSON.fromJson(new FileReader(path), AppConfig.class));

    }
}
