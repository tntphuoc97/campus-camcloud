package com.vng.iot.camera.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.utils.IOUtils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by baohv on 11/07/2017.
 */
public class Common {
    public static final Gson GSON = new GsonBuilder().create();
    private String a = "{ \"dt\": { \"uuid\": \"b0d930ed72243f6636c13227ac2d5b644aeeaa68\" }, \"msg_id\": 1, \"timestamp\": 1565674547 }";
    public static class AppPath {
        private static final String USER_DIR_KEY = ":Xf6{-";

        public static String getUserBaseDir(String userId) {
            return String.format("%s/user/%s", CloudConfig.getInstance().getWorkingDir(), HmacUtils.hmacSha1Hex(userId, USER_DIR_KEY));
        }

        public static String getRootDir(String userBaseDir) {
            return String.format("%s/%s", userBaseDir, "root");
        }

        public static String getArchiveDir(String userBaseDir) {
            return String.format("%s/%s", userBaseDir, "archive");
        }

        public static String getArchiveDirByUserId(String userId) {
            return getArchiveDir(getUserBaseDir(userId));
        }

        public static String getUserRelativeArchive(String userId, String checksum) {
            return String.format("dl/%s/archive/%s.tgz", HmacUtils.hmacSha1Hex(userId, USER_DIR_KEY), checksum);
        }
    }

    public static class DB {
        public static final String TBL_CAMERA = "camera";
        public static final String TBL_STREAM = "stream";
        public static final String TBL_PUBLISHER = "publisher";
        public static final String TBL_RECORDER = "recorder";
        public static final String TBL_RECORD_STREAM = "record_stream";
        public static final String TBL_SEGMENT = "segment";
        public static final String TBL_SITE = "site";
        public static final String TBL_GROUP = "groups";
        public static final String TBL_USER = "user";
        public static final String TBL_HUBMQTT = "hub_controlling";
    }

    public static class Utils {
        public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        public static final DateTimeFormatter DTF = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        public static final int OFFLINE = 3;
        private static Logger LOG = Logger.getLogger(Utils.class.getName());
        public synchronized static String generateUniqueId() {
            return UUID.randomUUID().toString().replaceAll("-", "");
        }

        public static int compressTagGz(String dirPath, String tarGzPath)
        {
            FileOutputStream fOut = null;
            BufferedOutputStream bOut = null;
            GzipCompressorOutputStream gzOut = null;
            TarArchiveOutputStream tOut = null;

            try {
                try {
                    fOut = new FileOutputStream(new File(tarGzPath));
                    bOut = new BufferedOutputStream(fOut);
                    gzOut = new GzipCompressorOutputStream(bOut);
                    tOut = new TarArchiveOutputStream(gzOut);
                    addFileToTarGz(tOut, dirPath, "");
                } finally {
                    tOut.finish();
                    tOut.close();
                    gzOut.close();
                    bOut.close();
                    fOut.close();
                }
            } catch (FileNotFoundException e) {
                LOG.severe("Gen config exception: " + e.toString());
                return -1;
            } catch (IOException e) {
                LOG.severe("Gen config exception: " + e.toString());
                return -2;
            }

            return 0;
        }

        private static void addFileToTarGz(TarArchiveOutputStream tOut, String path, String base)
                throws IOException
        {
            File f = new File(path);
            String entryName = base + f.getName();
            TarArchiveEntry tarEntry = new TarArchiveEntry(f, entryName);
            tOut.putArchiveEntry(tarEntry);

            if (f.isFile()) {
                IOUtils.copy(new FileInputStream(f), tOut);
                tOut.closeArchiveEntry();
            } else {
                tOut.closeArchiveEntry();
                File[] children = f.listFiles();
                if (children != null) {
                    for (File child : children) {
                        addFileToTarGz(tOut, child.getAbsolutePath(), entryName + "/");
                    }
                }
            }
        }
    }
}
