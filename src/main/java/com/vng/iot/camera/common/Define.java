package com.vng.iot.camera.common;

/**
 * Created by sondq on 11/08/2017.
 */
public class Define {

    public static final String CAMERA_TABLE = "cameraconfig";
    public static final String REGION_TABLE = "region";
    public static final String USER_TABLE = "user";
    public static final int USER_NOT_EXIST = -1;
    public static final String KEY_SESSION_USER_ID = "USER_ID";
//    public static  int sequence_id_camera = 0;
//    public static  int sequence_id_record_stream = 0;
    
    public static final String UuidStream = "8a2ed9bb54c9403283eb2b9a92cf54e2";
    public enum AUTHORIZE {
        VALID,
        INVALID
    }

    public static String getTopicSetConfigCamera() {
        return "vng/camera-cloud/camera/set_config";
    }
    public static String getTopicGetConfigCamera(String uuid){
        return String.format("vng/camera-cloud/campus/stream/set_config/%s",uuid);
    }
    public static String getTopicRecordStream(String uuid){
       return String.format("vng/camera-cloud/campus/record/set_config/%s",uuid);
    }
    public static String getTopicAddSegment(){
        return "vng/camera-cloud/campus/segment/add_segment";
       
    }
    public static  String getTopicDeleteSegment(){
        return "vng/camera-cloud/campus/segment/del_segment";
    }
    public static String getTopicSetConfigHub(String uuid){
        return String.format("vng/camera-cloud/campus/stream/set_config/%s",uuid);
    }
    public static String getTopicGetConfigHub(String uuid){
        return String.format("vng/camera-cloud/hub/get_config/%s",uuid);
    }
    public static String getTopicConfigCamera(){
        return "vng/camera-cloud/campus/stream/get_config";
    }
    public static String getTopicConfigRecord(){
        return "vng/camera-cloud/campus/record/get_config";
    }
}