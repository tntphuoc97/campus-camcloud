package com.vng.iot.camera.model;

import com.vng.iot.camera.data.User;
import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.common.Define;
import com.vng.iot.camera.controller.BaseController;
import com.vng.iot.camera.database.MySqlFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 * Created by sondq on 11/08/2017.
 * Contain functions working on mysql - collection user
 */
public class UserModel {
    private static UserModel instance = null;
    private static final Logger LOG = Logger.getLogger(BaseController.class.getName());
    private UserModel() {}
    public static UserModel getInstance(){
        if (instance == null){
            instance = new UserModel();
        }
        return instance;
    }

    /**
     *
     * @param name : username in database
     * @param passwd : password in database
     * @return userID if exist, else return Define.USER_NOT_EXIST
     **/
    public int getUserId(String name, String passwd){
        String query = String.format("SELECT user_id FROM %s WHERE user_name = '%s' AND user_pwd= '%s'", Define.USER_TABLE, name, passwd);
        LOG.info("Query getUser "+query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        int result = Define.USER_NOT_EXIST;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                LOG.info("User Exist ");
                result = rs.getInt("user_id");
            }
            else {
                LOG.info("User Not Exist");
            }
        }
        catch (Exception e){
            LOG.severe("Error when query getUser, err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        return result;
    }
    public int addUser(StringBuilder insertId,User user){
         int ret = -1;
        
         String query = String.format("INSERT INTO %s(username,password ) " +
                        "VALUES ('%s', '%s' )", Common.DB.TBL_USER,
                user.getUsername(), user.getPassword());

        LOG.info("Add cam query: " + query);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS) > 0) {
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    insertId.append(rs.getString(1));
                    ret = 0;
                }
                LOG.info("Insert user success " + insertId.toString());
            }
           
        } catch (Exception ex) {
            LOG.severe("Error while add hub  " + ex.toString());
            ret = -1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        return ret;
    }
    public String getTokenByUserId(String userId){
        int ret = -1;
        String query = String.format("SELECT token FROM %s WHERE id = %d", Define.USER_TABLE, Integer.parseInt(userId));
        LOG.info("Query get Token  "+query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        
        String token = null;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                LOG.info("User Exist ");
                token = rs.getString("token");
               
            }
            else {
                LOG.info("User Not Exist");
            }
        }
        catch (Exception e){
            LOG.severe("Error when query getUser, err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        
        
        return token;
    }
    // key gen tok:
    public int getUser(String username, String passwd, User user) {
        int ret = -1;
        String query = String.format("SELECT id, username, token , role FROM %s WHERE username = '%s' AND password= '%s'", Define.USER_TABLE, username, passwd);
        LOG.info("Query getUser " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                LOG.info("User Exist ");
                user.setId(rs.getString("id"));
                user.setUsername(rs.getString("username"));
                user.setToken(rs.getString("token"));
                user.setRole(rs.getInt("role"));
                ret = 0;
            }
        }
        catch (Exception e){
            LOG.severe("Error when query getUser, err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        return ret;
    }
}
