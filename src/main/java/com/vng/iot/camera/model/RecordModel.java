package com.vng.iot.camera.model;

import com.vng.iot.camera.data.Segment;
import com.vng.iot.camera.data.Recorder;
import com.vng.iot.camera.data.RecordConfig;
import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.database.MySqlFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Logger;

public class RecordModel {
    private static final Logger LOG = Logger.getLogger(RecordModel.class.getName());

    private static RecordModel instance = null;
    public static RecordModel getInstance(){
        if (instance == null) {
            instance = new RecordModel();
        }
        return instance;
    }
    public int getListRecorders(String uid, List<Recorder> listRecorders){
        int ret = -1;
        String query = String.format("SELECT * \n" +
                "FROM " + Common.DB.TBL_RECORDER + "  WHERE "+Common.DB.TBL_RECORDER+".`uid`= " + uid );
        LOG.info("Query get camera " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Recorder recorder = new Recorder();
                recorder.setId(rs.getString("id"));
                recorder.setAlias(rs.getString("alias"));
                recorder.setStoragePath(rs.getString("storage_path"));
                recorder.setAddress(rs.getString("address"));
                recorder.setUuid(rs.getString("uuid"));
                recorder.setUid(rs.getString("uid"));
                
                
                listRecorders.add(recorder);
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get cameras err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

             
        
        return ret;
    }
    public int getRecorderId(String uuid, Recorder rec) {
        int ret = -1;

        String query = String.format("SELECT id, storage_path FROM %s WHERE uuid='%s'", Common.DB.TBL_RECORDER, uuid);

        LOG.info("Query recorder id " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                rec.setId(rs.getString("id"));
                rec.setStoragePath(rs.getString("storage_path"));
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get recorder err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }
    public int getSegmentByFileName(Segment segment,String filename){
        int ret = -1;
        
        
        String query = String.format("SELECT * FROM %s WHERE filename = '%s'", Common.DB.TBL_SEGMENT, filename);

        LOG.info("Query camera " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                segment.setCamId(rs.getInt("cam"));
                segment.setFilename(rs.getString("filename"));
                segment.setTimestamp(rs.getDouble("timestamp"));
                //segment.setDuration(rs.getInt("duration"));
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get cameras err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        
        return ret;
    }
    public int getRecordConfigs(List<RecordConfig> recordConfigs){
        int ret = -1;
        String query = String.format("SELECT * FROM %s ", Common.DB.TBL_RECORD_STREAM);

        LOG.info("Query camera " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                RecordConfig recordConfig = new RecordConfig();
                recordConfig.setCamId(rs.getInt("cam_id"));
                recordConfig.setStreamName(rs.getString("stream_name"));
                recordConfig.setSegmentLength(rs.getInt("segment_length"));
                recordConfig.setTypeRecordInput(rs.getInt("type_record_input"));
                
                
                recordConfigs.add(recordConfig);
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get cameras err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        return ret;
    }
    public int getRecordConfigs(String recorder, List<RecordConfig> recordConfigs) {
        int ret = -1;

        String query = String.format("SELECT stream_name, segment_length , type_record_input FROM %s WHERE recorder = %s;", Common.DB.TBL_RECORD_STREAM, recorder);

        LOG.info("Query camera " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                RecordConfig recordConfig = new RecordConfig();
                recordConfig.setStreamName(rs.getString("stream_name"));
                recordConfig.setSegmentLength(rs.getInt("segment_length"));
                recordConfig.setTypeRecordInput(rs.getInt("type_record_configs"));
                recordConfigs.add(recordConfig);
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get cameras err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int addSegment(Segment segment,StringBuilder insertId) {
        int ret = -1;

        String query = String.format("INSERT INTO %s(cam, recorder, filename, timestamp) " +
                "VALUES (%d, %d, '%s', %f)", Common.DB.TBL_SEGMENT, segment.getCamId(), segment.getRecorder(),segment.getFilename(),segment.getTimestamp());
        LOG.info("Query add segment: "+query);
        
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS) > 0) {
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    insertId.append(rs.getString(1));
                    ret = 0;
                }
                LOG.info("Insert camera success " + insertId.toString());
            }
            
        } catch (Exception ex) {
            LOG.severe("Error while camera stream " + ex.toString());
            ret = -1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int getSegments(String camId, Long from, Long to, List<Segment> segments) {
        int ret = -1;

        String query = String.format("SELECT R.alias, S.filename FROM %s S, %s R WHERE S.recorder = R.id AND cam = %s AND timestamp >= %d and timestamp < %d ORDER BY timestamp ASC ", Common.DB.TBL_SEGMENT, Common.DB.TBL_RECORDER, camId, from, to);

        LOG.info("Query segment " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                Segment segment = new Segment();
                segment.setRecorder(rs.getInt("recorder"));
                segment.setFilename(rs.getString("filename"));
                segments.add(segment);
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get segment err: " + e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return 0;
    }
    public int delSegment(String filename){
        int ret = -1;
        
        String query = String.format("DELETE FROM %s WHERE filename='%s'",
                Common.DB.TBL_SEGMENT, filename );


        LOG.info("Delete segment query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Delete segment success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while delete camera stream " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
        
        
    }
    public int delSegmentByMqttMessage(String message){
        int ret = -1;
        String query = String.format("Delete FROM %s where filename = '%s'", Common.DB.TBL_SEGMENT,message);
        
        LOG.info("Delete segment by mqtt messsge mqtt query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Delete segment success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while delete camera stream " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        
        return ret;
    }
    public int getStreamPathByCamId(StringBuilder path, int camId){
        int ret = -1;
        
        String query = String.format("SELECT src FROM %s WHERE cam = %d AND name = '%s'  ",Common.DB.TBL_STREAM,camId , String.valueOf(camId));

        LOG.info("Query segment " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                path.append(rs.getString("src"));
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get segment err: " + e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        
        
        return ret;
    }
    public int getStreamLiveByCamId(StringBuilder path, int camId){
        int ret = -1;
        
        String query = String.format("SELECT live FROM %s WHERE cam = %d AND name = '%s'  ",Common.DB.TBL_STREAM,camId , String.valueOf(camId));

        LOG.info("Query segment " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                path.append(rs.getString("live"));
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get segment err: " + e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        
        
        return ret;
    }
}
