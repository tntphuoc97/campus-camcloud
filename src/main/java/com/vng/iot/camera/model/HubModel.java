package com.vng.iot.camera.model;

import com.vng.iot.camera.data.Hub;
import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.database.MySqlFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class HubModel {
    private static final Logger LOG = Logger.getLogger(HubModel.class.getName());

    private static HubModel instance = null;
    public static HubModel getInstance(){
        if (instance == null) {
            instance = new HubModel();
        }
        return instance;
    }
    
    public int getHubId(String uuid, Hub hub) {
        int ret = -1;

        String query = String.format("SELECT id FROM %s WHERE uuid='%s'", Common.DB.TBL_PUBLISHER, uuid);

        LOG.info("Query hub id " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                hub.setId(rs.getString("id"));
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get cameras err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }
    public int getUuids(List<String> uuids){
        int ret = -1;
        String query = String.format("SELECT uuid from %s", Common.DB.TBL_PUBLISHER);

        LOG.info("Query uuids hub " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                uuids.add(rs.getString("uuid"));
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get uuids err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        
        return ret;
    }
    public int getHubsWithoutLogin(List<Hub> hubs){
        int ret = -1;
        String query = String.format("SELECT id, last_active, alias, uuid FROM %s ", Common.DB.TBL_PUBLISHER);

        LOG.info("Query hub without login" + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Hub hub = new Hub();
                hub.setId(rs.getString("id"));
                hub.setAlias(rs.getString("alias"));
                hub.setLastActive(Common.Utils.SDF.format(new Date(rs.getTimestamp("last_active").getTime())));
                hub.setUuid(rs.getString("uuid"));

                hubs.add(hub);
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get hubs err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        
        return ret;
    }
    public int getHubs(String userId, List<Hub> hubs) {
        int ret = -1;

        String query = String.format("SELECT id, last_active, alias, uuid FROM %s WHERE uid=%s", Common.DB.TBL_PUBLISHER, userId);

        LOG.info("Query hub " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Hub hub = new Hub();
                hub.setId(rs.getString("id"));
                hub.setAlias(rs.getString("alias"));
                hub.setLastActive(Common.Utils.SDF.format(new Date(rs.getTimestamp("last_active").getTime())));
                hub.setUuid(rs.getString("uuid"));

                hubs.add(hub);
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get hubs err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }


        return ret;
    }

    public int updateLastUpdateHub(String hubId) {
        int ret = -1;

        String query = String.format("UPDATE %s SET last_active=now() WHERE id=%s", Common.DB.TBL_PUBLISHER, hubId);

        LOG.info("Update hub active query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Update hub success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while execute hub " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }
    public int getUuidByCamId( StringBuilder uuid,String camId){
        int ret = -1;
        String query = String.format("select uuid from %s where id = (select hub from %s where id = %d );",Common.DB.TBL_PUBLISHER,Common.DB.TBL_CAMERA,Integer.parseInt(camId));
        
        LOG.info("Query getUuidByCamId " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                 uuid.append(rs.getString("uuid"));
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get uuid by camId err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        
        return ret;
    }
    public int getUuidByHubId(StringBuilder uuid,int hubId){
        int ret = -1;
        String query = String.format("SELECT uuid FROM %s WHERE id= %d", Common.DB.TBL_PUBLISHER, hubId);

        LOG.info("Query hub id " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                uuid.append(rs.getString("uuid"));
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get cameras err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        return ret;
    }
    public int addHub(String userId,Hub hub,StringBuilder insertId){
        int ret = -1;
        String query = String.format("INSERT INTO %s(uid,alias,uuid) " +
                        "VALUES (%d, '%s', '%s' )", Common.DB.TBL_PUBLISHER,
                Integer.parseInt(userId),hub.getAlias(), hub.getUuid());

        LOG.info("Add cam query: " + query);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS) > 0) {
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    insertId.append(rs.getString(1));
                    ret = 0;
                }
                LOG.info("Insert hub success " + insertId.toString());
            }
            hub.setId(insertId.toString());
        } catch (Exception ex) {
            LOG.severe("Error while add hub  " + ex.toString());
            ret = -1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        return ret;
    }
}
