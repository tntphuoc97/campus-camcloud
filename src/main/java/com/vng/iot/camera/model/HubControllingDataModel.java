/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.model;

import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.data.Group;
import com.vng.iot.camera.data.Hub;
import com.vng.iot.camera.data.HubMqttControlling;
import com.vng.iot.camera.database.MySqlFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 *
 * @author phuoc
 */
public class HubControllingDataModel {

    private static final Logger LOG = Logger.getLogger(CameraModel.class.getName());
    private static HubControllingDataModel instance = null;

    public static HubControllingDataModel getInstance() {
        if (instance == null) {
            instance = new HubControllingDataModel();
        }
        return instance;
    }
    public int addHubControllingData (Hub data,StringBuilder insertId){
        int ret = -1;
        String query = String.format("INSERT INTO %s(hub_id,sequence_id_cam, sequence_id_record,uuid) " +
                        "VALUES (%d,%d,%d, '%s' )", Common.DB.TBL_HUBMQTT,
               Integer.parseInt(data.getId()),0,0,data.getUuid());

        LOG.info("Add cam query: " + query);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS) > 0) {
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    insertId.append(rs.getString(1));
                    ret = 0;
                }
                LOG.info("Insert site success " + insertId.toString());
            }
          
        } catch (Exception ex) {
            LOG.severe("Error while add hub  " + ex.toString());
            ret = -1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        
        return ret;
        
        
    }
    public int getHubControlingDataByHubId(HubMqttControlling data, int hubId) {
        int ret = -1;

        String query = String.format("Select * from %s where hub_id= %d", Common.DB.TBL_HUBMQTT, hubId);

        LOG.info("Query get Hub data mqtt : " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                data.setId(rs.getInt("id"));
                data.setHub_id(rs.getInt("hub_id"));
                data.setSequence_id_stream(rs.getInt("sequence_id_cam"));
                data.setSequence_id_record(rs.getInt("sequence_id_record"));
                data.setUuid(rs.getString("uuid"));
            }

            ret = 0;
        } catch (Exception e) {
            LOG.severe("Error when query get hub data Mqtt err: " + e);
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;

    }

    public int getSequenceIdStream(int hubId) {
        int ret = -1;
        String query = String.format("Select sequence_id_cam from %s where hub_id= %d", Common.DB.TBL_HUBMQTT, hubId);

        LOG.info("Query get sequence_id : " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                ret = rs.getInt("sequence_id_cam");
            }

        } catch (Exception e) {
            LOG.severe("Error when query get sequence_id err: " + e);
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }
    public int getSequenceIdRecord(int hubId) {
        int ret = -1;
        String query = String.format("Select sequence_id_record from %s where hub_id= %d", Common.DB.TBL_HUBMQTT, hubId);

        LOG.info("Query get sequence_id_record : " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                ret = rs.getInt("sequence_id_record");
            }

        } catch (Exception e) {
            LOG.severe("Error when query get sequence_id err: " + e);
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int addSequenceId(HubMqttControlling data) {
        int ret = -1;
        String query = String.format("UPDATE %s SET sequence_id_cam = %d, sequence_id_record = %d WHERE uuid = '%s'",
                Common.DB.TBL_HUBMQTT, data.getSequence_id_stream(), data.getSequence_id_record(),data.getUuid());

        LOG.info("Update Hub Controlling query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Update Hub controlling success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while update Hub control " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }
    public int getHubControlingDataByUuid(HubMqttControlling data, String uuid) {
        int ret = -1;

        String query = String.format("Select * from %s where uuid= '%s'", Common.DB.TBL_HUBMQTT, uuid);

        LOG.info("Query get Hub data mqtt : " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                data.setId(rs.getInt("id"));
                data.setHub_id(rs.getInt("hub_id"));
                data.setSequence_id_stream(rs.getInt("sequence_id_cam"));
                data.setSequence_id_record(rs.getInt("sequence_id_record"));
                data.setUuid(rs.getString("uuid"));
            }

            ret = 0;
        } catch (Exception e) {
            LOG.severe("Error when query get hub data Mqtt err: " + e);
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;

    }

    

}
