/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.model;

import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.data.Group;
import com.vng.iot.camera.data.Site;
import com.vng.iot.camera.database.MySqlFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author phuoc
 */
public class GroupModel {
    private static final Logger LOG = Logger.getLogger(CameraModel.class.getName());
    private static GroupModel instance = null;

    public static GroupModel getInstance(){
        if (instance == null) {
            instance = new GroupModel();
        }
        return instance;
    }
    
    public int getGroups (List<Group> groups){
       int ret = -1;
        String query = String.format("SELECT * FROM %s", Common.DB.TBL_GROUP);

        LOG.info("Query get groups " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Group group = new Group();
                group.setId(rs.getInt("id"));
                group.setAlias(rs.getString("alias"));
                group.setDescription(rs.getString("description"));
                
                groups.add(group);
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get groups err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

       return ret;
    }
    public int addGroup(Group group, StringBuilder insertId){
        int ret = -1;
        String query = String.format("INSERT INTO %s(alias, description) " +
                        "VALUES ('%s', '%s' )", Common.DB.TBL_GROUP,
                group.getAlias(), group.getDescription());

        LOG.info("Add group query: " + query);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS) > 0) {
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    insertId.append(rs.getString(1));
                    ret = 0;
                }
                LOG.info("Insert group success " + insertId.toString());
            }
           group.setId(Integer.parseInt(insertId.toString()));
        } catch (Exception ex) {
            LOG.severe("Error while add group:  " + ex.toString());
            ret = -1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        return ret;
    }
    public int updateGroup(Group group){
        int ret = -1;
        String query = String.format("UPDATE %s SET alias='%s', description='%s' WHERE id= %d",
                Common.DB.TBL_GROUP, group.getAlias(), group.getDescription(),group.getId());


        LOG.info("Update group query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Update group success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while update group " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        return ret;
    }
    public int delGroup(Group group){
        int ret = -1;
        
        String query = String.format("DELETE FROM %s WHERE id= %d",
                Common.DB.TBL_GROUP, group.getId());


        LOG.info("Delete group query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Delete group success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while delete group " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }
}
