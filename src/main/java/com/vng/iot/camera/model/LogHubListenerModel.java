/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.model;

import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.database.MySqlFactory;
import com.vng.iot.mqtt.controller.DataHubListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 *
 * @author phuoc
 */
public class LogHubListenerModel {
    
    private static final Logger LOG = Logger.getLogger(LogHubListenerModel.class.getName());
    private LogHubListenerModel instance = null;
    
    public LogHubListenerModel getInstance()
    {
        if(instance == null){
            instance = new LogHubListenerModel();
        }
        return instance;
    }
    
    public int addLogs(DataHubListener dt,StringBuilder insertId){
        int ret  =-1;
        
        String query = String.format("INSERT INTO %s(alias, address, hub, uid, main_encoder, sub_encoder,source_storage,site_id ,mac_address ) " +
                        "VALUES ('%s', '%s', '%s','%s', %d, %d, '%s' )", Common.DB.TBL_CAMERA);

        LOG.info("Add log query: " + query);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS) > 0) {
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    insertId.append(rs.getString(1));
                    ret = 0;
                }
                LOG.info("Insert camera success " + insertId.toString());
            }
            
        } catch (Exception ex) {
            LOG.severe("Error while camera stream " + ex.toString());
            ret = -1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        
        
        return ret;
    }
}
