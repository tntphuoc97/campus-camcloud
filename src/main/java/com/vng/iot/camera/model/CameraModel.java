package com.vng.iot.camera.model;

import com.vng.iot.camera.data.Camera;
import com.vng.iot.camera.data.CameraMap;
import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.database.MySqlFactory;
import com.vng.iot.camera.mqtt.StreamModelMqtt;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Logger;

public class CameraModel {

    private static final Logger LOG = Logger.getLogger(CameraModel.class.getName());
    private static CameraModel instance = null;

    public static CameraModel getInstance() {
        if (instance == null) {
            instance = new CameraModel();
        }
        return instance;
    }

    public int getCamerasMap(String userId, List<CameraMap> cameras) {
        int ret = -1;

        //String query = String.format("SELECT alias, address, lng, lat, icon, icon_cluster, main_stream, sub_stream FROM %s WHERE uid='%s'", Common.DB.TBL_CAMERA, userId);
//        String query = String.format("SELECT C.id, C.alias, C.address,C.hub, C.main_encoder, C.sub_encoder, C.source_storage, C.site_id,C.mac_address,  S1.name as main_stream, S2.name as sub_stream, S1.live_hls as main_live , S2.live_hls as sub_live \n" +
//                "FROM (SELECT id, alias, address, hub, main_encoder, sub_encoder, source_storage,site_id, mac_address , main_stream, sub_stream FROM " + Common.DB.TBL_CAMERA +" WHERE uid='" + userId + " ') as C\n" +
//                "INNER JOIN stream S1 ON S1.id = C.main_stream\n" +
//                "INNER JOIN stream S2 ON S2.id = C.sub_stream;");
        //query get all cameras map in DB
        String query = String.format("SELECT C.id, C.alias, C.address,C.hub, C.main_encoder, C.sub_encoder, C.source_storage, C.site_id,C.mac_address,  S1.name as main_stream, S2.name as sub_stream, S1.live_hls as main_live , S2.live_hls as sub_live \n"
                + "FROM (SELECT id, alias, address, hub, main_encoder, sub_encoder, source_storage,site_id, mac_address , main_stream, sub_stream FROM " + Common.DB.TBL_CAMERA + " WHERE uid !='" + "0" + " ') as C\n"
                + "INNER JOIN stream S1 ON S1.id = C.main_stream\n"
                + "INNER JOIN stream S2 ON S2.id = C.sub_stream;");
        LOG.info("Query camera " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                CameraMap camera = new CameraMap();
                camera.setId(rs.getString("id"));
                camera.setAlias(rs.getString("alias"));
                camera.setAddress(rs.getString("address"));
                camera.setHubId(rs.getInt("hub"));
                camera.setMainEncoder(rs.getInt("main_encoder"));
                camera.setSubEncoder(rs.getInt("sub_encoder"));
                camera.setSourceStorage(rs.getString("source_storage"));
                camera.setSiteId(rs.getInt("site_id"));
                camera.setMacAdress(rs.getString("mac_address"));
                //camera.setIconCluster(rs.getString("icon_cluster"));userId
                camera.setStreamMain(String.format("%s/%s/index.m3u8", rs.getString("main_live"), rs.getString("main_stream")));
                camera.setStreamSub(String.format("%s/%s/index.m3u8", rs.getString("sub_live"), rs.getString("sub_stream")));

                cameras.add(camera);
            }

            ret = 0;
        } catch (Exception e) {
            LOG.severe("Error when query get cameras err: " + e);
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int getCameras(String userId, List<Camera> cameras) {
        int ret = -1;

        String query = String.format("SELECT C.id, C.alias, C.address, C.hub, C.main_stream, C.sub_stream, C.main_encoder, C.sub_encoder, C.source_storage,C.site_id,C.mac_address , S1.src as main_stream_src, S2.src as sub_stream_src \n"
                + "FROM (SELECT id, alias, address, main_stream, sub_stream, hub, main_encoder, sub_encoder, source_storage, site_id , mac_address FROM " + Common.DB.TBL_CAMERA + " WHERE uid='" + userId + " ') as C\n"
                + "INNER JOIN stream S1 ON S1.id = C.main_stream\n"
                + "INNER JOIN stream S2 ON S2.id = C.sub_stream;");

        LOG.info("Query get cameras " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Camera camera = new Camera();
                camera.setId(rs.getString("id"));
                camera.setAlias(rs.getString("alias"));

//                camera.setLng(rs.getString("lng"));
//                camera.setLat(rs.getString("lat"));
//                camera.setIcon(rs.getString("icon"));
                camera.setMainStreamId(rs.getString("main_stream"));
                camera.setMainStream(rs.getString("main_stream_src"));
                camera.setSubStreamId(rs.getString("sub_stream"));
                camera.setSubStream(rs.getString("sub_stream_src"));
                camera.setHubId(rs.getInt("hub"));
                camera.setMainEncoder(rs.getInt("main_encoder"));
                camera.setSubEncoder(rs.getInt("sub_encoder"));
                camera.setSourceStorage(rs.getString("source_storage"));
                camera.setSiteId(rs.getInt("site_id"));
                camera.setMacAddress(rs.getString("mac_address"));
                cameras.add(camera);
            }

            ret = 0;
        } catch (Exception e) {
            LOG.severe("Error when query get cameras err: " + e);
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int getCamerasByHubId(int hubId, List<Camera> cameras) {
        int ret = -1;

        String query = String.format("select * from %s where hub = '%s'", Common.DB.TBL_CAMERA, hubId);

        LOG.info("Query get cameras " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Camera camera = new Camera();
                camera.setId(rs.getString("id"));
                camera.setAlias(rs.getString("alias"));

//                camera.setLng(rs.getString("lng"));
//                camera.setLat(rs.getString("lat"));
//                camera.setIcon(rs.getString("icon"));
//                camera.setMainStreamId(rs.getString("main_stream"));
                camera.setMainStream(rs.getString("main_stream"));
                camera.setSubStreamId(rs.getString("sub_stream"));
                camera.setSiteId(rs.getInt("site_id"));
                camera.setHubId(rs.getInt("hub"));
                camera.setMainEncoder(rs.getInt("main_encoder"));
                camera.setSubEncoder(rs.getInt("sub_encoder"));
                camera.setSourceStorage(rs.getString("source_storage"));
                camera.setMacAddress(rs.getString("mac_address"));
                cameras.add(camera);
            }

            ret = 0;
        } catch (Exception e) {
            LOG.severe("Error when query get cameras err: " + e);
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int getAllCameras(List<Camera> cameras) {
        int ret = -1;

        String query = String.format("select * from %s ", Common.DB.TBL_CAMERA);

        LOG.info("Query get cameras " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Camera camera = new Camera();
                camera.setId(rs.getString("id"));
                camera.setAlias(rs.getString("alias"));

//                camera.setLng(rs.getString("lng"));
//                camera.setLat(rs.getString("lat"));
//                camera.setIcon(rs.getString("icon"));
//                camera.setMainStreamId(rs.getString("main_stream"));
                camera.setMainStream(rs.getString("main_stream"));
                camera.setSubStreamId(rs.getString("sub_stream"));
                camera.setSiteId(rs.getInt("site_id"));
                camera.setHubId(rs.getInt("hub"));
                camera.setMainEncoder(rs.getInt("main_encoder"));
                camera.setSubEncoder(rs.getInt("sub_encoder"));
                camera.setSourceStorage(rs.getString("source_storage"));
                camera.setMacAddress(rs.getString("mac_address"));
                cameras.add(camera);
            }

            ret = 0;
        } catch (Exception e) {
            LOG.severe("Error when query get cameras err: " + e);
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int getCamerasByUuid(String uuid, List<Camera> cameras) {
        int ret = -1;
        String query = String.format("select * from %s where hub = (select id from %s where uuid = '%s')", Common.DB.TBL_CAMERA, Common.DB.TBL_PUBLISHER, uuid);

        LOG.info("Query get cameras by uuid " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Camera camera = new Camera();
                camera.setId(rs.getString("id"));
                camera.setAlias(rs.getString("alias"));

//                camera.setLng(rs.getString("lng"));
//                camera.setLat(rs.getString("lat"));
//                camera.setIcon(rs.getString("icon"));
//                camera.setMainStreamId(rs.getString("main_stream"));
                camera.setMainStream(rs.getString("main_stream"));
                camera.setSubStreamId(rs.getString("sub_stream"));

                camera.setHubId(rs.getInt("hub"));
                camera.setMainEncoder(rs.getInt("main_encoder"));
                camera.setSubEncoder(rs.getInt("sub_encoder"));
                camera.setSourceStorage(rs.getString("source_storage"));
                camera.setSiteId(rs.getInt("site_id"));
                camera.setMacAddress(rs.getString("mac_address"));

                cameras.add(camera);
            }

            ret = 0;
        } catch (Exception e) {
            LOG.severe("Error when query get cameras by uuid err: " + e);
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int addCamera(String userId, Camera camera, StringBuilder insertId) {
        int ret = -1;

        String query = String.format("INSERT INTO %s(alias, address, hub, uid, main_encoder, sub_encoder,source_storage,site_id ,mac_address ) "
                + "VALUES ('%s', '%s', '%s','%s', %d, %d, '%s',%d, '%s')", Common.DB.TBL_CAMERA,
                camera.getAlias(), camera.getAddress(), camera.getHubId(), userId, camera.getMainEncoder(), camera.getSubEncoder(), camera.getSourceStorage(), camera.getSiteId(), camera.getMacAddress());

        LOG.info("Add cam query: " + query);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS) > 0) {
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    insertId.append(rs.getString(1));
                    ret = 0;
                }
                LOG.info("Insert camera success " + insertId.toString());
            }
            camera.setId(insertId.toString());
        } catch (Exception ex) {
            LOG.severe("Error while camera stream " + ex.toString());
            ret = -1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int updateCamera(String id, Camera camera) {
        int ret = -1;
        String query = String.format("UPDATE %s SET alias='%s', address='%s', hub=%s, main_encoder =%d, sub_encoder=%d, source_storage='%s', site_id = %d , mac_address ='%s' WHERE id='%s'",
                Common.DB.TBL_CAMERA, camera.getAlias(), camera.getAddress(), camera.getHubId(), camera.getMainEncoder(), camera.getSubEncoder(), camera.getSourceStorage(), camera.getSiteId(), camera.getMacAddress(), id);

        LOG.info("Update camera stream query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Update camera stream success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while update camera stream " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;

    }

    public int deleteCamera(String id) {
        int ret = -1;
        String query = String.format("DELETE FROM %s WHERE id='%s'",
                Common.DB.TBL_CAMERA, id);

        LOG.info("Delete camera stream query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Delete camera stream success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while delete camera stream " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;

    }

    public int updateCameraStream(String id, String mainStream, String subStream) {
        int ret = -1;
        String query = String.format("UPDATE %s SET main_stream=%s, sub_stream=%s WHERE id='%s'",
                Common.DB.TBL_CAMERA, mainStream, subStream, id);

        LOG.info("Update camera stream query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Update camera stream success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while update camera stream " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;

    }

    public int getHubUuidById(Camera cam, StringBuilder uuid) {
        int ret = -1;
        String query = String.format("SELECT uuid from %s where id = %d ", Common.DB.TBL_PUBLISHER, cam.getHubId());

        LOG.info("get Uuid by camera " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                uuid.append(rs.getString("uuid"));
            }

            ret = 0;
        } catch (Exception e) {
            LOG.severe("Error when query get cameras err: " + e);
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;

    }

    public int getOlderHubId(int camId) {
        int ret = 0;
        String query = String.format("SELECT hub from %s WHERE id = %d",
                Common.DB.TBL_CAMERA, camId);

        LOG.info("Update get source storage query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {

                ret = rs.getInt("hub");
            }

        } catch (Exception ex) {
            LOG.severe("Error while get source storage " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        return ret;
    }

    

    public int getSourcesStorage(Camera cam, String camId) {
        int ret = -1;

        String query = String.format("SELECT id,source_storage from %s WHERE id = %d ",
                Common.DB.TBL_CAMERA, Integer.parseInt(camId));

        LOG.info("Update get source storage query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {

                cam.setId(rs.getString("id"));
                cam.setSourceStorage(rs.getString("source_storage"));
            }

            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while get source storage " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int getMainStreamByCamId(StreamModelMqtt stream, Camera cam) {
        int ret = -1;
        String query = String.format("SELECT * from %s WHERE cam = %d AND name = '%s'",
                Common.DB.TBL_STREAM, Integer.parseInt(cam.getId()), cam.getId());

        LOG.info("Update get source storage query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                stream.setId(cam.getId());
                stream.setStreamInput(rs.getString("src"));
                stream.setStreamOutout(rs.getString("live").replaceAll("-f flv ", ""));
                //stream.setStreamOutout("rtmp://sarimi.camera.campus.zing.vn:8999/live/"+cam.getId());
            }

            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while get source storage " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int getSubStreamByCamId(StreamModelMqtt stream, Camera cam) {
        int ret = -1;
        String query = String.format("SELECT * from %s WHERE cam = %d AND name = '%s'",
                Common.DB.TBL_STREAM, Integer.parseInt(cam.getId()), cam.getId() + "_sub");

        LOG.info("Update src in stream query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                stream.setId(cam.getId() + "_sub");
                stream.setStreamInput(rs.getString("src"));
                stream.setStreamOutout(rs.getString("live").replaceAll("-f flv ", ""));
                //stream.setStreamOutout("rtmp://sarimi.camera.campus.zing.vn:8999/live/"+cam.getId()+"_sub");
            }

            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while get substream " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }
}
