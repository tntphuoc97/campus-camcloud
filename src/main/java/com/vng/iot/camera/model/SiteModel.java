/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.model;

import com.vng.iot.camera.data.Site;
import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.database.MySqlFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author phuoc
 */
public class SiteModel {
    private static final Logger LOG = Logger.getLogger(CameraModel.class.getName());
    private static SiteModel instance = null;

    public static SiteModel getInstance(){
        if (instance == null) {
            instance = new SiteModel();
        }
        return instance;
    }
    public int getSites(List<Site> sites){
        int ret = -1;
        
        String query = String.format("SELECT * FROM %s", Common.DB.TBL_SITE);

        LOG.info("Query hub " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Site site = new Site();
                site.setId(rs.getInt("id"));
                site.setSite_name(rs.getString("site_name"));
                site.setDescription(rs.getString("description"));
                
                sites.add(site);
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get hubs err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }
    public int addSite(Site site, StringBuilder insertId){
        int ret = -1;
        
         String query = String.format("INSERT INTO %s(site_name, description) " +
                        "VALUES ('%s', '%s' )", Common.DB.TBL_SITE,
                site.getSite_name(), site.getDescription());

        LOG.info("Add cam query: " + query);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS) > 0) {
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    insertId.append(rs.getString(1));
                    ret = 0;
                }
                LOG.info("Insert site success " + insertId.toString());
            }
           site.setId(Integer.parseInt(insertId.toString()));
        } catch (Exception ex) {
            LOG.severe("Error while add hub  " + ex.toString());
            ret = -1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }
    public int updateSite(Site site){
        int ret  = -1;
        
        String query = String.format("UPDATE %s SET site_name='%s', description='%s' WHERE id= %d",
                Common.DB.TBL_SITE, site.getSite_name(), site.getDescription(),site.getId());


        LOG.info("Update site query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Update camera stream success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while update camera stream " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        return ret;
    }
    public int delSite(Site site){
        int ret = -1;
        String query = String.format("DELETE FROM %s WHERE id= %d",
                Common.DB.TBL_SITE, site.getId());


        LOG.info("Delete site query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Delete site success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while update stream " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }
    
}

