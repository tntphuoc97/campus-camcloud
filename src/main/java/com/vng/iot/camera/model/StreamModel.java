package com.vng.iot.camera.model;

import com.vng.iot.camera.data.Stream;
import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.controller.BaseController;
import com.vng.iot.camera.database.MySqlFactory;
import org.apache.commons.lang.mutable.MutableInt;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Logger;

public class StreamModel {
    private static final Logger LOG = Logger.getLogger(StreamModel.class.getName());

    private static StreamModel instance = null;
    public static StreamModel getInstance(){
        if (instance == null) {
            instance = new StreamModel();
        }
        return instance;
    }

    public int getStreams(String publisher, List<Stream> streams) {
        int ret = -1;

        //String query = String.format("SELECT name, src_opt, src, live, transcode FROM %s WHERE publisher=%s;", Common.DB.TBL_STREAM, publisher);
        String query = String.format("SELECT S.name, S.src_opt, S.src, S.live, transcode FROM %s P, %s C, %s S WHERE P.id = C.hub AND S.cam = C.id AND P.id=%s;", Common.DB.TBL_PUBLISHER, Common.DB.TBL_CAMERA, Common.DB.TBL_STREAM, publisher);

        LOG.info("Query camera " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                Stream stream = new Stream();
                stream.setName(rs.getString("name"));
                stream.setSrcOption(rs.getString("src_opt"));
                stream.setSrc(rs.getString("src"));
                stream.setTranscode(rs.getString("transcode"));
                stream.setOut(rs.getString("live"));

                streams.add(stream);
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get cameras err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int addStream(String cam, Stream stream, StringBuilder insertId) {
        int ret = -1;

        String query = String.format("INSERT INTO %s(name, src_opt, src, transcode, live, live_hls, cam) " +
                        "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %s)", Common.DB.TBL_STREAM,
                stream.getName(), stream.getSrcOption(), stream.getSrc(), stream.getTranscode(), stream.getOut(), stream.getLiveHls(), cam);


        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS) > 0) {
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    insertId.append(rs.getString(1));
                    ret = 0;
                }
                LOG.info("Insert stream success " + insertId.toString());
            }
        } catch (Exception ex) {
            LOG.severe("Error while insert stream " + ex.toString());
            ret = -1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int updateStream(String id, Stream stream) {
        int ret = -1;
        String query = String.format("UPDATE %s SET src='%s' WHERE id='%s'",
                    Common.DB.TBL_STREAM, stream.getSrc(), id);


        LOG.info("Update stream query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Update stream success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while update stream " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }

    public int deleteStream(String id) {
        int ret = -1;

        String query = String.format("DELETE FROM %s WHERE id='%s'",
                Common.DB.TBL_STREAM, id);


        LOG.info("Delete stream query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Delete stream success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while update stream " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }
}
