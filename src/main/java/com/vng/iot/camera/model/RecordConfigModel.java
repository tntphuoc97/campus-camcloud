/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.camera.model;

import com.vng.iot.camera.data.RecordConfig;
import com.vng.iot.camera.common.Common;
import com.vng.iot.camera.database.MySqlFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author phuoc
 */
public class RecordConfigModel {
    private static final Logger LOG = Logger.getLogger(HubModel.class.getName());

    private static RecordConfigModel instance = null;
    public static RecordConfigModel getInstance(){
        if (instance == null) {
            instance = new RecordConfigModel();
        }
        return instance;
    }
    
    public int getRecordConfigByCamId(String camId,RecordConfig rec){
        int ret = -1;
        String query = String.format("SELECT * FROM %s WHERE cam_id=%s", Common.DB.TBL_RECORD_STREAM, camId);
        LOG.info("Query recordstream " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                rec.setId(rs.getInt("id"));
                rec.setCamId(rs.getInt("cam_id"));
                rec.setStreamName(rs.getString("stream_name"));
                rec.setSegmentLength(rs.getInt("segment_length"));
                rec.setRecorderId(rs.getInt("recorder"));
                rec.setDayStorage(rs.getInt("day_storage"));
                rec.setEnable(rs.getInt("enable"));
                rec.setTypeRecordInput(rs.getInt("type_record_input"));
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get record stream err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }
    public int getRecordConfigByUuid(List<RecordConfig> recs, String uuid){
        int ret =-1;
        
        
        String query = String.format("select * from %s where cam_id in (select id from %s where hub = (select id from %s where uuid = '%s'))", Common.DB.TBL_RECORD_STREAM,Common.DB.TBL_CAMERA,Common.DB.TBL_PUBLISHER, uuid);
        LOG.info("Query recordstream " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                
                RecordConfig rec = new RecordConfig();
                rec.setId(rs.getInt("id"));
                rec.setCamId(rs.getInt("cam_id"));
                rec.setStreamName(rs.getString("stream_name"));
                rec.setSegmentLength(rs.getInt("segment_length"));
                rec.setRecorderId(rs.getInt("recorder"));
                rec.setDayStorage(rs.getInt("day_storage"));
                rec.setEnable(rs.getInt("enable"));
                rec.setTypeRecordInput(rs.getInt("type_record_input"));
                
                recs.add(rec);
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get record stream err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        
        
        return ret;
    }
    public int getRecordConfigByCamId(List<RecordConfig> recs, String camId){
        int ret =-1;
        
        
        String query = String.format("select * from %s where cam_id= %d", Common.DB.TBL_RECORD_STREAM, Integer.parseInt(camId));
        LOG.info("Query recordstream " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                
                RecordConfig rec = new RecordConfig();
                rec.setId(rs.getInt("id"));
                rec.setCamId(rs.getInt("cam_id"));
                rec.setStreamName(rs.getString("stream_name"));
                rec.setSegmentLength(rs.getInt("segment_length"));
                rec.setRecorderId(rs.getInt("recorder"));
                rec.setDayStorage(rs.getInt("day_storage"));
                rec.setEnable(rs.getInt("enable"));
                rec.setTypeRecordInput(rs.getInt("type_record_input"));
                
                recs.add(rec);
            }

            ret = 0;
        } catch (Exception e){
            LOG.severe("Error when query get record stream err: "+ e);
        }
        finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        
        
        return ret;
    }
 /*   public int addRecordConfig(String asd, RecordConfig recordConfig,String insertId){
        String query = String.format("INSERT INTO %s(alias, address, hub, uid, recordstreamId ) " +
                        "VALUES ('%s', '%s', '%s', '%s', '%s' )", Common.DB.TBL_RECORD_STREAM,
                recordConfig.getRecorderId(), recordConfig.getSegmentLength(),  recordConfig.getEnable());

        LOG.info("Add cam query: " + query);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS) > 0) {
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    insertId.append(rs.getString(1));
                    ret = 0;
                }
                LOG.info("Insert camera success " + insertId.toString());
            }
        } catch (Exception ex) {
            LOG.severe("Error while camera stream " + ex.toString());
            ret = -1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        return ret;
    }*/
    
    public int addRecordConfig(StringBuilder camId,RecordConfig recordConfig,StringBuilder insertId){
        int ret = -1;
        String query = String.format("insert into %s(cam_id,stream_name,segment_length,recorder,day_storage,type_record_input)"+" values(%d,'%s',%d,%d,%d,%d)" 
                        , Common.DB.TBL_RECORD_STREAM,
                Integer.parseInt(camId.toString()),camId.toString(), recordConfig.getSegmentLength(),  recordConfig.getRecorderId(),recordConfig.getDayStorage(),recordConfig.getTypeRecordInput());
        
        LOG.info("Add record config query: " + query);
        int temp = 0;
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            temp = stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            if (temp > 0) {
                rs = stmt.getGeneratedKeys();
                
                while (rs.next()) {
                    insertId.append(rs.getString(1));
                    ret = 0;
                }
                LOG.info("Insert camera success " + insertId.toString());
            }
            recordConfig.setId(Integer.parseInt(insertId.toString()));
            recordConfig.setCamId(Integer.parseInt(camId.toString()));
        }catch(Exception e){
            LOG.severe("Error while camera stream " + e.toString());
            ret = -1;
        }finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }
        
        return ret;
    }
    
    public int deleteRecordStream(String camId){
        int ret =-1;
        String query = String.format("DELETE FROM %s WHERE cam_id='%s'",
                Common.DB.TBL_RECORD_STREAM, camId);


        LOG.info("Delete record stream query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            if (stmt.executeUpdate(query) > 0) {
                LOG.info("Delete record stream success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while update record stream " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        
        return ret;
    }
    
    public int updateRecordConfig(String camId, RecordConfig recordConfig){
        int ret = -1;
        String query = String.format("UPDATE %s SET segment_length=%d, recorder=%d, day_storage = %d, type_record_input = %d  WHERE cam_id= %d",
                    Common.DB.TBL_RECORD_STREAM, recordConfig.getSegmentLength(),recordConfig.getRecorderId(),recordConfig.getDayStorage(), recordConfig.getTypeRecordInput(),Integer.parseInt(camId));


        LOG.info("Update record stream query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            int temp = stmt.executeUpdate(query);
            if ( temp > 0) {
                LOG.info("Update record stream success");
            }
            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while update record stream " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        
        return ret;
    }
    public int getDaysStorage(RecordConfig recordConfig, String camId){
        int ret = -1;
        String query = String.format("SELECT id,day_storage from %s WHERE cam_id = %d ",
                Common.DB.TBL_RECORD_STREAM,Integer.parseInt(camId));


        LOG.info("Get day storage  query: " + query);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = MySqlFactory.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                
                recordConfig.setId(rs.getInt("id"));
                recordConfig.setDayStorage(rs.getInt("day_storage"));
            }

            ret = 0;
        } catch (Exception ex) {
            LOG.severe("Error while get day storage " + ex.toString());
            ret = 1;
        } finally {
            MySqlFactory.getInstance().safeClose(stmt);
            MySqlFactory.getInstance().safeClose(connection);
        }

        
        
        return ret;
    }
}
