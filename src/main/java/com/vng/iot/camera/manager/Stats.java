package com.vng.iot.camera.manager;

public class Stats {
    private static Stats ourInstance = new Stats();

    public static Stats getInstance() {
        return ourInstance;
    }

    private Stats() {
    }
}
