/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.mqtt.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vng.iot.camera.common.Define;
import com.vng.iot.camera.data.Camera;
import com.vng.iot.camera.data.CameraMap;
import com.vng.iot.camera.data.Hub;
import com.vng.iot.camera.data.HubMqttControlling;
import com.vng.iot.camera.model.CameraModel;
import com.vng.iot.camera.data.RecordConfig;
import com.vng.iot.camera.model.HubControllingDataModel;
import com.vng.iot.camera.model.HubModel;
import com.vng.iot.camera.model.RecordModel;
import com.vng.iot.camera.mqtt.RecordModelMqtt;
import com.vng.iot.camera.mqtt.StreamModelMqtt;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author phuoc
 */
public class DefineBrokerModel {
    private static final Logger LOG = Logger.getLogger(DefineBrokerModel.class.getName());

    private Gson gson = new GsonBuilder().serializeNulls().create();

    public static MqttCameraModel defineHubMqttController(List<Camera> cams, String uuid, int error, int message_id) {
        MqttCameraModel mqtt = new MqttCameraModel();
        mqtt.setMsg_id(message_id);
        mqtt.setMsg("camera broker");
        mqtt.setError(error);
        Hub hubPicker  = new Hub();
        
        if(HubModel.getInstance().getHubId(uuid, hubPicker) != 0){
            System.out.println("Can not get Hub Id");
        }
        
        
        HubMqttControlling hubData   = new HubMqttControlling();
        if(HubControllingDataModel.getInstance().getHubControlingDataByUuid(hubData, Define.UuidStream) != 0){
            LOG.warning("Cant get Hub Model Mqtt");
        }
        
        int sequence_id_camera = hubData.getSequence_id_stream() + 1;
        
        
        
        HubCameraMqttModel hubMqtt = new HubCameraMqttModel();
        hubMqtt.setSequence_id(sequence_id_camera);
        hubMqtt.setUuid(Define.UuidStream);
        
        //convert cameras to streamModelMqtt
        List<StreamModelMqtt> streams = new ArrayList<>();
        cams.forEach( cam -> {
            StreamModelMqtt mainStream  = new StreamModelMqtt();
            if(CameraModel.getInstance().getMainStreamByCamId(mainStream, cam) != 0){
                System.out.println("com.vng.gbc.mqtt.controller.DefineBrokerModel.defineHubMqttController()");
            }
                
            StreamModelMqtt subStream = new StreamModelMqtt();
            if(CameraModel.getInstance().getSubStreamByCamId(subStream, cam) != 0){
                System.out.println("com.vng.gbc.mqtt.controller.DefineBrokerModel.defineHubMqttController()");
            }
            
            //streams.add(mainStream);
            streams.add(subStream);
                    
        });
        
        
        
        
        
        hubMqtt.setDevice_list(streams);

        mqtt.setDt(hubMqtt);
        hubData.setSequence_id_stream(sequence_id_camera);
        
        if(HubControllingDataModel.getInstance().addSequenceId(hubData) != 0 ){
            LOG.warning("Cant add Hub Sequence Stream  to Hub Data Model");
        }
        return mqtt;
    }

    public static MqttCameraModel defineBasicHubMqttController(List<Camera> cams, String uuid, int error, int message_id) {
        MqttCameraModel mqtt = new MqttCameraModel();
        mqtt.setMsg_id(message_id);
        mqtt.setMsg("camera broker");
        mqtt.setError(error);

        
        HubMqttControlling hubData = new HubMqttControlling();
        
        if(HubControllingDataModel.getInstance().getHubControlingDataByUuid(hubData, uuid) != 0 ){
            LOG.warning("Cant get hub Mqtt Data Model");
        }
        int sequence_id_camera = hubData.getSequence_id_stream();
        
        
        
        HubCameraMqttModel hubMqtt = new HubCameraMqttModel();
        hubMqtt.setSequence_id(sequence_id_camera);
        
        
        hubMqtt.setUuid(Define.UuidStream);
        //convert camera to Stream model mqtt
        
        
        
        
        
      //convert cameras to streamModelMqtt
        List<StreamModelMqtt> streams = new ArrayList<>();
        cams.forEach( ele -> {
            StreamModelMqtt mainStream  = new StreamModelMqtt();
            if(CameraModel.getInstance().getMainStreamByCamId(mainStream, ele) != 0){
                System.out.println("com.vng.gbc.mqtt.controller.DefineBrokerModel.defineHubMqttController()");
            }
                
            StreamModelMqtt subStream = new StreamModelMqtt();
            if(CameraModel.getInstance().getSubStreamByCamId(subStream, ele) != 0){
                System.out.println("com.vng.gbc.mqtt.controller.DefineBrokerModel.defineHubMqttController()");
            }
            
            //streams.add(mainStream);
            streams.add(subStream);
                    
        });
        
        
        
        hubMqtt.setDevice_list(streams);

        mqtt.setDt(hubMqtt);

        return mqtt;
    }

    public static MqttRecordStreamModel defineRecordMqttController(List<RecordConfig> recordConfigs, String uuid, int error, int msg_id) {
        MqttRecordStreamModel mqtt = new MqttRecordStreamModel();
        mqtt.setMsg_id(msg_id);
        mqtt.setMsg("record stream broker");
        mqtt.setError(error);

        HubMqttControlling hubData = new HubMqttControlling();
        if(HubControllingDataModel.getInstance().getHubControlingDataByUuid(hubData, uuid) != 0 ){
            LOG.warning("Cant get HubMqtt Data");
        }
        
        int sequence_id_record_stream = hubData.getSequence_id_record() +1;
        
        HubRecordStreamMqttModel hubMqtt = new HubRecordStreamMqttModel();
        hubMqtt.setSequence_id(sequence_id_record_stream++);
        hubMqtt.setUuid(uuid);
        //convert recordconfig to streammodelmqt
        List<RecordModelMqtt> records = new ArrayList<>();

        recordConfigs.forEach(e -> {
            RecordModelMqtt re = new RecordModelMqtt();
            re.setId(String.valueOf(e.getCamId()));
            re.setDayStorage(e.getDayStorage());
            re.setRecordPath("/mnt/record/recorder0" + e.getRecorderId());
            re.setSegment(String.valueOf(e.getSegmentLength()));

            re.setEnable(true);
            if (e.getTypeRecordInput() == 1) {
                StringBuilder recordPath = new StringBuilder();

                if (RecordModel.getInstance().getStreamPathByCamId(recordPath, e.getCamId()) == 0) {
                    System.out.println("com.vng.gbc.mqtt.controller.DefineBrokerModel.defineRecordMqttController()");
                }
                re.setRecordInput("-rtsp_flags prefer_tcp -i " + recordPath.toString());
            } else {
                StringBuilder recordPath = new StringBuilder();
                if (RecordModel.getInstance().getStreamLiveByCamId(recordPath, e.getCamId()) == 0){
                   
                }
                      
                re.setRecordInput(recordPath.toString());
               // re.setRecordInput("-f live_flv -i rtmp://sarimi.camera.campus.zing.vn:8999/live/" + e.getCamId());
            }
            records.add(re);
        });
        
        hubData.setSequence_id_record(sequence_id_record_stream);
        if(HubControllingDataModel.getInstance().addSequenceId(hubData) != 0){
            
            LOG.warning("Cant add Hub Mqtt Controlling");
        }
        
        hubMqtt.setDevice_list(records);

        mqtt.setDt(hubMqtt);

        return mqtt;
    }

    public static MqttRecordStreamModel defineBasicRecordMqttController(List<RecordConfig> recordConfigs, String uuid, int error, int msg_id) {
        MqttRecordStreamModel mqtt = new MqttRecordStreamModel();
        mqtt.setMsg_id(msg_id);
        mqtt.setMsg("record stream broker");
        mqtt.setError(error);
        
        HubRecordStreamMqttModel hubMqtt = new HubRecordStreamMqttModel();
        
        //hub mqtt controllingMqttCameraModel
        HubMqttControlling hubData  = new HubMqttControlling();
        if(HubControllingDataModel.getInstance().getHubControlingDataByUuid(hubData, uuid) != 0){
            LOG.warning("Cant get Hub Mqtt ");
        }
        int sequence_id_record_stream = hubData.getSequence_id_record();
         
         
         
         
        hubMqtt.setSequence_id(sequence_id_record_stream);
        hubMqtt.setUuid(uuid);
        if(recordConfigs.size() == 0){
            hubMqtt.setDevice_list(new ArrayList<RecordModelMqtt>());
        }else{
        //convert recordconfig to streammodelmqtt
        List<RecordModelMqtt> records = new ArrayList<>();

        recordConfigs.forEach(e -> {
            RecordModelMqtt re = new RecordModelMqtt();
            re.setId(String.valueOf(e.getCamId()));
            re.setDayStorage(e.getDayStorage());
            re.setRecordPath("/mnt/record/recorder0" + e.getRecorderId());
            re.setSegment(String.valueOf(e.getSegmentLength()));

            re.setEnable(true);
            if (e.getTypeRecordInput() == 1) {
                StringBuilder recordPath = new StringBuilder();

                if (RecordModel.getInstance().getStreamPathByCamId(recordPath, e.getCamId()) == 0) {
                   
                }
                re.setRecordInput("-rtsp_flags prefer_tcp -i " + recordPath.toString());
            } else {
                StringBuilder recordPath = new StringBuilder();
                if (RecordModel.getInstance().getStreamLiveByCamId(recordPath, e.getCamId()) == 0){
                   
                }
                        
                        
//                re.setRecordInput("-f live_flv -i rtmp://sarimi.camera.campus.zing.vn:8999/live/" + e.getCamId());
                re.setRecordInput(recordPath.toString());
            }
            records.add(re);
        });
        
        hubMqtt.setDevice_list(records);
        }
        mqtt.setDt(hubMqtt);

        return mqtt;
    }
    
}
