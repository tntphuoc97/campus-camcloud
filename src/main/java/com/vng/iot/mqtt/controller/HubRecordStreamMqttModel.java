/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.mqtt.controller;

import com.vng.iot.camera.data.Camera;
import com.vng.iot.camera.data.RecordConfig;
import com.vng.iot.camera.mqtt.RecordModelMqtt;
import com.vng.iot.camera.mqtt.StreamModelMqtt;
import java.util.List;

/**
 *
 * @author phuoc
 */
public class HubRecordStreamMqttModel {
      private String uuid;
    private int sequence_id = 0;   
    private List<RecordModelMqtt> device_list;

    public int getSequence_id() {
        return sequence_id;
    }

    public void setSequence_id(int sequence_id) {
        this.sequence_id = sequence_id;
    }
    

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public HubRecordStreamMqttModel() {
        
    }

    public List<RecordModelMqtt> getDevice_list() {
        return device_list;
    }

    public void setDevice_list(List<RecordModelMqtt> device_list) {
        this.device_list = device_list;
    }

    

    

}
