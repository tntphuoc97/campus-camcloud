/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.mqtt.controller;

/**
 *
 * @author phuoc
 */
public class MqttRecordStreamModel {
    private int msg_id;
    private String msg;
    private int error;
    private HubRecordStreamMqttModel dt;

    public int getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(int msg_id) {
        this.msg_id = msg_id;
    }

    

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public HubRecordStreamMqttModel getDt() {
        return dt;
    }

    public void setDt(HubRecordStreamMqttModel dt) {
        this.dt = dt;
    }


}
