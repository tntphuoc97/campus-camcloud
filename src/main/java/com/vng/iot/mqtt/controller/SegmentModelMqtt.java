/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.mqtt.controller;

/**
 *
 * @author phuoc
 */
public class SegmentModelMqtt {
    private SegmentMessageMqtt dt;
    private int msg_id;
    private double timestamp;
    private int duration;

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    
    
    
    public SegmentMessageMqtt getDt() {
        return dt;
    }

    public void setDt(SegmentMessageMqtt dt) {
        this.dt = dt;
    }

    

    public int getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(int msg_id) {
        this.msg_id = msg_id;
    }

    public double getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(double timestamp) {
        this.timestamp = timestamp;
    }
    
}

