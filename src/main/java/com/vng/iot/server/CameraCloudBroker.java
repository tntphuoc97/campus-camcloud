/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.iot.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vng.iot.camera.common.Define;
import com.vng.iot.camera.data.Camera;
import com.vng.iot.camera.model.CameraModel;
import com.vng.iot.camera.model.HubModel;
import com.vng.iot.camera.data.RecordConfig;
import com.vng.iot.camera.model.RecordConfigModel;
import com.vng.iot.camera.mqtt.MqttPublisherHelper;
import com.vng.iot.camera.mqtt.MqttSubscriberHelper;
import com.vng.iot.mqtt.controller.DefineBrokerModel;
import com.vng.iot.mqtt.controller.MqttCameraModel;
import com.vng.iot.mqtt.controller.MqttRecordStreamModel;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author phuoc
 */
public class CameraCloudBroker {

    private static final Logger LOG = Logger.getLogger(CameraCloudBroker.class.getName());
    private static Gson gson = new GsonBuilder().serializeNulls().create();

    public static void runBroker() throws MqttException {
        try {
            //publish default data to broker
            publishData();
            //subscribe 
            MqttSubscriberHelper subscriber = new MqttSubscriberHelper();

            subscriber.subscribeTopic(Define.getTopicAddSegment());
            subscriber.subscribeTopic(Define.getTopicDeleteSegment());
            subscriber.subscribeTopic(Define.getTopicConfigCamera());
            subscriber.subscribeTopic(Define.getTopicConfigRecord());
        } catch (Exception e) {
            LOG.log(Level.WARNING, e.getMessage());

        }

    }

    private static int publishData() {
        int ret = -1;
        List<String> listUuid = new ArrayList<>();

        if (HubModel.getInstance().getUuids(listUuid) != 0) {
            LOG.warning("failed to load uuid when starting server");
        }
        listUuid.stream().forEach(c -> {
            //publish camera and record stream by hubs uuid
            
            
            
//            List<Camera> cameras = new ArrayList<Camera>();
//            if (CameraModel.getInstance().getCamerasByUuid(c, cameras) != 0) {
//                LOG.warning("Failed to load cameras when starting server");
//            } else if (processPushCameraMqtt(c, cameras) != 0) {
//                LOG.warning("Pub cameras of uuid:" + c + " failed!!!");
//            }
            List<RecordConfig> recordconfigs = new ArrayList<>();
            if (RecordConfigModel.getInstance().getRecordConfigByUuid(recordconfigs, c) != 0) {
                LOG.warning("Failed to load record streams when starting server");
            } else if (processPubRecordStreamMqtt(c, recordconfigs) != 0) {
                LOG.warning("Pub record stream of uuid :" + c + " failed");
            }

        });
        List<Camera> cameras = new  ArrayList<>();
        if(CameraModel.getInstance().getAllCameras(cameras) != 0){
            LOG.warning("Failed to load all cameras to mqtt");
        }
        if( processPushCameraMqtt(Define.UuidStream, cameras) != 0){
            LOG.warning("Load stream to mqtt failed");
        }
        
        
                
        
        
        return ret;
    }

    private static int processPushCameraMqtt(String uuid, List<Camera> cameras) {
        int ret = 0;

        //get list cameras by uuid
        MqttCameraModel mqttController = DefineBrokerModel.defineBasicHubMqttController(cameras, uuid, 0, 999);

        String dt = gson.toJson(mqttController, MqttCameraModel.class);
        String topic = Define.getTopicSetConfigHub(uuid);
//        MqttPublisherHelper mqtt = new MqttPublisherHelper();
        if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) != 0) {
            LOG.warning("Publish to broker failed");
            ret = -1;
        }

        return ret;
    }

    private static int processPubRecordStreamMqtt(String uuid, List<RecordConfig> recordConfigs) {
        int ret = 0;
        MqttRecordStreamModel mqttController = DefineBrokerModel.defineBasicRecordMqttController(recordConfigs, uuid, ret, ret);

        String dt = gson.toJson(mqttController, MqttRecordStreamModel.class);
        String topic = Define.getTopicRecordStream(uuid);
//        MqttPublisherHelper mqtt = new MqttPublisherHelper();
        if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) != 0) {
            LOG.warning("Publish to broker failed");
            ret = -1;
        }

        return ret;
    }

}
