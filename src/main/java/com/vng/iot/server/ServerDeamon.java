package com.vng.iot.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.org.apache.bcel.internal.generic.AALOAD;
import com.vng.iot.camera.common.CloudConfig;
import com.vng.iot.camera.common.Define;
import com.vng.iot.camera.controller.CameraController;
import com.vng.iot.camera.controller.GroupController;
import com.vng.iot.camera.controller.PublisherController;
import com.vng.iot.camera.controller.RecordController;
import com.vng.iot.camera.controller.SegmentController;
import com.vng.iot.camera.controller.SiteController;
import com.vng.iot.camera.controller.UserController;
import com.vng.iot.camera.data.Camera;
import com.vng.iot.camera.model.CameraModel;
import com.vng.iot.camera.data.Hub;
import com.vng.iot.camera.model.HubModel;
import com.vng.iot.camera.data.RecordConfig;
import com.vng.iot.camera.model.RecordConfigModel;
import com.vng.iot.camera.model.RecordModel;
import com.vng.iot.camera.mqtt.MqttPublisherHelper;
import com.vng.iot.camera.mqtt.MqttSubscriberHelper;
import com.vng.iot.camera.mqtt.StreamModelMqtt;
import com.vng.iot.mqtt.controller.DefineBrokerModel;
import com.vng.iot.mqtt.controller.MqttCameraModel;
import com.vng.iot.mqtt.controller.MqttRecordStreamModel;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.session.HashSessionIdManager;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.paho.client.mqttv3.MqttException;

public class ServerDeamon implements Runnable {

    private static final Logger LOG = Logger.getLogger(ServerDeamon.class.getName());
    private Server server = new Server();
    private static ServerDeamon _instance = null;
    private static final Lock createLock_ = new ReentrantLock();
    private static Gson gson = new GsonBuilder().serializeNulls().create();

    public static ServerDeamon getInstance() {
        if (_instance == null) {
            createLock_.lock();
            try {
                if (_instance == null) {
                    _instance = new ServerDeamon();
                }
            } finally {
                createLock_.unlock();
            }
        }
        return _instance;
    }

    public static void main(String[] args) throws MqttException {
        LOG.info("Start CameraCloud");
        LOG.info("Load service configuration");

        String configPath = "config/cloudconfig.json";
        if (CloudConfig.getInstance().setup(configPath) != 0) {
            LOG.severe("Load configuration failed");
            return;
        }

        Server server = new Server(CloudConfig.getInstance().getServerPort());

        // Specify the Session ID Manager
        HashSessionIdManager sessionIdMng = new HashSessionIdManager();
        server.setSessionIdManager(sessionIdMng);
        FilterHolder filterHolder = new FilterHolder(CrossOriginFilter.class);
        filterHolder.setInitParameter("allowedOrigins", "*");
        filterHolder.setInitParameter("allowedMethods", "GET, POST");
        LOG.info("Start server...");
        ServletContextHandler handler = new ServletContextHandler(server, "/");
        handler.addServlet(UserController.class, "/api/user/*");
        handler.addServlet(PublisherController.class, "/api/publisher/*");
        handler.addServlet(CameraController.class, "/api/cam/*");
        handler.addServlet(RecordController.class, "/api/rec/*");
        handler.addServlet(SegmentController.class, "/api/seg/*");
        handler.addServlet(SiteController.class, "/api/site/*");
        handler.addServlet(GroupController.class, "/api/group/*");
        
        
        handler.addFilter(filterHolder, "/*", null);

        // Create the SessionHandler (wrapper) to handle the sessions
        HashSessionManager manager = new HashSessionManager();
        SessionHandler sessions = new SessionHandler(manager);
        handler.insertHandler(sessions);

        server.setHandler(handler);
        CameraCloudBroker.runBroker();
        // mqtt
//        MqttSubscriberHelper mqttSubscriber = new MqttSubscriberHelper();
//        StringBuilder messageConfigCamera = new StringBuilder();
//
//        //subsribe topic 1
//        mqttSubscriber.subscribeTopic(Define.getTopicAddSegment());
        
        try {
            server.start();
            server.join();

        } catch (Exception ex) {
            LOG.severe("Server error: " + ex.toString());
        }

    }
    
    

    private static void runBroker() throws MqttException {
        //publish default data to broker
        publishData();
        //subscribe 
        MqttSubscriberHelper subscriber = new MqttSubscriberHelper();
        
        subscriber.subscribeTopicAddSegment(Define.getTopicAddSegment());
        subscriber.subscribeTopicDelSegment(Define.getTopicDeleteSegment());
        
        
        Thread thread = new Thread();
    }

    private static int publishData() {
        int ret = -1;
        List<String> listUuid = new ArrayList<>();

        if (HubModel.getInstance().getUuids(listUuid) != 0) {
            LOG.warning("failed to load uuid when starting server");
        }
        listUuid.stream().forEach(c -> {
            //publish camera and record stream by hubs uuid
            List<Camera> cameras = new ArrayList<Camera>();
            if (CameraModel.getInstance().getCamerasByUuid(c, cameras) != 0) {
                LOG.warning("Failed to load cameras when starting server");
            } else {
                if(processPushCameraMqtt(c, cameras) != 0 ){
                    LOG.warning("Pub cameras of uuid:" + c + " failed!!!");
                }
        
            }
            List<RecordConfig> recordconfigs  = new ArrayList<>();
            if(RecordConfigModel.getInstance().getRecordConfigByUuid(recordconfigs,c) != 0 ){
                LOG.warning("Failed to load record streams when starting server");
            }else{
                if (processPubRecordStreamMqtt(c, recordconfigs)!= 0) {
                    LOG.warning("Pub record stream of uuid :" + c +" failed");
                }
            }
            
            
            
        });
        return ret;
    }

    private static int processPushCameraMqtt(String uuid,List<Camera> cameras) {
        int ret = 0;

        

        //get list cameras by uuid
        
        //convert list cameras to streams
      
        
        MqttCameraModel mqttController = DefineBrokerModel.defineHubMqttController(cameras, uuid, 0,999);

        String dt = gson.toJson(mqttController, MqttCameraModel.class).replaceAll("\\\\", "");
        String topic = Define.getTopicSetConfigHub(uuid);
//        MqttPublisherHelper mqtt = new MqttPublisherHelper();
        if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) != 0) {
            LOG.warning("Publish to broker failed");
            ret = -1;
        }
        

        return ret;
    }
    private static int processPubRecordStreamMqtt(String uuid,List<RecordConfig> recordConfigs){
        int ret = 0;
        MqttRecordStreamModel mqttController = DefineBrokerModel.defineRecordMqttController(recordConfigs, uuid, 0,998);

        String dt = gson.toJson(mqttController, MqttRecordStreamModel.class).replaceAll("\\\\", "");
        String topic = Define.getTopicRecordStream(uuid);
//        MqttPublisherHelper mqtt = new MqttPublisherHelper();
        if (MqttPublisherHelper.getInstance().pushDataToBroker(dt, topic, 1) != 0) {
            LOG.warning("Publish to broker failed");
            ret = -1;
        }
        
        
        return ret;
    }
    
    @Override
    public void run() {
        List<Hub> hubs = new ArrayList<Hub>();
        if(HubModel.getInstance().getHubsWithoutLogin(hubs) != 0){
            LOG.info("get hubs without saving failed");
            return;
        }
        List<String> topics = new ArrayList<>();
        hubs.forEach( x -> {
            topics.add(Define.getTopicGetConfigHub(x.getUuid()));
        });
        
        
    }
}
