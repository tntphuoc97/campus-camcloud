var cloudApp = angular
        .module('sbAdminApp', [
            'oc.lazyLoad',
            'ui.router',
            // 'ngAnimate',
            'ngSanitize',
            'toastr',
            'ui.bootstrap',
            'mwl.confirm',
            'angular-loading-bar',
            'ngStorage',
            'datatables',
            'datatables.bootstrap',
            'ngMap',
            'vjs.video',
            'angular-uri',
            'ui.bootstrap.datetimepicker',
                    // 'angular-jquery-locationpicker',
        ]);

(function () {
    'use strict';
    /**
     * @ngdoc overview
     * @name sbAdminApp
     * @description
     * # sbAdminApp
     *
     * Main module of the application.
     */
    cloudApp

            .run(function ($rootScope, $state, $location, $sessionStorage, $http) {
                // keep user logged in after page refresh
                $rootScope.username = '';
                $rootScope.userLogin= {};
                $http.defaults.headers.common['Access-Control-Allow-Credentials'] = "true";
                if ($sessionStorage.currentUser) {
                    $http.defaults.headers.common.Authorization = 'Bearer ' + $sessionStorage.currentUser.token;
                }
                // redirect to login page if not logged in and trying to access a restricted page
                $rootScope.$on('$locationChangeStart', function (event, next, current) {
                    var publicPages = ['/login', '/register'];
                    var restrictedPage = publicPages.indexOf($location.path()) === -1;
                    if (restrictedPage) {
                        console.log("Check authenrize when change url");

                        $http.post(apiDomain + "/api/user/authorize")
                                .then(function (response) {
                                    // Auto go login if authen failure (include err:-999 in msg)
                                    console.log("." + response.data.msg);

                                },
                                        function (response) {
                                            console.log(".." + response.data.msg);
                                            $state.go("login");
                                        });
                    }
                });

                $state.defaultErrorHandler(function () { /* do nothing */
                });
            })
            .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $httpProvider) {

                    // alternatively, register the interceptor via an anonymous factory
                    $httpProvider.interceptors.push(function ($q, $state, $location) {
                        return {
                            'request': function (request) {
                                // same as above
                                return request;
                            },
                            'response': function (response) {
                                // same as above           
                                if (response.data.err === -999) {
                                    $location.path('/login');
                                    $state.go("login", {}, {reload: true});

                                } else {
                                    //console.log("err" + response.data);
                                }
                                return response;
                            },
                            'responseError': function (rejection) {
                                return $q.reject(rejection);
                            }

                        };
                    });

                    $ocLazyLoadProvider.config({
                        debug: false,
                        events: true,
                    });

                    $urlRouterProvider.otherwise('/login');

                    $stateProvider
                            .state('login', {
                                templateUrl: 'views/pages/login.html',
                                url: '/login',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/services/authentication.service.js'
                                            ]
                                        })
                                    }
                                },
                                controller: function ($rootScope, $state, $location, AuthenticationService, $http) {
                                    var vm = this;
                                    $('#loginForm').validator().on('submit', function (e) {
                                        if (e.isDefaultPrevented()) {
                                            // handle the invalid form...
                                        } else {
                                            // everything looks good!
                                            vm.login();
                                        }
                                    })

                                    vm.checkIfEnterKeyWasPressed = checkIfEnterKeyWasPressed;
                                    function checkIfEnterKeyWasPressed($event) {
                                        var keyCode = $event.which || $event.keyCode;
                                        if (keyCode === 13) {
                                            // Do that thing you finally wanted to do
                                            //vm.login();
                                        }
                                    }
                                    ;

                                    vm.login = login;
                                    function login() {
                                        AuthenticationService.Login(vm.username, vm.password, function (result) {
                                            if (result === true) {
                                                console.log("$rootScope.userLogin",$rootScope.userLogin);
                                                
                                                if($rootScope.userLogin.role === 1){
                                                    $state.go("dashboard.device");
                                                }
                                                else{
                                                    $state.go("dashboard.streamrtmp");
                                                }
                                            } else {
//                      vm.error = 'Username or password is incorrect';
//                      vm.loading = false;
                                                alert("Username or password is incorrect")
                                            }
                                        });

                                        if (vm.username === 'gbc')
                                        {
                                            $rootScope.username = 'gbc';

                                        }

                                    }
                                    ;
                                },
                                controllerAs: 'vm'
                            })
                            .state('register', {
                                templateUrl: 'views/pages/register.html',
                                url: '/register',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/services/authentication.service.js'
                                            ]
                                        })
                                    }
                                },
                                controller: function ($state, $location, $http, AuthenticationService) {
                                    var vm = this;
                                    $('#registerForm').validator().on('submit', function (e) {
                                        if (e.isDefaultPrevented()) {
                                            // handle the invalid form...
                                        } else {
                                            // everything looks good!
                                            console.log(vm.username, ",", vm.password)
                                            vm.register();
                                        }
                                    })

                                    vm.checkIfEnterKeyWasPressed = checkIfEnterKeyWasPressed;
                                    function checkIfEnterKeyWasPressed($event) {
                                        var keyCode = $event.which || $event.keyCode;
                                        if (keyCode === 13) {
                                            // Do that thing you finally wanted to do
                                            //vm.register();
                                        }
                                    }
                                    ;

                                    vm.register = register;
                                    function register() {
                                        AuthenticationService.Register(vm.username, vm.password, function (res) {
                                            if (res.success === true) {
                                                alert("Register success")
                                                $state.go("login");
                                            } else {
                                                alert("Register failed " + res.err)
                                            }
                                        });
                                    }
                                    ;
                                },
                                controllerAs: 'vm'
                            })
                            .state('dashboard', {
                                url: '/dashboard',
                                abstract: true,
                                templateUrl: 'views/dashboard/main.html',
                                controller: function ($scope, $state, $sessionStorage/*, AuthenticationService*/, $http) {
                                    var vm = this;
                                    if ($sessionStorage.currentUser != undefined) {
                                        vm.username = $sessionStorage.currentUser.username;
                                    } else {
                                        if (vm.logout)
                                            vm.logout();
                                    }

                                    vm.logout = logout;
                                    vm.showKey = showKey;

                                    function logout() {
                                        $http.post(apiDomain + "/api/user/logout")
                                                .then(function (response) {
                                                    console.log("Logout successful");
                                                    if ($sessionStorage.currentUser != null &&
                                                            $sessionStorage.currentUser != undefined)
                                                        delete $sessionStorage.currentUser;
                                                    $http.defaults.headers.common.Authorization = '';
                                                    $state.go("login");
                                                },
                                                        function (response) {
                                                            console.log("Logout failure");
                                                            alert("Logout failure ");
                                                        });
                                    }
                                    ;

                                    function showKey() {
                                        var userKeyDialog = new BootstrapDialog({
                                            title: 'Message',
                                            message: 'Your key: ' + $sessionStorage.currentUser.token
                                        });
                                        userKeyDialog.open();
                                    }
                                },
                                controllerAs: 'vm',
                                resolve: {
                                    loadMyDirectives: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load(
                                                {
                                                    name: 'sbAdminApp',
                                                    files: [
                                                        'scripts/directives/header/header.js',
                                                        'scripts/directives/header/header-notification/header-notification.js',
                                                        'scripts/directives/sidebar/sidebar.js',
                                                        'scripts/directives/sidebar/sidebar-search/sidebar-search.js'
                                                                * 'scripts/directives/sidebar/sidebar-search/sidebar.js'/*
                                                                 'scripts/services/authentication.service.js'*/
                                                    ]
                                                }),
                                                $ocLazyLoad.load(
                                                        {
                                                            name: 'toggle-switch',
                                                            files: ["bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                                                                "bower_components/angular-toggle-switch/angular-toggle-switch.css"
                                                            ]
                                                        }),
                                                // $ocLazyLoad.load(
                                                // {
                                                //   name:'ngAnimate',
                                                //   files:['bower_components/angular-animate/angular-animate.js']
                                                // })
                                                $ocLazyLoad.load(
                                                        {
                                                            name: 'ngCookies',
                                                            files: ['bower_components/angular-cookies/angular-cookies.js']
                                                        })
                                        $ocLazyLoad.load(
                                                {
                                                    name: 'ngResource',
                                                    files: ['bower_components/angular-resource/angular-resource.js']
                                                })
                                        $ocLazyLoad.load(
                                                {
                                                    name: 'ngSanitize',
                                                    files: ['bower_components/angular-sanitize/angular-sanitize.js']
                                                })
                                        $ocLazyLoad.load(
                                                {
                                                    name: 'ngTouch',
                                                    files: ['bower_components/angular-touch/angular-touch.js']
                                                })
                                    }
                                }
                            })
                            .state('dashboard.map', {
                                url: '/map',
                                controller: 'MapCtrl',
                                controllerAs: 'vm',
                                templateUrl: 'views/dashboard/map.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/map.js'
                                            ]
                                        })
                                    }
                                }
                            })
                            .state('dashboard.cameradevice', {
                                url: '/cam',
                                controller: 'CamCtrl',
                                controllerAs: 'vm',
                                templateUrl: 'views/dashboard/camera.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/cam.js'
                                            ]
                                        })
                                    }
                                }
                            })
                            .state('dashboard.hubdevice', {
                                url: '/hub',
                                controller: 'HubCtrl',
                                controllerAs: 'vm',
                                templateUrl: 'views/dashboard/hub.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/hub.js'
                                            ]
                                        })
                                    }
                                }
                            })
                            .state('dashboard.streamhls', {
                                url: '/streamhls',
                                controller: 'StreamHLSCtrl',
                                controllerAs: 'vm',
                                templateUrl: 'views/dashboard/streamhls.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/streamhls.js'
                                            ]
                                        })
                                    }
                                }
                            })
                            .state('dashboard.streamrtmp', {
                                url: '/streamrtmp',
                                controller: 'StreamRTMPCtrl',
                                controllerAs: 'vm',
                                templateUrl: 'views/dashboard/streamrtmp.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/streamrtmp.js'
                                            ]
                                        })
                                    }
                                }
                            })
                            .state('dashboard.site', {
                                url: '/site',
                                controller: 'SiteCtrl',
                                controllerAs: 'vm',
                                templateUrl: 'views/dashboard/site.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/site.js'
                                            ]
                                        })
                                    }
                                }
                            })
                            .state('dashboard.group', {
                                url: '/group',
                                controller: 'GroupCtrl',
                                controllerAs: 'vm',
                                templateUrl: 'views/dashboard/group.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/group.js'
                                            ]
                                        })
                                    }
                                }
                            })
                            .state('dashboard.cameragroup', {
                                url: '/cameragroup',
                                controller: 'CameraGroupCtrl',
                                controllerAs: 'vm',
                                templateUrl: 'views/dashboard/cameragroup.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/cameragroup.js'
                                            ]
                                        })
                                    }
                                }
                            })
                            .state('dashboard.playback', {
                                url: '/playback',
                                controller: 'PlaybackCtrl',
                                controllerAs: 'vm',
                                templateUrl: 'views/dashboard/playback.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/playback.js'
                                            ]
                                        })
                                    }
                                }
                            })
                            .state('dashboard.live', {
                                url: '/live',
                                controller: 'LiveCtrl',
                                controllerAs: 'vm',
                                templateUrl: 'views/dashboard/live.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/live.js'
                                            ]
                                        })
                                    }
                                }
                            })
                            .state('dashboard.camera', {
                                url: '/camera',
                                controller: 'MainCtrl',
                                templateUrl: 'views/dashboard/home.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/main.js',
                                                'scripts/directives/timeline/timeline.js',
                                                'scripts/directives/notifications/notifications.js',
                                                'scripts/directives/chat/chat.js',
                                                'scripts/directives/dashboard/stats/stats.js'
                                            ]
                                        })
                                    }
                                }
                            })
                            .state('dashboard.home', {
                                url: '/home',
                                controller: 'MainCtrl',
                                templateUrl: 'views/dashboard/home.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/main.js',
                                                'scripts/directives/timeline/timeline.js',
                                                'scripts/directives/notifications/notifications.js',
                                                'scripts/directives/chat/chat.js',
                                                'scripts/directives/dashboard/stats/stats.js'
                                            ]
                                        })
                                    }
                                }
                            })
                            // .state('dashboard.map',{
                            //   url:'/map',
                            //   controller: 'MapCtrl',
                            //   templateUrl:'views/dashboard/map.html',
                            //   resolve: {
                            //     loadMyFiles:function($ocLazyLoad) {
                            //       return $ocLazyLoad.load({
                            //         name:'sbAdminApp',
                            //         files:[              
                            //         'scripts/controllers/map.js'
                            //         ]
                            //       })
                            //     }
                            //   }
                            // })
                            .state('dashboard.device', {
                                url: '/device',
                                controller: 'DeviceCtrl',
                                templateUrl: 'views/dashboard/device.html',
                                resolve: {
                                    loadMyFiles: function ($ocLazyLoad) {
                                        return $ocLazyLoad.load({
                                            name: 'sbAdminApp',
                                            files: [
                                                'scripts/controllers/device.js'
                                            ]
                                        })
                                    }
                                }
                            })


                }]);

})();