(function () {
    'use strict';

    cloudApp.factory('AuthenticationService', Service);
    function Service($http, $sessionStorage, $rootScope) {
        var service = {};

        service.Login = Login;
        //service.Logout = Logout;
        service.Register = Register;

        return service;

        function Login(username, password, callback) {
            var secret = 'secret';
            var encrypted = CryptoJS.HmacSHA1(password, secret);
            $http.post(apiDomain + '/api/user/login', {username: username, password: encrypted.toString(CryptoJS.enc.Base64)})
                    .then(function (response) {
                        // login successful if there's a token in the response

                        console.log(response);
                        var ms = response.data.msg;
                        var obj = JSON.parse(ms);
                        $rootScope.userLogin = obj;
                        if (response.data.err === 0) {
                            callback(true);
                            $sessionStorage.currentUser = {username: username, token: "61asdsad64a13d"};
                            var user = response.data.dt;
                            console.log("User login",user);
                        } else {
                            callback(false);
                        }
//                    if (response.data.err == 0) {
//                        if (response.data.dt.token) {
//                            // store username and token in local storage to keep user logged in between page refreshes
//                            //$sessionStorage.currentUser = { username: username, token: response.data.dt.token };
//                            $sessionStorage.currentUser = true;
//                            // add jwt token to auth header for all requests made by the $http service
//                            //$http.defaults.headers.common.Authorization = 'Bearer ' + response.token;
//
//                            // execute callback with true to indicate successful login
//                            callback(true);
//                        } else {
//                            // execute callback with false to indicate failed login
//                            callback(false);
//                        }
//                    } else {
//                        // execute callback with false to indicate failed login
//                        callback(false);
//                    }
                    }, function (response) {
                        callback(false);
                    });
        }


        function Register(username, password, callback) {
            var secret = 'secret';
            var encrypted = CryptoJS.HmacSHA1(password, secret);
            $http.post(apiDomain + '/api/user/register', {username: username, password: encrypted.toString(CryptoJS.enc.Base64)})
                    .then(function (response) {
                        var res = {};
                        // login successful if there's a token in the response
                        console.log(response);
                        if (response.data.err == 0) {
                            res.success = true;
                            callback(res);
                        } else {
                            // execute callback with false to indicate failed login
                            res.success = false;
                            res.err = response.data.err;
                            callback(res);
                        }
                    }, function (response) {
                        var res = {};
                        res.success = false;
                        res.err = 1;
                        callback(res);
                    });
        }
        function CheckAuthen(callback) {
            $http.post(apiDomain + '/api/user/authenrize')
                    .then(function (response) {
                        console.log(response);
                        callback(true);
                    },
                            function (response) {
                                console.log("authenrize failure");
                                callback(false);
                            });
        }
    }
})();
