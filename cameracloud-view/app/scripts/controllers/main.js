(function () {
    'use strict';
    /**
     * @ngdoc function
     * @name sbAdminApp.controller:MainCtrl
     * @description
     * # MainCtrl
     * Controller of the sbAdminApp
     */
    cloudApp.controller('MainCtrl', function ($rootScope, $scope, $http) {
        $scope.summary = {};
        $scope.checkUser = function () {
            
            if ($rootScope.username !== 'gbc') {
                var sidebar = document.getElementById("menusidebar").style.display = "none";
                var container = document.getElementById("container").style.paddingLeft = "0px";
            }
        }
        $scope.checkUser();
        $scope.getSummary = function () {
            console.log("getSummary");
            $http.post("/api/dashboard/summary").then(function (response) {
                $scope.summary = response.data.dt;
            }, function (response) {

            });
        }

        $scope.getSummary();

        $scope.checkDisplaySidebar = function () {

        }

    });

})();
