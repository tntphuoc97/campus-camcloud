/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function () {
    'use strict';
    /**
     * @ngdoc function
     * @name sbAdminApp.controller:DeviceCtrl
     * @description
     * # DeviceCtrl
     * Controller of the sbAdminApp
     */
    cloudApp.controller('StreamRTMPCtrl', function ($rootScope,$scope, NgMap, $http, $timeout, toastr, $log) {
        var vm = this;
        $scope.display1 = 'block';
        $scope.display4 = 'block';
        $scope.display9 = 'block';
        $scope.display32 = 'block';
        vm.containerView = "container";
        vm.cams = [];
        $scope.camsTemp = [];
        vm.getCams = function () {
            $http.post(apiDomain + "/api/cam/mapget").then(function (response) {

                console.log("data: ", response.data);
                if (response.data.err == 0) {
                    console.log("hello");
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== vm.cams) {
                            response.data.dt.forEach(function (cam) {
                                cam.mediaToggle = {
                                    sources: [
                                        {
                                            src: "",
                                            type: 'application/x-mpegURL'
                                        }
                                    ]
                                };
                            });
                            vm.cams = response.data.dt;
                            $scope.camsTemp = response.data.dt;

                            vm.cams.sort(function (a, b) {
                                return a.alias > b.alias;
                            });
                        }
                    }
                }
            }, function (response) {

            });
        }
        $scope.checkUser = function() {
           
            if($rootScope.userLogin.role !== 1){
                var sidebar = document.getElementById("menusidebar").style.display = "none";
                var container = document.getElementById("container").style.paddingLeft = "0px";
            }
        }
        $scope.checkUser();
        vm.getCams();
        $scope.sites = [];
        $scope.getSites = function () {
            var dt = {routerId: $scope.routerId};
            $http.post(apiDomain + "/api/cam/getSites", dt).then(function (response) {

                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.sites) {
                            $scope.sites = response.data.dt;

                            $scope.sites.push({id: 0, site_name: "Tất Cả", description: null})
                        }
                    }
                }
            }, function (response) {

            });
        }


        $scope.getSites();
        $scope.onChangeSite = function () {

            if (vm.site_selected !== null && vm.site_selected.id.toString() !== "0") {

                var camsFinding = [];
                $scope.camsTemp.forEach(function (cam) {


                    if (vm.site_selected.id.toString() === cam.siteId.toString()) {
                        camsFinding.push(cam);
                    }

                });
                vm.cams = camsFinding;
                if (vm.cams.length === 0) {
                    toastr.error('Không có camera trong khu vực đã chọn');
                }

            } else {
                vm.cams = $scope.camsTemp;
            }
//                
            $scope.checkDisplay(vm.cams);
        }
        var isMobile = {
            Android: function () {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };

        $scope.$on('vjsVideoError', function (e, data) {
            console.log('video error:' + data.id);
        });


        $scope.$on('vjsVideoReady', function (e, data) {
            console.log('video id:' + data.id);
            console.log('video:', data);

            var index = data.id.replace("player-hls-grid-", "");
            var cam = vm.cams[index];

            console.log("video for cam: ", cam);

            var src = cam.streamSub;

            // Not switch on IOS
            if (!isMobile.iOS()) {
                data.player.on('fullscreenchange', function () {
                    console.log("full screen mode");
                    src = cam.streamMain;
                    if (!data.player.isFullscreen()) {
                        console.log("use sub stream");
                        src = cam.streamSub;
                    } else {
                        console.log("use main stream")
                    }
                    cam.mediaToggle = {
                        sources: [
                            {
                                src: src,
                                type: 'application/x-mpegURL'
                            }
                        ]
                    };
                });
            } /*else {
             // Force use main stream for IOS
             src = cam.streamMain;
             }
             
             cam.mediaToggle = {
             sources: [
             {
             src: src,
             type: 'application/x-mpegURL'
             }
             ]
             };
             
             var player = data.player;
             
             player.on("error", function(e) {
             console.log("stream error retry it next 5 seconds");
             player.error(null);
             if (!isMobile.iOS()) {
             $timeout(function() {
             var hlstech = player.tech({ IWillNotUseThisInPlugins: true });
             hlstech.reset();
             //player.src({type:"application/x-mpegURL", "src": src});
             //player.play();
             
             cam.mediaToggle = {
             sources: [
             {
             src: src,
             type: 'application/x-mpegURL'
             }
             ]
             };
             
             },5000);
             }
             });*/
        });

        //function stream hls2

        vm.loadStreamHLS2 = function (cam) {
            console.log("load grid hls cam: ", cam);
            var src = cam.streamSub;
            // Not switch on IOS
            if (!isMobile.iOS()) {
                // data.player.on('fullscreenchange',function() {
                // 	console.log("full screen mode");
                // 	src = cam.streamMain;
                // 	if (!data.player.isFullscreen()) {
                // 	console.log("use sub stream");
                // 	  src = cam.streamSub;
                // 	} else {
                // 	console.log("use main stream")
                // 	}
                // 	cam.mediaToggle = {
                //            sources: [
                //                {
                //                    src: src,
                //                    type: 'application/x-mpegURL'
                //                }
                //            ]
                //        };
                // });
            } else {
                // Force use main stream for IOS
                src = cam.streamMain;
            }

            cam.mediaToggle = {
                sources: [
                    {
                        src: src,
                        type: 'application/x-mpegURL'
                    }
                ]
            };
        };

        //var last_player = null;




        vm.loadStreamFlash2 = function (cam, index) {
            var videoId = "player-flash-grid-" + index;
            // if (isMobile.any()) {
            // 	console.log("mobile detect");
            // 	$('#'+videoId).html("RTMP protocol is not supported on your device.");
            // 	return;
            // }
            $timeout(function () {
                // var src = "rtmp://192.168.101.31:8997/live/62";
                // var rtmpDomain = "rtmp://" + window.location.hostname;	
                // var src = cam.rtmpDomain + "/" +  cam.id;	
                var rtmpDomain = "rtmp://" + window.location.hostname + ":8081";
                console.log(rtmpDomain);
                //var src = "rtmp://sggp.camera.campus.zing.vn:8997/live/"+cam.id;
                //var src = rtmpDomain + "/live/" + cam.id + "_sub";

                var src = 'rtmp://172.26.12.10:1935/live/' + cam.id +'_sub';

                console.log(src + " id " + videoId);


                var flashvars = {
                    autoPlay: 'true',
                    src: src,
                    streamType: 'live',
                    scaleMode: 'letterbox',
                    // javascriptCallbackFunction: vm.ontet
                };
                var params = {
                    allowFullScreen: 'true',
                    allowScriptAccess: 'always',
                    wmode: 'opaque'
                };
                var attributes = {
                    id: videoId
                };

                var height = 500 / vm.gridFlashView;
                console.log(height);
                
                swfobject.embedSWF('grindplayer/GrindPlayer.swf', videoId, '100%',
                        "350px", '10.2', null, flashvars, params, attributes);
                // $scope.$apply(); 
            });

        }

        vm.changeSrcFlash = function (event) {
            // console.log(event);
            var rtmpDomain = "rtmp://" + window.location.hostname + ":8081";
            var cam = vm.viewCams[event.target.id];
            var src = "rtmp://sggp.camera.campus.zing.vn:8997/live/57";
            var playerId = "player-flash-grid-" + event.target.id;
            var player = document.getElementById(playerId);
            if (event.target.checked) {
                //var src = rtmpDomain + "/live/" + cam.id;
                var src = "rtmp://sggp.camera.campus.zing.vn:8997/live/57";
                player.setMediaResourceURL(src);
            } else {
                //var src = rtmpDomain + "/live/" + cam.id + "_sub";
                var src = "rtmp://sggp.camera.campus.zing.vn:8997/live/57";
                player.setMediaResourceURL(src);
            }
            // console.log(player.getDynamicStreamSwitching(0));
            // console.log(player.getDynamicStreamItemAt(1));

        }
        // Init params
        vm.currentPage = 1;
        vm.maxSize = 5;
        vm.viewCams = [];
        vm.cleanViewCam = function () {
            for (var i = 0; i < vm.viewCams.length; i++) {
                var video_id = "player-flash-grid-" + i;
                var player = videojs(video_id);
                player.dispose();
            }
            vm.viewCams = [];
        }

        vm.cleanFlashViewCam = function () {
            for (var i = 0; i < vm.viewCams.length; i++) {
                var video_id = "player-flash-grid-" + i;
                var player = videojs(video_id);
                player.dispose();
            }
            vm.viewCams = [];
        }

        vm.updateViewCam = function () {
            
            var gView;
            if (vm.gridView != 0) {
                vm.cleanViewCam();
                gView = vm.gridView;
                
            } else {
                // vm.cleanFlashViewCam();
                gView = vm.gridFlashView;
                
            }
            
            if (gView !== 36) {
                
                vm.videoColumnSize = 12 / Math.round(Math.sqrt(gView));
            }
            else{
                vm.videoColumnSize = 2;
                if($rootScope.username !== 'gbc'){
                    var styleCamera  = document.getElementsByClassName("container")[0].style.width = "1234px"; 
                }
            }
            setInterval(function () {
                // console.log("update view cams ");
                // console.log(vm.viewCams);            	
                var startSlice = (vm.currentPage - 1) * vm.itemPerPage;
                vm.viewCams = vm.cams.slice(startSlice, startSlice + vm.itemPerPage);

                $scope.$apply();
            });
        }

        // vm.updateViewFlashCam = function() {
        // 	// vm.cleanViewCam();
        // }

        vm.updateViewPage = function () {
            vm.currentPage = 1;
            vm.itemPerPage = vm.gridView;
            //Disable select Flash
            if (vm.gridFlashView != 0)
                // vm.cleanFlashViewCam();
                vm.gridFlashView = 0;
        }

        vm.updateViewFlashPage = function () {
            vm.currentPage = 1;
            vm.itemPerPage = vm.gridFlashView;
            //Disable select hls
            if (vm.gridView != 0)
                vm.cleanViewCam();
            vm.gridView = 0;

        }

        vm.gridLayoutChanged = function () {
            vm.updateViewPage();
            // vm.cleanViewCam();
            vm.updateViewCam(vm.gridView);
        };


        vm.gridLayoutFlashChanged = function () {
            vm.updateViewFlashPage();
            // vm.cleanFlashViewCam();
            vm.updateViewCam(vm.gridFlashView);
            console.log("flash grid");
        }

        vm.setPage = function (pageNo) {
            //$scope.currentPage = pageNo;
        };

        vm.pageChanged = function () {
            console.log('Page changed to: ' + vm.currentPage);
            vm.updateViewCam();
        };

        $scope.$on('$destroy', function () {
            console.log("Clean cam view");
            if (vm.gridView != 0)
            {
                vm.cleanViewCam();
            } else {
                // vm.cleanFlashViewCam();
            }
        });

    });

})();
