(function () {
'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:DeviceCtrl
 * @description
 * # DeviceCtrl
 * Controller of the sbAdminApp
 */
    cloudApp.controller('PlaybackCtrl', function($scope, $http, DTOptionsBuilder, $uibModal, toastr, $interval, $location, $timeout) {
        $scope.getPath = function () {
            var reader = new FileReader();
            reader.fileName = file.name // file came from a input file element. file = el.files[0];
            reader.onload = function (readerEvt) {
                console.log(readerEvt.target.fileName);
            };
        }
        $scope.cams = []
        $scope.camsTemp = [];
        $scope.getCams = function() {
            $http.post(apiDomain + "/api/cam/gets").then(function(response){
                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.cams) {
                            $scope.cams = response.data.dt;
                            $scope.camsTemp = response.data.dt;
                        }
                    }
                }
            }, function(response) {

            });
        }
        

        $scope.getCams();
        $scope.sites = [];
        $scope.getSites = function() {
            var dt = {routerId: $scope.routerId};
            $http.post(apiDomain + "/api/cam/getSites", dt).then(function (response) {
                
                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.sites) {
                            $scope.sites = response.data.dt;
                            console.log("$scope.sites",$scope.sites);
                            $scope.sites.push({id:0,site_name:"Tất Cả",description:null})
                        }
                    }
                }
            }, function (response) {
                
            });
        }
        $scope.getSites();
        
        $scope.onChangeSite = function (){
           
            console.log($scope.cams);
            if ($scope.site_selected !== null && $scope.site_selected.id.toString() !== "0") {
                console.log($scope.site_selected.id.toString());
                var camsFinding = [];
                $scope.camsTemp.forEach(function(cam){
                    
                   
                    if($scope.site_selected.id.toString() === cam.siteId.toString() ){
                        camsFinding.push(cam);
                    }
                    
                });
                 $scope.cams = camsFinding; 
            }else{
                $scope.cams = $scope.camsTemp;
            }
//                
            
        }
        
        $scope.segments = [];

        $scope.getSegment = function() {
            if ($scope.selectedCam === undefined) {
                toastr.error('Vui lòng chọn camera');
                return;
            }

            if ($scope.selectedDate === undefined) {
                toastr.error('Vui lòng chọn thời điểm');
                return;
            }


            var from = Date.parse($scope.selectedDate);
            var to = from + (60*60*1000);

            var dt = {cam: $scope.selectedCam.id, from: from/1000, to: to/1000};

            $http.post(apiDomain + "/api/rec/get_segment", dt).then(function(response){

                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.segments) {
                            $scope.segments = response.data.dt;
                            $scope.totalItems = $scope.segments.length;

                            if (response.data.dt.length <= 0) {
                                toastr.warning('Không có dữ liệu ghi');
                                $scope.clearPlayback();
                            }
                        }
                    }
                }
            }, function(response) {
                $scope.clearPlayback();
                toastr.error('Không thể kết nối đến máy chủ');
            });
        }


        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 5;
        $scope.maxSize = 5; //Number of pager buttons to show

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function() {
            console.log('Page changed to: ' + $scope.currentPage);
        };

        $scope.setItemsPerPage = function(num) {
          $scope.itemsPerPage = num;
          $scope.currentPage = 1; //reset to first paghe
        }

        $scope.clearPlayback = function() {
            $scope.playFile("")
        }

        var proto = window.location.protocol + "//";

        var video_player = null;

        $scope.$on('$destroy', function() {
            console.log("Clear playback");
            $timeout.cancel($scope.retryTimeout);

            if (video_player != null) {
                video_player.dispose();
                video_player = null;
            }
        });

         $scope.playFile = function(segment) {
            $scope.showVideo = true;
            var reg = /[^/\\]+/;
            var videoId = "video";
            var play_url = '';
            var play_type = '';
            play_type = 'video/mp4';
            //play_url = proto + $location.host() + ':'+$location.port()+"/" +"/zserver/".match(reg)+"/test/testplayback.mp4";
            //play_url = proto + $location.host() + ':'+$location.port() + '/zserver/test/a.mp4'
            play_url = proto + $location.host() + '/' + segment.filename;
            console.log("Play URL: ",play_url);
         
            var player = videojs(videoId, {});
            player.ready(function() {
                this.on("error", function() {
                    player.error(null);
                    console.log("player error ", videoId);
                    this.setTimeout(function() {
                        var hlstech = player.tech({IWillNotUseThisInPlugins: true});
                        hlstech.reset();

                        player.src({"type": play_type, "src": play_url});
                        player.play();
                    }, 2000);
                });

                player.src({"type": play_type, "src": play_url});
                player.play();
            });
        };
    });

})();
    
