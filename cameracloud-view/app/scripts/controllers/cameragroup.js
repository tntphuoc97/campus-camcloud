


(function () {
    'use strict';
    /**
     * @ngdoc function
     * @name sbAdminApp.controller:DeviceCtrl
     * @description
     * # DeviceCtrl
     * Controller of the sbAdminApp
     */
    cloudApp.controller('CameraGroupCtrl', function ($scope, NgMap, $http, $timeout, $uibModal, toastr, $log) {

        $scope.cameragroups = [];
        $scope.getGroups = function () {
            var dt = {routerId: $scope.routerId};
            $http.post(apiDomain + "/api/cam/getSites", dt).then(function (response) {

                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.sites) {
                            $scope.sites = response.data.dt;


                        }
                    }
                }
            }, function (response) {

            });
        }


        $scope.getGroups();
        $scope.openDlgAddGroup = function () {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/dashboard/group_detail.html',
                size: 'md',
                controller: 'CameraGroupDetailInstanceCtrl',
                resolve: {
                    group: undefined,
                }
            });

            modalInstance.result.then(function (added) {
                $scope.getGroups();
            }, function () {
            });
        }
        $scope.openDlgEditSite = function (group) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/dashboard/group_detail.html',
                size: 'md',
                controller: 'CameraGroupDetailInstanceCtrl',
                resolve: {
                    site: group,
                }
            });

            modalInstance.result.then(function (added) {
                $scope.getGroups();
            }, function () {
            });
        }
        $scope.delGroup = function(group){
            $http.post(apiDomain + "/api/group/del", group).then(function (response) {
                if (response.data.err == 0) {
                    toastr.success('Delete group success');
                    $scope.getGroups();
                } else {
                    toastr.error('Delete group error', 'Err: ' + response.data.err);
                }
            }, function (response) {
                toastr.error('Delete camera error', 'Lost connection to server');
            });
        }
    });
    cloudApp.controller('CamGroupDetailInstanceCtrl', function ($scope, $http, $uibModalInstance, toastr, group, NgMap, $timeout) {
        $scope.editMode = false;

        if (group != undefined) {
            $scope.group = angular.copy(group);

            $scope.title = "Update Group";
            $scope.action = "Update";
            $scope.editMode = true;
        } else {
            $scope.group = {
            };

            $scope.title = "Add Group";
            $scope.action = "Add";

        }
        $scope.add = function () {
            var temp = $scope.recordstream;

            let dt = JSON.stringify($scope.group);
                    $http.post(apiDomain + "/api/group/add", dt).then(function (response) {
                if (response.data.err == 0) {
                    toastr.success('Add group success ');
                    $uibModalInstance.close(true);
                } else {
                    toastr.error('Add group error', 'Error code: ' + response.data.err);
                }
            }, function () {
                toastr.error('Add group error', 'Lost connection to server');
            });
        }

        $scope.edit = function () {
            

            let dt = JSON.stringify($scope.group);
                    $http.post(apiDomain + "/api/group/udt", dt).then(function (response) {
                if (response.data.err == 0) {
                    toastr.success('Update group success');
                    $uibModalInstance.close(true);
                } else {
                    toastr.error('Update group error', 'Err: ' + response.data.err);
                }
            }, function () {
                toastr.error('Update group error', 'Lost connection to server');
            });
        }

        $scope.ok = function () {
            if ($scope.editMode) {
                $scope.edit();
            } else {
                $scope.add();
               
            }
        };
    });

})();




