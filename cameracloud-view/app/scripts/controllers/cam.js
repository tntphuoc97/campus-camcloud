/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function () {
    'use strict';
    /**
     * @ngdoc function
     * @name sbAdminApp.controller:DeviceCtrl
     * @description
     * # DeviceCtrl
     * Controller of the sbAdminApp
     */
    cloudApp.controller('CamCtrl', function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, $uibModal, toastr, $interval) {
        $scope.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers')
                .withBootstrap()
                .withOption('responsive', true)
                .withOption('stateSave', true)
                .withOption('stateDuration', -1);

        $scope.cams = []
        $scope.camsTemp = [];
        $scope.getCams = function () {
            $http.post(apiDomain + "/api/cam/gets").then(function (response) {
                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.cams) {
                            $scope.cams = response.data.dt;
                            $scope.camsTemp = response.data.dt;
                        }
                    }
                }
            }, function (response) {

            });
        }




        $scope.getCams();

        $scope.reload = function () {
//            $scope.getHubs();
//            $scope.getCams();
//            $scope.getRecorders();

        }

        $scope.stopTime = $interval(function () {
            $scope.reload();
        }, 60000)

        $scope.$on('$destroy', function () {
            // Make sure that the interval is destroyed too
            $interval.cancel($scope.stopTime);
        });

        //$scope.hubs = [{id: "1", alias: "pi1"},{id: "2", alias: "pi2"}];
        $scope.hubs = [];
        $scope.getHubs = function () {
            var dt = {routerId: $scope.routerId};
            $http.post(apiDomain + "/api/cam/get_hubs", dt).then(function (response) {
                console.log(response.data)
                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.hubs) {
                            $scope.hubs = response.data.dt;
                            console.log($scope.hubs)
                        }
                    }
                }
            }, function (response) {

            });
        }

        $scope.getAliasHub = function (cam) {
            var cut = '';
            $scope.hubs.forEach(function (hub) {
                if (hub.id.toString() === cam.hubId.toString()) {

                    cut = hub.alias;
                }
            });
            return cut;
        };
        $scope.sites = [];
        $scope.getSites = function () {
            var dt = {routerId: $scope.routerId};
            $http.post(apiDomain + "/api/cam/getSites", dt).then(function (response) {

                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.sites) {
                            $scope.sites = response.data.dt;
                            console.log("$scope.sites", $scope.sites);
                        }
                    }
                }
            }, function (response) {

            });
        }
        $scope.getSites();
        $scope.recordStreams = [];
        $scope.getRecordStreams = function () {
            $http.post(apiDomain + "/api/cam/getRecordStreams").then(function (response) {
                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.recordStreams) {
                            $scope.recordStreams = response.data.dt;

                        }
                    }
                }
            }, function (response) {

            });
        }

        $scope.getSegmentLenght = function (cam) {
            var cut = 0;
            $scope.recordStreams.forEach(function (recordstream) {
                if (recordstream.camId.toString() === cam.id.toString()) {

                    cut = recordstream.segmentLength;
                }
            });
            return cut;
        };

        $scope.DATA = {};

        $scope.getHubs();
        $scope.recorders = [];
        $scope.getRecorders = function () {
            var dt = {routerId: $scope.routerId};
            $http.post(apiDomain + "/api/cam/getRecorders", dt).then(function (response) {
                if (response.data.err === 0) {
                    if (response.data.dt) {
                        $scope.recorders = response.data.dt;
                    }
                }
            }, function (response) {

            });
        }
        $scope.getRecorders()
        $scope.getRecordStreams();

        $scope.onChangeHub = function () {


            if ($scope.hub_selected !== null) {
                var camsFinding = [];
                $scope.camsTemp.forEach(function (cam) {


                    if ($scope.hub_selected.id.toString() === cam.hubId.toString()) {
                        camsFinding.push(cam);
                    }

                });
                $scope.cams = camsFinding;
            } else {
                $scope.cams = $scope.camsTemp;
            }
        }
        $scope.openDlgAddCamera = function () {
            console.log($scope.recorders);
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/dashboard/cam_detail.html',
                size: 'md',
                controller: 'CameraDetailInstanceCtrl',
                resolve: {
                    cam: undefined,
                    hubs: function () {
                        return $scope.hubs;
                    },
                    recorders: function () {
                        return $scope.recorders;
                    },
                    sites: function () {
                        return $scope.sites;
                    }
                }
            });

            modalInstance.result.then(function (added) {
                $scope.getCams();
            }, function () {
            });
        }

        $scope.openDlgEditCamera = function (cam) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/dashboard/cam_detail.html',
                size: 'md',
                controller: 'CameraDetailInstanceCtrl',
                resolve: {
                    cam: cam,
                    hubs: function () {
                        return $scope.hubs;
                    },
                    recorders: function () {
                        return $scope.recorders;
                    }
                    ,
                    sites: function () {
                        return $scope.sites;
                    }
                }

            });

            modalInstance.result.then(function (respones) {
                $scope.getCams();
            }, function () {
            });
        }

        $scope.delCamera = function (cam) {
            $http.post(apiDomain + "/api/cam/del", cam).then(function (response) {
                if (response.data.err == 0) {
                    toastr.success('Delete camera success');
                    $scope.getCams();
                } else {
                    toastr.error('Delete camera error', 'Err: ' + response.data.err);
                }
            }, function (response) {
                toastr.error('Delete camera error', 'Lost connection to server');
            });
        };

    });
    cloudApp.controller('CameraDetailInstanceCtrl', function ($scope, $http, $uibModalInstance, toastr, cam, hubs, recorders, sites, NgMap, $timeout) {
        var vm = this;
        $scope.hubs = hubs;
        $scope.recorders = recorders;
        $scope.sites = sites;

        $scope.editMode = false;

        if (cam != undefined) {
            $scope.cam = angular.copy(cam);
            console.log("cam ne", $scope.cam);
            $scope.title = "Update Camera";
            $scope.action = "Update";
            $scope.editMode = true;
        } else {
            $scope.cam = {
            };
            $scope.recordstream = {
            };
            $scope.title = "Add Camera";
            $scope.action = "Add";

        }

        $scope.address = "";
        $scope.recordConfigDefault;
        $scope.getRecordConfig = function () {
            var cam = $scope.cam;
            if (cam !== undefined) {
                var camId = cam.id;
                $http.post(apiDomain + "/api/cam/getRecordConfig", camId).then(function (response) {
                    if (response.data.err === 0) {
                        if (response.data.dt) {
                            $scope.recordstream = response.data.dt;
                            $scope.recordConfigDefault = response.data.dt;
                        }
                        console.log("$scope.recordstream", $scope.recordstream);
                        $scope.recorders.forEach(function (element) {

                            if (element.id === $scope.recordstream.recorderId.toString()) {
                                $scope.displayRecorder = element.alias;
                            }
                        })
                    }
                }, function (response) {

                });
            }

        }
        $scope.getHubDisplay = function () {
            if ($scope.editMode === true) {
                $scope.hubs.forEach(function (element) {
                    if (element.id.toString() === $scope.cam.hubId.toString()) {
                        $scope.HubDisPlay = element.alias;
                    }
                })
            }
        }
        $scope.getHubDisplay();
        $scope.changeHubSelected = function () {
            if ($scope.hubSelected !== null) {
                $scope.cam.hubId = $scope.hubSelected;
            }
        }



        //
        $scope.getSiteDisplay = function () {
            if ($scope.editMode === true) {
                $scope.sites.forEach(function (element) {
                    if (element.id.toString() === $scope.cam.siteId.toString()) {
                        $scope.SiteDisPlay = element.site_name;
                    }
                })
            }
        }
        $scope.getSiteDisplay();
        $scope.changeSiteSelected = function () {
            if ($scope.siteSelected !== null) {
                $scope.cam.siteId = $scope.siteSelected;
            }
        }


        $scope.main_encoder = [{id: 1, value: "H265"}, {id: 2, value: 'H264'}];
        $scope.getMainEncoderDisplay = function () {
            if ($scope.cam.mainEncoder === 1) {
                $scope.mainEncoderDisPlay = "H265";
            } else if ($scope.cam.mainEncoder === 2) {
                $scope.mainEncoderDisPlay = "H264";
            } else {
                $scope.mainEncoderDisPlay = "";
            }
        }
        $scope.getMainEncoderDisplay();
        $scope.changeMainEncoder = function () {
            if ($scope.mainEncoderSelected !== null) {
                $scope.cam.mainEncoder = $scope.mainEncoderSelected;
            }
        }

        $scope.sub_encoder = [{id: 1, value: "H265"}, {id: 2, value: 'H264'}];
        $scope.getSubEncoderDisplay = function () {
            if ($scope.cam.subEncoder === 1) {
                $scope.subEncoderDisplay = "H265";
            } else if ($scope.cam.subEncoder === 2) {
                $scope.subEncoderDisplay = "H264";
            } else {
                $scope.subEncoderDisplay = "";
            }
        }
        $scope.getSubEncoderDisplay();
        $scope.changeSubEncoderSelected = function () {
            if ($scope.SubEncoderSelected !== null) {
                $scope.cam.subEncoder = $scope.SubEncoderSelected;
            }
        }
        //test api
        $scope.daysStorage = 0;
        $scope.getDayStorage = function (cam) {
            var dt = cam.id;
            $http.post(apiDomain + "/api/cam/get_days_storage_by_camId", dt).then(function (response) {
                console.log(response.data)
                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.daysStorage) {
                            $scope.daysStorage = response.data.dt;

                            console.log("$scope.daysStorage", $scope.daysStorage);
                        }
                    }
                }
            }, function (response) {

            });
        }

        $scope.sourceStorage = '';
        $scope.getSourceStorage = function (cam) {
            var dt = cam.id;
            $http.post(apiDomain + "/api/cam/get_source_storage_by_camId", dt).then(function (response) {
                console.log(response.data)
                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.sourceStorage) {
                            $scope.sourceStorage = response.data.dt;

                            console.log("$scope.sourceStorage", $scope.sourceStorage);
                        }
                    }
                }
            }, function (response) {

            });
        }
        $scope.getData = function () {
            if ($scope.editMode === true) {
                $scope.getDayStorage($scope.cam);
                $scope.getSourceStorage($scope.cam);
            }
        }
        $scope.getData()
        ///
        $scope.day_storage = [{id: 1, value: 30}, {id: 2, value: 15}, {id: 3, value: 7}];
        $scope.segment_time = [{id: 1, value: 1800}, {id: 2, value: 900}, {id: 3, value: 300}];

        $scope.changeSegmentLength = function () {
            $scope.recordstream.segmentLength = $scope.SegmentLengthValue;
            $scope.segment_time.forEach(function (element) {


                if (element.id.toString() === $scope.SegmentLengthValue) {

                    $scope.recordstream.segmentLength = element.value;
                }
            });
        }
        $scope.changeRecorderSelected = function () {
            $scope.recordstream.recorderId = $scope.recorderValue;
        }
        $scope.changeDayStorageSelected = function () {
            $scope.recordstream.dayStorage = $scope.days_storage;
            $scope.day_storage.forEach(function (element) {
                if (element.id.toString() === $scope.days_storage) {
                    $scope.recordstream.dayStorage = element.value;
                }
            });

        }

        $scope.getRecordConfig(cam);
        $scope.add = function () {
            var temp = $scope.recordstream;

            let dt = JSON.stringify($scope.cam) + JSON.stringify($scope.recordstream);
                    $http.post(apiDomain + "/api/cam/add", dt).then(function (response) {
                if (response.data.err == 0) {
                    toastr.success('Add camera success ');
                    $uibModalInstance.close(true);
                } else {
                    toastr.error('Add camera error', 'Error code: ' + response.data.err);
                }
            }, function () {
                toastr.error('Add camera error', 'Lost connection to server');
            });
        }

        $scope.edit = function () {
            var temp = $scope.recordstream;

            let dt = JSON.stringify($scope.cam) + JSON.stringify($scope.recordstream);
                    $http.post(apiDomain + "/api/cam/udt", dt).then(function (response) {
                if (response.data.err == 0) {
                    toastr.success('Update camera success');
                    $uibModalInstance.close(true);
                } else {
                    toastr.error('Update camera error', 'Err: ' + response.data.err);
                }
            }, function () {
                toastr.error('Update camera error', 'Lost connection to server');
            });
        }

        $scope.ok = function () {
            if ($scope.editMode) {
                $scope.edit();
            } else {
                $scope.add();
            }
        };

        $scope.dragEnd = function (event) {
            console.log('final position is ' + event.latLng.lat() + ' / ' + event.latLng.lng());
            $scope.cam.lng = event.latLng.lng();
            $scope.cam.lat = event.latLng.lat();
        };

        $scope.placeChanged = function () {
            // console.log($scope.cam.address)
            vm.map.setCenter($scope.cam.address);
        };

        $scope.mark = function () {
            console.log($scope.cam.address)
            console.log("current ", vm.map.markers["mark_cam"]);
            console.log("center ", vm.map.getCenter());
            vm.map.markers["mark_cam"].setPosition(vm.map.getCenter());
            $scope.cam.lng = vm.map.getCenter().lng();
            $scope.cam.lat = vm.map.getCenter().lat();
        };

        $scope.gomark = function () {
            console.log("current ", vm.map.markers["mark_cam"].getPosition());
            vm.map.setCenter(vm.map.markers["mark_cam"].getPosition());
        };

        $scope.resetForm = function () {
            if ($scope.editMode === true) {
                $scope.cam = angular.copy(cam);
            } else {
                $scope.cam = {
                };
            }
            $scope.recordstream = $scope.recordConfigDefault;
        }
    });

})();

