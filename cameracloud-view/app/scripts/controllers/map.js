(function () {
'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:DeviceCtrl
 * @description
 * # DeviceCtrl
 * Controller of the sbAdminApp
 */
    cloudApp.controller('MapCtrl',function($scope,NgMap,$http, $timeout) {
        var vm = this;
    	$scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyDP4i2bV3eK1LE220OeLB5As7bQKCzwpCc";
    	MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_PATH_  = 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m';

    	$scope.mediaToggle = {
            sources: [
                {
                    src: "",
                    type: 'application/x-mpegURL'
                }
            ]
        };

        vm.cams = [];

        vm.markerClusterer = null;

        var last_player = null;
        var videoInit = false;

	    NgMap.getMap().then(function(map) {
	        vm.map = map;
	    	$http.post(apiDomain +'/api/cam/mapget').then(function(response) {
	    		console.log(response);

                if (response.data.err == 0) {
                	vm.dynMarkers = [];
                    vm.cams = response.data.dt;

                    for (var i = 0; i < vm.cams.length; i++) {
                    	var cam = vm.cams[i];
                        cam.position = [cam.lat, cam.lng];

                        var latLng = new google.maps.LatLng(cam.lat, cam.lng);
                        var icon = {
						    url: cam.icon, // url
						    scaledSize: new google.maps.Size(32, 32), // scaled size
						    origin: new google.maps.Point(0,0), // origin
						    anchor: new google.maps.Point(0,0) // anchor
						};

                        var marker = new google.maps.Marker({position:latLng, icon: icon});

                        marker.cam = vm.cams[i];

                        //marker.metadata = {type: "point", id: cam.alias};
                        marker.addListener('click', function() {
							//vm.showDetail(i, this);

							vm.cam = this.cam;

							console.log("Show info for ", cam);

							vm.map.showInfoWindow('iw-camdetail', this);

							
							//$scope.$apply();

							//$timeout(function () {
							//	vm.loadStreamHLS();
							//}, 300);
				        });

        				vm.dynMarkers.push(marker);
                    }

                    if (vm.markerClusterer != null)
                    	vm.markerClusterer.clearMarkers();

                    vm.markerClusterer = new MarkerClusterer(vm.map, vm.dynMarkers, {});
                }
	    	});

    	});

    	$scope.$on('$destroy', function() {
			console.log("Clear cluster");
			if (vm.markerClusterer != null)
   				vm.markerClusterer.clearMarkers();

   			$timeout.cancel($scope.retryTimeout);

   			if (last_player != null) {
				last_player.dispose();
				last_player = null;
			}
        });

    	vm.clicked = function() {
            alert('Clicked a link inside infoWindow');
        };

        var isMobile = {
	        Android: function() {
	            return navigator.userAgent.match(/Android/i);
	        },
	        BlackBerry: function() {
	            return navigator.userAgent.match(/BlackBerry/i);
	        },
	        iOS: function() {
	            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	        },
	        Opera: function() {
	            return navigator.userAgent.match(/Opera Mini/i);
	        },
	        Windows: function() {
	            return navigator.userAgent.match(/IEMobile/i);
	        },
	        any: function() {
	            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	        }
	    };

		vm.showDetail = function(cam, marker) {
			console.log("index:", i);
			vm.cam = cam;

			console.log("Show info for ", cam);

			var clean = false;

			if (last_player != null) {
				clean = true;
			}

			vm.map.showInfoWindow('iw-camdetail', marker);

			if (clean) {
				last_player.dispose();
				last_player = null;
			}

			$scope.$apply();

			vm.loadStreamHLS();
		};

		vm.loadStreamHLSInit = function() {
			console.log("Load video at init");
			videoInit = true;
			vm.loadStreamHLS();
		};

        vm.loadStreamHLS = function() {
        	console.log("Load stream hls");

        	if (videoInit == false) {
        		console.log("Init video");
        		last_player = videojs("video-iw");
        		videoInit = true;
        	}

        	if (last_player != null) {
	        	last_player.dispose();
				last_player = null;
			}

	    	var src = vm.cam.streamSub;

            if (!isMobile.iOS()) {
            } else {
            	// Force use main stream for IOS
				src = vm.cam.streamMain;
            }

            $timeout(function () {
	            var videoId = "video-iw";
	            var player = videojs(videoId, {}).ready(function() {
	            	last_player = this;
					console.log("Video ", videoId , " now ready")
					var src = vm.cam.streamSub;
					// Not switch on IOS
					if (!isMobile.iOS()) {
						player.on('fullscreenchange',function() {
							console.log("full screen mode");
							src = vm.cam.streamMain;
							if (!player.isFullscreen()) {
							console.log("use sub stream");
							  src = vm.cam.streamSub;
							} else {
							console.log("use main stream")
							}
							player.src({"type":"application/x-mpegURL", "src":src});
							player.play();
						});
					} else {
						// Force use main stream for IOS
						src = vm.cam.streamMain;
					}

					player.src({type:"application/x-mpegURL", src: src});
					player.play();

					console.log("Play url: ", src);

					player.on("error", function() {
						console.log("stream error retry it next 5 seconds");
						player.error(null);
						if (!isMobile.iOS()) {
							$scope.retryTimeout = $timeout(function() {
								var hlstech = player.tech({ IWillNotUseThisInPlugins: true });
								hlstech.reset();
								player.src({type:"application/x-mpegURL", "src": src});
								player.play();
							},5000);
						}
					});
				});

        	}, 0);
		}


          $scope.$on('vjsVideoReady', function (e, data) {
				//data contains `id`, `vid`, `player` and `controlBar`
				//NOTE: vid is depricated, use player instead
				console.log('video id:' + data.id);
				$timeout.cancel($scope.retryTimeout);
				console.log('video.js player instance:' + data.player);
				console.log('video.js controlBar instance:' + data.controlBar);

                // Auto switch stream
                var player = data.player;
                var src = vm.cam.streamSub;
				// Not switch on IOS
				if (!isMobile.iOS()) {
					player.on('fullscreenchange',function() {
						console.log("full screen mode");
						src = vm.cam.streamMain;
						if (!player.isFullscreen()) {
						console.log("use sub stream");
						  src = vm.cam.streamSub;
						} else {
						console.log("use main stream")
						}

						$scope.mediaToggle = {
				            sources: [
				                {
				                    src: src,
				                    type: 'application/x-mpegURL'
				                }
				            ]
				        };

					});
				}

                player.on("error", function() {
                	console.log("stream error retry it next 5 seconds");
					player.error(null);
					if (!isMobile.iOS()) {
						$scope.retryTimeout = $timeout(function() {
							var hlstech = player.tech({ IWillNotUseThisInPlugins: true });
							hlstech.reset();

							$scope.mediaToggle = {
					            sources: [
					                {
					                    src: src,
					                    type: 'application/x-mpegURL'
					                }
					            ]
					        };

						}, 5000);
					}
		      	});

          });

          //listen for when the vjs-media object changes
          $scope.$on('vjsVideoMediaChanged', function (e, data) {
              console.log('vjsVideoMediaChanged event was fired: ', data);
          });

          vm.hideDetail = function() {
            vm.map.hideInfoWindow('foo-iw');
          };

          vm.infoWindowClose = function() {
          	console.log("infowindows close");

          	$timeout.cancel($scope.retryTimeout);

          	if (last_player != null) {
				last_player.dispose();
				last_player = null;
			}
          };

	    $scope.loadStreamHLS = function(camera) {
	    	var video = document.getElementById("video");			
			var src="https://camera.campus.zing.vn:8998/live/1/index.m3u8";
			video.src=src; 	
			var hls = null;
	    	if(Hls.isSupported()) 
			{
				if (hls!=null)
					hls.destroy();
				hls = new Hls();
				hls.loadSource(src);
				hls.attachMedia(video);
				hls.on(Hls.Events.MANIFEST_PARSED,function() {
				video.play();
				});
			}
		}
				
		$scope.changeHlsWindownPlayer = function(){
			if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement)
			{	
				video = document.getElementById("video");
				var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen;
				requestMethod.call(video);
			}
			else
				if (document.exitFullscreen) 
					document.exitFullscreen();
				else if (document.msExitFullscreen)
					document.msExitFullscreen();
				else if (document.mozCancelFullScreen)
					document.mozCancelFullScreen();
				else if (document.webkitExitFullscreen)
					document.webkitExitFullscreen();
		}
		
		var map;      
		var prev_window;
		var prev_marker;
		function addMarker(map,cam,bound,markerCluster){    
			console.log("cam" +cam);
			var marker = new google.maps.Marker({
				position: cam.location,
				icon: cam.icon,
				title: cam.alias,
				map: map,			
			});
			bound.extend(cam.location);
			markerCluster.addMarker(marker);
			//Toto add to hls
			var VideoTag= "<video id='video' style='object-fit: initial; width: 320px; height: 200px' src='' autoplay />";

			var contentString = "<div style='width: 320px'>"+
			"<h4>" + cam.alias +"</h4>"+
			"Toạ độ: <span style='color: maroon; font-weight: bold;'>" + cam.location.lat +" " + cam.location.lng + "</span><br />" +
			"Địa chỉ: <span style='color: maroon; font-weight: bold;'>" + cam.alias + "</span><br /><br />" +
			  "<a id='fullsize_link' onclick='changeHlsWindownPlayer()'>" +
			"<div id='fullscreen'>"+ VideoTag +"</div>"+
			"</a><br />"+
			"Độ phân giải camera: <span style='color: maroon; font-weight: bold;'>" + "resolution" + "px</span>&nbsp;&nbsp;";


			var infowindow = new google.maps.InfoWindow({
				content: contentString
			});     
			// Add click function to marker  
			marker.addListener('click', function() {
				if (prev_window != null) prev_window.close();
				if (prev_marker != null) prev_marker.open = false;
				console.log("Click to marker");
				infowindow.open(map,this);
				prev_marker = marker;
				prev_window = infowindow;			
				$scope.loadStreamHLS(cam);
			});
			// Add click function for map
			map.addListener('click', function() {
				console.log("Click to map");
				infowindow.close();
				prev_window.close();
				prev_marker.open = false;
				$scope.destroyStreamHLS();
			});
		} 

		$scope.icons = 
        { cam_alive: 
          { name: 'cam alive',
            icon: 'img/cam_alive.png'
          },
          cam_dead: 
          { name: 'cam dead',
            icon: 'img/cam_dead.png'
          },
          group_alive: 
          { name: 'group alive',
            icon: 'img/group_alive.png'
          }
        };

        var clusterStyles = [
        {
          textColor: 'white',
          fontWeight: 'bold',
          url: 'img/markerclusterer/m1.png',
          height: 37,
          width: 36
        },
        {
          textColor: 'white',
          url: 'img/markerclusterer/m2.png',
          height: 37,
          width: 36
        },
        {
          textColor: 'white',
          url: 'img/markerclusterer/m3.png',
          height: 43,
          width: 42
        }];   

		$scope.clusterStyles = [
        {
          textColor: 'white',
          fontWeight: 'bold',
          url: 'img/markerclusterer/m1.png',
          height: 37,
          width: 36
        },
        {
          textColor: 'white',
          url: 'img/markerclusterer/m2.png',
          height: 37,
          width: 36
        },
        {
          textColor: 'white',
          url: 'img/markerclusterer/m3.png',
          height: 43,
          width: 42
        }];   

    	$scope.cams =
	      [
	      { location: {lat: 10.7689878, lng: 106.6309066},
	      short_name: '1-TTBH ESCORT-2M',
	      long_name: '1-TTBH ESCORT-2M',
	      address: 'TTBH ESCORT-126 Lũy Bán Bích, Phường Tân Thới Hòa, Quận Tân Phú,HCM',
	      icon: 'cam_alive',
	      chain: [{id3: '000', res: '1920x1080'}]
	      },
	      { location: {lat: 16.066715, lng: 108.192022},
	      short_name: '2-cnescortdanang',
	      long_name: '2-CN-ESCORT-ĐÀ NẴNG',
	      address: '2-CN ESCORT-92 Hà Huy Tập',
	      icon: 'cam_alive',
	      chain: [{id3: '001', res: '1920x1080'}]
	      },
	      { location: {lat: 10.7623904, lng: 106.6587836},
	      short_name: '3-Showroom ESCORT',
	      long_name: '3-Showroom ESCORT',
	      address: '3-SR-ESSCORT126 Lũy Bán Bích, Phường Tân Thới Hòa, Q.Tân Phú,HCM',
	      icon: 'cam_alive',
	      chain: [{id3: '002', res: '1920x1080'}]
	      },
	      { location: {lat: 10.939860, lng: 106.868350},
	      short_name: '4-vongxoaytamhiep',
	      long_name: '4-Vòng Xoay Tam Hiệp-Biên Hòa-Đồng Nai',
	      address: 'Vòng xoay Tam Hiệp - Biên Hòa- Đồng Nai',
	      icon: 'cam_alive',
	      chain: [{id3: '003', res: '1920x1080'}]
	      },
	      { location: {lat: 12.682349, lng: 108.048672},
	      short_name: '5-nga4nguyentatthanh-batrieu-buonmathuoc-daclac',
	      long_name: '5-Ngã 4 Nguyễn Tất Thành-Bà Triệu-Buôn Ma Thuộc-Đắc Lắc',
	      address: '5-Ngã 4 Nguyễn Tất Thành-Bà Triệu-Buôn Ma Thuộc-Đắc Lắc',
	      icon: 'cam_alive',
	      chain: [{id3: '004', res: '1920x1080'}]
	      },
	      { location: {lat: 13.776266, lng: 109.226491},
	      short_name: '6-cvquangtrungqnhonbdinh',
	      long_name: '6-Công viên Quang Trung-Qui Nhơn - Bình Định',
	      address: '6-Công viên Quang Trung-Qui Nhơn - Bình Định',
	      icon: 'cam_alive',
	      chain: [{id3: '005', res: '1920x1080'}]
	      },
	      { location: {lat: 9.166264, lng: 105.144864},
	      short_name: '7-nguyentatthanhcamau',
	      long_name: '7-nguyễn Tất Thành,Triều Khang, Cà Mau',
	      address: '7-nguyễn Tất Thành,Triều Khang, Cà Mau',
	      icon: 'cam_alive',
	      chain: [{id3: '006', res: '1920x1080'}]
	      },
	      { location: {lat: 11.941553, lng: 108.438770},
	      short_name: '8-nguyentatthanhcamau',
	      long_name: '8-5 Trần Quốc Toản P1 - Đà Lạt',
	      address: '8-5 Trần Quốc Toản P1 - Đà Lạt',
	      icon: 'cam_alive',
	      chain: [{id3: '007', res: '1920x1080'}]
	      },
	      { location: {lat: 15.876412, lng: 108.326428},
	      short_name: '9-phocohoianchua',
	      long_name: '9-Phố cổ hội an -Chùa Cầu-Quảng Nam',
	      address: '9-Phố cổ hội an -Chùa Cầu-Quảng Nam',
	      icon: 'cam_alive',
	      chain: [{id3: '008', res: '1920x1080'}]
	      }
	      ];        
	});
    	
} )();
