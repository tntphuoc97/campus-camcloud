/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function () {
    'use strict';
    /**
     * @ngdoc function
     * @name sbAdminApp.controller:DeviceCtrl
     * @description
     * # DeviceCtrl
     * Controller of the sbAdminApp
     */
    cloudApp.controller('HubCtrl', function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, $uibModal, toastr, $interval) {
        $scope.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers')
                .withBootstrap()
                .withOption('responsive', true)
                .withOption('stateSave', true)
                .withOption('stateDuration', -1);
        $scope.hubs = [];
        $scope.getHubs = function () {
            var dt = {routerId: $scope.routerId};
            $http.post(apiDomain + "/api/cam/get_hubs", dt).then(function (response) {
                console.log(response.data)
                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.hubs) {
                            $scope.hubs = response.data.dt;
                            console.log($scope.hubs)
                        }
                    }
                }
            }, function (response) {

            });
        }
        $scope.getHubs();
        $scope.openHubToolDlg = function (hub) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/dashboard/hub_tool.html',
                size: 'xl',
                //windowClass: "modal-xl"
                controller: HubControllerInstanceCtrl,
                resolve: {
                    hub: hub
                }
            });

            modalInstance.result.then(function (added) {
                $scope.getCams();
            }, function (added) {
                console.log("Dismiss dialog");
                $scope.getCams();
            });
        }
        var HubDetailInstanceCtrl = function ($scope, $http, $uibModalInstance, toastr, hub) {
            $scope.title = "Add Hub";
            $scope.ok = function ()
            {
                if ($scope.alias == "")
                    toastr.success('Add hub success ');
                else {
                    console.log($scope.hub);
                    $http.post(apiDomain + "/api/publisher/add", $scope.hub).then(function (response) {
                        if (response.data.err == 0) {
                            toastr.success('Add hub success ');
                            $uibModalInstance.close(true);
                        } else {
                            toastr.error('Add hub error', 'Error code: ' + response.data.err + "\n" + response.data.msg);
                        }
                    }, function () {
                        // toastr.error('Add hub error', 'Lost connection to server');
                    });
                }
            }

            $scope.reset = function () {
                $scope.hub = {
                    alias: ""
                };
            }
        }
        var HubControllerInstanceCtrl = function ($compile, $scope, $http, $uibModalInstance, DTOptionsBuilder, DTColumnDefBuilder, toastr, hub, $timeout) {
            var clipboard = new Clipboard('#acopy', {
                text: function (trigger) {
                    toastr.success("Copy uri to clipboard")
                    return trigger.getAttribute('uri');
                }
            });

            $scope.$on('$destroy', function () {
                clipboard.destroy();
            });

            $scope.range = function (min, max, step) {
                step = step || 1;
                var input = [];
                for (var i = parseInt(min); i <= parseInt(max); i += step)
                    input.push(i);
                return input;
            };

            $scope.dtColumnDefs = [
//            DTColumnDefBuilder.newColumnDef(0),
//            DTColumnDefBuilder.newColumnDef(1).notVisible(),
//            DTColumnDefBuilder.newColumnDef(2).notSortable()
            ];

            var vm = this;

            $scope.dtOptions2 = DTOptionsBuilder.newOptions().withPaginationType('full_numbers')
                    .withBootstrap()
                    .withOption('responsive', false)
                    .withOption('stateSave', true)
                    .withOption('stateDuration', -1);

            $scope.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers')
                    .withBootstrap()
                    .withOption('responsive', true)
                    .withOption('stateSave', true)
                    .withOption('stateDuration', -1);

            // $scope.dtOptions.withOption('fnRowCallback',
            // function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //     $compile(nRow)($scope);
            // });

            $scope.editMode = false;

            $scope.hub = hub;

            $scope.types = [{id: 0, name: "Not use"}, {id: 1, name: "Main Stream"}, {id: 2, name: "Sub Stream"}];

            // if (cam != undefined) {
            //     $scope.cam = angular.copy(cam);
            //     $scope.cam.location = [$scope.cam.lat, $scope.cam.lng];
            //     $scope.title = "Update camera"
            //     $scope.action = "Update"
            //     $scope.editMode = true;
            // } else {
            //     $scope.cam = {

            //     };
            //     $scope.title = "Add camera"
            //     $scope.action = "Add"
            // }

            $scope.action = "OK"
            /*
             * 
             * 
             * WebSocket
             var wsUrl = "";
             if (window.location.protocol != 'https:')
             wsUrl = 'ws://' + window.location.hostname + ':8081/ws';
             else
             wsUrl = 'wss://' + window.location.hostname + ':8081/ws';
             
             
             var connection = new autobahn.Connection({url: wsUrl, realm: 'realm1', options: {rejectUnauthorized: false}});
             var isOpen = false;
             var currentSession = null;
             
             connection.onopen = function (session, details) {
             isOpen = true;
             console.log("Connected", details);
             currentSession = session;
             };
             
             connection.open();*/

            $scope.devices = [];


            $scope.discovery = function (type) {
                $scope.devices = [];
                if (isOpen == false) {
                    toastr.error('Discovery error', 'Lost connection to server');
                } else {
                    // 4) call a remote procedure
                    currentSession.call('vng.cam.controller.' + hub.uuid + '.discovery', ['token', type]).then(
                            function (response) {
                                console.log("Result:", response);
                                // var devices = JSON.parse(res).devices;
                                // $scope.devices = devices;
                                var result = JSON.parse(response);
                                if (result.err == 0) {
                                    result.devices.forEach(function (device) {
                                        device.username = "admin";
                                        device.password = "";
                                    });
                                    $scope.devices = result.devices;
                                    toastr.success("Discovery done");
                                } else {
                                    toastr.error("Discovery", "Error " + result.err);
                                }
                            }
                    );
                    toastr.success('Send discovery command to hub, wait for response in a few seconds');
                }
            };


            $scope.model = "Not update";
            $scope.streams = [];

            $scope.currentDevice = {};

            $scope.getStream = function (index) {
                var device = $scope.devices[index];
                $scope.currentDevice = device;
                //device.username = $("#row-"+(index+1)+"-username").val();
                //device.password = $("#row-"+(index+1)+"-password").val();
                console.log("Get stream: ", device);
                $scope.model = "Not update";
                $scope.streams = [];
                if (isOpen == false) {
                    //connection.open();
                    toastr.error('Get stream error', 'Lost connection to server');
                } else {
                    // 4) call a remote procedure
                    currentSession.call('vng.cam.controller.' + hub.uuid + '.getStream', ['token', JSON.stringify(device)]).then(
                            function (response) {
                                console.log("Result:", response);
                                var result = JSON.parse(response);
                                if (result.err == 0) {
                                    result.streams.forEach(function (stream) {
                                        stream.streamUri = URI(stream.streamUri).username(device.username).password(device.password);
                                        //stream.videoEncoder.resolution
                                        stream.videoEncoder.support.resolutionAvailables.forEach(function (resolution, index) {
//                                    resolution.value = resolution.width + 'x' + resolution.height;
                                            if (resolution.width == stream.videoEncoder.resolution.width && resolution.height == stream.videoEncoder.resolution.height) {
                                                stream.videoEncoder.resolution.index = index;
                                                console.log("Found resolution at ", index);
                                            }
                                        });
                                    });

                                    $scope.streams = result.streams;
                                    //$scope.deviceModel = result.model;
                                    $scope.deviceModel = "CAM";
                                    toastr.success("Get stream done");
                                } else {
                                    toastr.error("Get stream", "Error " + result.err);
                                }
                            },
                            function () {
                                toastr.error("Get stream error");
                            }
                    );
                    toastr.success('Send get stream command to hub, wait for response in a few seconds');
                }
            };

            $scope.updateStream = function (stream) {
                /// Validate
                // Bitrate
                if (stream.videoEncoder.support.bitrateRange.min == "" || stream.videoEncoder.support.bitrateRange.max == "") {
                    console.log("Device don't support bitrate range")
                } else {
                    var bitrateMin = parseInt(stream.videoEncoder.support.bitrateRange.min);
                    var bitrateMax = parseInt(stream.videoEncoder.support.bitrateRange.max);
                    if (stream.videoEncoder.rateControl.bitrate > bitrateMax || stream.videoEncoder.rateControl.bitrate < bitrateMin) {
                        toastr.error("Update stream error", "Bitrate must between " + bitrateMin + " and " + bitrateMax);
                        return;
                    }
                }

                // FPS
                var fpsMin = parseInt(stream.videoEncoder.support.frameRateRange.min);
                var fpsMax = parseInt(stream.videoEncoder.support.frameRateRange.max);
                if (stream.videoEncoder.rateControl.frameRate > fpsMax || stream.videoEncoder.rateControl.frameRate < fpsMin) {
                    toastr.error("Update stream error", "Framerate must between " + fpsMin + " and " + fpsMax);
                    return;
                }

                // GOV
                var govMin = parseInt(stream.videoEncoder.support.govLengthRange.min);
                var govMax = parseInt(stream.videoEncoder.support.govLengthRange.max);
                if (stream.videoEncoder.h264.govLength > govMax || stream.videoEncoder.h264.govLength < govMin) {
                    toastr.error("Updatef stream error", "Gov length must between " + govMin + " and " + govMax);
                    return;
                }

                // Remap resolution
                var resIdx = stream.videoEncoder.resolution.index;
                stream.videoEncoder.resolution = stream.videoEncoder.support.resolutionAvailables[resIdx];
                stream.videoEncoder.resolution.index = resIdx;

                if (isOpen == false) {
                    toastr.error('Get stream error', 'Lost connection to server');
                } else {
                    // 4) call a remote procedure
                    currentSession.call('vng.cam.controller.' + hub.uuid + '.setStream', ['token', JSON.stringify($scope.currentDevice), JSON.stringify(stream)]).then(
                            function (response) {
                                var result = JSON.parse(response);
                                if (result.err == 0) {
                                    toastr.success('Update stream success');
                                } else {
                                    toastr.error("Update stream failed", "Err " + response.err);
                                }
                            },
                            function () {
                                toastr.error("Get stream error");
                            }
                    );
                    toastr.success('Send get stream command to hub, wait for response in a few seconds');
                }
            }

            var added = false;


            $scope.ok = function () {
                $uibModalInstance.close(added);
            }
        };

    });

})();
