/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function () {
    'use strict';
    /**
     * @ngdoc function
     * @name sbAdminApp.controller:DeviceCtrl
     * @description
     * # DeviceCtrl
     * Controller of the sbAdminApp
     */
    cloudApp.controller('SiteCtrl', function ($scope, NgMap, $http, $timeout, $uibModal, toastr, $log) {

        $scope.sites = [];
        $scope.getSites = function () {
            var dt = {routerId: $scope.routerId};
            $http.post(apiDomain + "/api/cam/getSites", dt).then(function (response) {

                if (response.data.err == 0) {
                    if (typeof response.data.dt != 'undefined') {
                        if (response.data.dt !== $scope.sites) {
                            $scope.sites = response.data.dt;


                        }
                    }
                }
            }, function (response) {

            });
        }


        $scope.getSites();
        $scope.openDlgAddSite = function () {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/dashboard/site_detail.html',
                size: 'md',
                controller: 'SiteDetailInstanceCtrl',
                resolve: {
                    site: undefined,
                }
            });

            modalInstance.result.then(function (added) {
                $scope.getSites();
            }, function () {
            });
        }
        $scope.openDlgEditSite = function (site) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/dashboard/site_detail.html',
                size: 'md',
                controller: 'SiteDetailInstanceCtrl',
                resolve: {
                    site: site,
                }
            });

            modalInstance.result.then(function (added) {
                $scope.getSites();
            }, function () {
            });
        }
        $scope.delSite = function(site){
            $http.post(apiDomain + "/api/site/del", site).then(function (response) {
                if (response.data.err == 0) {
                    toastr.success('Delete camera success');
                    $scope.getSites();
                } else {
                    toastr.error('Delete camera error', 'Err: ' + response.data.err);
                }
            }, function (response) {
                toastr.error('Delete camera error', 'Lost connection to server');
            });
        }
    });
    cloudApp.controller('SiteDetailInstanceCtrl', function ($scope, $http, $uibModalInstance, toastr, site, NgMap, $timeout) {
        $scope.editMode = false;

        if (site != undefined) {
            $scope.site = angular.copy(site);

            $scope.title = "Update Site";
            $scope.action = "Update";
            $scope.editMode = true;
        } else {
            $scope.site = {
            };

            $scope.title = "Add Site";
            $scope.action = "Add";

        }
        $scope.add = function () {
            var temp = $scope.recordstream;

            let dt = JSON.stringify($scope.site);
                    $http.post(apiDomain + "/api/site/add", dt).then(function (response) {
                if (response.data.err == 0) {
                    toastr.success('Add site success ');
                    $uibModalInstance.close(true);
                } else {
                    toastr.error('Add site error', 'Error code: ' + response.data.err);
                }
            }, function () {
                toastr.error('Add site error', 'Lost connection to server');
            });
        }

        $scope.edit = function () {
            

            let dt = JSON.stringify($scope.site);
                    $http.post(apiDomain + "/api/site/udt", dt).then(function (response) {
                if (response.data.err == 0) {
                    toastr.success('Update site success');
                    $uibModalInstance.close(true);
                } else {
                    toastr.error('Update site error', 'Err: ' + response.data.err);
                }
            }, function () {
                toastr.error('Update site error', 'Lost connection to server');
            });
        }

        $scope.ok = function () {
            if ($scope.editMode) {
                $scope.edit();
            } else {
                $scope.add();
            }
        };
    });

})();


