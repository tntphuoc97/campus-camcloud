(function () {
    'use strict';
    /**
     * @ngdoc function
     * @name sbAdminApp.controller:DeviceCtrl
     * @description
     * # DeviceCtrl
     * Controller of the sbAdminApp
     */
    cloudApp.controller('ClientCtrl', function ($scope, $http, DTOptionsBuilder, $interval, $sessionStorage) {
        $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withBootstrap()
                .withOption('responsive', true)
                .withOption('lengthMenu', [[10, 25, 50, -1], [10, 25, 50, "All"]])
                .withOption('stateSave', true)
                .withOption('stateDuration', -1);

        $scope.routerId = "0";
        $scope.routers = []

        $scope.getRouters = function () {
            $http.post("/api/router/gets").then(function (response) {
                if (response.data.err == 0) {
                    if (response.data.dt !== $scope.routers) {
                        $scope.routers = response.data.dt;
                    }
                }
            }, function (response) {

            });
        }
        $scope.getRouters();

       
        $scope.clients = []

        $scope.getClients = function () {
            var dt = {routerId: $scope.routerId};
            $http.post("/api/dashboard/clients", dt).then(function (response) {
                if (response.data.err == 0) {
                    $scope.clients = response.data.dt;
                }
            }, function (response) {

            });
        }

        $scope.reload = function () {
            $scope.getClients();
        }
        $scope.reload();

        $scope.stopTime = $interval(function () {
            $scope.reload();
        }, 30000)

        $scope.$on('$destroy', function () {
            // Make sure that the interval is destroyed too
            $interval.cancel($scope.stopTime);
        });
    });

})();
