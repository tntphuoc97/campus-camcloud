CREATE TABLE `camera` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lng` varchar(45) DEFAULT NULL,
  `lat` varchar(45) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `icon_cluster` varchar(45) DEFAULT NULL,
  `main_stream` int(11) DEFAULT NULL,
  `sub_stream` int(11) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `uid` varchar(45) DEFAULT NULL,
  `hub` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

CREATE TABLE `publisher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `alias` varchar(45) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  `last_active` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `stream` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cam` int(11) DEFAULT NULL,
  `uid` varchar(45) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `live` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `transcode` varchar(45) DEFAULT NULL,
  `live_hls` varchar(45) DEFAULT NULL,
  `src_opt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `token` varchar(45) DEFAULT NULL,
  `verify_token` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

